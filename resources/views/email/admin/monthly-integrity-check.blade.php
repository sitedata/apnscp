@if ($stats->get("success"))
@component('email.indicator', ['status' => 'success'])
	Integrity check completed
@endcomponent
@else
@component('email.indicator', ['status' => 'error'])
	Integrity check failed
@endcomponent
@endif
@component('mail::message')
# Hello,

This is part of the monthly integrity check of {{ PANEL_BRAND }}!

**Server** {{ $hostname }}

**Server IP** {{ $ip }}

@if (!$stats->get("success"))
Some tasks failed. See attached log for explanation of tasks that failed.
@endif

---
**Items checked:** {{ $stats->get("ok") + $stats->get("changed") }}<br />
**Items changed:** {{ $stats->get("changed") }}

Duration: {{ floor(($stats->get("end") - $stats->get("begin"))/60) }} minutes {{ (floor($stats->get("end") - $stats->get("begin")) % 60) }} seconds
---
See attached log for full list of detected changes on your server. Monthly integrity checks can be easily changed from the terminal using
	[configuration scopes](https://docs.apiscp.com/admin/Scopes/).

To disable checks:<br />
`cpcmd scope:set system.monthly-integrity-check false`
@endcomponent

