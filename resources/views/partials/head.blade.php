<title>@yield('title', $Page->getApplicationTitle() . ' | ' . PANEL_BRAND)</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="{{ PANEL_BRAND }}" name="description">
<meta name="csrf-param" content="{{ \Auth_UI::CSRF_KEY }}">
<meta name="csrf-token" content="{{ \Session::get(\Auth_UI::CSRF_KEY,'') }}">
@if (FRONTEND_CONTENT_SECURITY_POLICY)
<meta http-equiv="Content-Security-Policy" content="{!! str_replace('"', "'", FRONTEND_CONTENT_SECURITY_POLICY) !!}">
@endif
<link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
<link rel="icon" href="/images/favicon.png">

@include('theme::partials.user.rolejs-metadata')

@php
	$buffer = array();
	foreach ($Page->get_css() as $css) {
		$buffer[] = '<link rel="stylesheet" href="' . $css . '" type="text/css" />';
	}
	$internal = $Page->get_css('internal');
	if (!empty($internal)) {
		$buffer[] = "<style type=\"text/css\">" . join("", $internal) . "</style>";
	}
	echo implode('', $buffer);
@endphp

@if (Page_Renderer::do_header())
	<style type="text/css">
		#ui-storage-gauge .ui-gauge-used {
			width: {{ round((float)$storagePercent, 3) }}%;
		}

		#ui-bandwidth-gauge .ui-gauge-used {
			width: {{ round((float)$bwPercent, 3) }}%;
		}
	</style>
@endif

@foreach ($Page->get_javascript('external-preload') as $js)
	<script type="text/javascript" src="{{ $js }}"></script>
@endforeach

<script language="javascript" type="text/javascript">
	$(document).ready(function () {
		$("body").removeClass("nojs").addClass("js");
		@if ($internal = $Page->get_javascript('internal', true))
		{!! $internal !!}
		@endif
	});
	{!! $Page->get_javascript('internal', false, true) !!}
</script>

{!! $Page->get_head() !!}