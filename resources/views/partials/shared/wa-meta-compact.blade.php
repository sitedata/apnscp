@if ($pane->getApplicationType())
	<span data-toggle="tooltip" class="app d-inline-block" title="{{ $pane->getName() }}"
		data-title="{{ $pane->getApplicationType() }}" style="max-height: 1em;">
		@includeFirst(['@webapp(' . $pane->getApplicationType(). ')::icon-sm', 'theme::partials.shared.wa-type-indicator'])
	</span>
@endif