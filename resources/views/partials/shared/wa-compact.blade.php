<div class="col-12 item hostname screenshot mb-1 ui-webapp-panel"
     data-hostname="{{ $pane->getHostname() }}" data-type="{{ $pane->getApplicationType() }}"
     data-docroot="{{ $pane->getDocumentRoot() }}">

	<div class="d-flex align-items-center">
		@isset($actions)
			<div class="mr-2">
				@include($actions['view'], array_except($actions, 'view'))
			</div>
		@endif

		<div class="meta-inline form-control-static mx-2 {{ $meta['classes'] ?? "" }}">
			@include($meta['view'] ?? 'theme::partials.shared.wa-meta-compact')
		</div>

		<div class="d-inline-block form-control-static @if ($pane->isSubdomain()) subdomain @else domain @endif">
			{{ $pane->getLocation() }}
		</div>

		<div class="ml-auto mr-0 attributes d-none d-lg-inline-flex align-items-center">
			<span class="docroot ml-3 d-none attribute">
				<i class="fa fa-folder"></i>
				{{ $pane->getDocumentRoot() }}
			</span>

				<span class="type ml-3 d-none attribute">
				<i class="fa fa-question"></i>
				{{ $pane->getApplicationType() }}
			</span>

				<span class="hostname ml-3 d-none attribute">
				<i class="fa fa-globe"></i>
				{{ $pane->getHostname() }}
			</span>
		</div>

	</div>
</div>