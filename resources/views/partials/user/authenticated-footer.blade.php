@if (\UCard::is('site') && cmd('crm_configured'))
	<div id="feedbackContainer" class="hide container-fluid ma-0">
		<div class="row">
			<div class="col-12 text-center">
				<h3 class="display-4 mb-3">
					<i class="fa fa-check-circle-o  text-success"></i>
					Feedback Form
				</h3>
			</div>
			<div class="col-12 col-md-6 col-lg-4">
				<label class="d-block b">
					What's on your mind?
				</label>
				<select name="type" class="custom-select col-12" id="feedbackType">
					<option value="{{ cmd('crm_get_subject_id_by_subject', 'general feedback') }}">Feature Request
					</option>
					<option value="{{ cmd('crm_get_subject_id_by_subject', 'control panel bug') }}">Bug</option>
				</select>
			</div>
			<div class="col-12 col-md-6 col-lg-8">
				<label class="d-block b">
					Contact Address
				</label>
				<div class="input-group">
							<span class="input-group-addon">
								<i class="fa fa-envelope-o"></i>
							</span>
					<input type="text" class="form-control" name="email" id="feedbackEmail"
						   value="{{ cmd('site_get_admin_email') }}"/>
				</div>
			</div>
			<div class="col-12 mt-3">
				<label class="d-block b">
					Description
				</label>
				<textarea class="form-control" id="feedback"></textarea>
			</div>
			<div class="col-12">
				<button type="button" id="submitFeedback" value="1" class="btn btn-primary mt-3">
					<i class="fa fa fa-envelope-o"></i>
					Submit Feedback
				</button>
			</div>
		</div>
	</div>
@endif