<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class MetricsConcurrencyRefresh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::connection('pgsql')->getPdo()->exec("DROP MATERIALIZED VIEW IF EXISTS metrics_value_view CASCADE");
		DB::connection('pgsql')->getPdo()->exec("CREATE MATERIALIZED VIEW metrics_value_view  AS 
			SELECT
			  site_id,
			  ts,
			  \"value\",
			  \"name\"
			  FROM metrics
			  JOIN metric_attributes USING (attr_id)
			  WHERE 
			  type = 'value' AND
			  ts >= NOW() - interval '1 day'
			  WINDOW w AS (ORDER BY ts ASC)");
		DB::connection('pgsql')->getPdo()->exec("CREATE INDEX metrics_value_view_site_id_idx ON metrics_value_view USING BRIN(site_id,ts);");
		DB::connection('pgsql')->getPdo()->exec("TRUNCATE TABLE metrics");
		foreach (['value', 'monotonic'] as $name) {
			DB::connection('pgsql')->getPdo()->exec("REFRESH MATERIALIZED VIEW metrics_${name}_view");
			DB::connection('pgsql')->getPdo()->exec("CREATE UNIQUE INDEX IF NOT EXISTS metrics_${name}_view_index ON metrics_${name}_view(site_id,ts,name)");
		}

		DB::connection('pgsql')->getPdo()->exec("CREATE VIEW metrics_view AS 
			  SELECT
				site_id,
				ts,
				\"value\",
				\"name\"
				FROM metrics_value_view
			  UNION
			  SELECT
				site_id,
				ts,
				\"value\",
				\"name\"
			  FROM metrics_monotonic_view;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
