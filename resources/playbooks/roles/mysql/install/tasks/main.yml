---
- include_role: name=common tasks_from=create-self-signed-certificate.yml
  vars:
    crt: "{{ mysql_ssl_pem }}"
    owner: mysql
    notify: Restart mariadb
  # service command won't forward to systemd if alias,
  # "service mysql restart" with notify leaves service in failed state
- name: Remove /etc/init.d/mysql SysV service
  file:
    path: /etc/init.d/mysql
    state: absent
  # MariaDB 5.5/10.0 lack systemd service file
- name: Install backwards compatibility systemd service
  include_role: name=apnscp/install-services
  vars:
    templated_services:
      - service: mariadb
        template: mariadb-systemd-compatibility.service.j2
    services:
      - name: mariadb
        files:
          - src: "{{ mariadb_service_file }}"
            dest: /etc/systemd/system/mariadb.service
  when: mariadb_version is version('10.0', '<=')
- name: Override systemd defaults
  include_tasks: set-systemd-defaults.yml
- systemd: name=mariadb state=started enabled=yes
- set_fact:
    password: "{{ lookup('password', '/dev/null chars=ascii_letters length=25') }}"
  no_log: true
- name: Check for password presence
  stat: path={{ mysql_pass_file }}
  register: s

- name: Verify MySQL socket presence
  stat: path=/var/lib/mysql/mysql.sock
  register: sockStat
- name: Fix mysql.sock linkage
  include_role:
    name: apnscp/initialize-filesystem-template
    tasks_from: relink-mysql.yml
  when: not sockStat.stat.exists
- block:
  - name: Enable root MySQL user
    include_tasks: enable-root-user.yml
  rescue:
    # Recover MariaDB instance from busted authentication
    - name: Copy password recovery configuration
      copy:
        src: "files/password-recovery.conf"
        dest: "/etc/my.cnf.d/{{ mysql_recovery_config }}"
      notify: Restart mariadb
    - meta: flush_handlers
    - name: Clear root password
      shell: >-
        echo "UPDATE global_priv SET Priv = JSON_REPLACE(Priv, '\$.authentication_string', '')
          WHERE User = 'root'; FLUSH PRIVILEGES;" | mysql mysql
      when: mariadb_version is version('10.4', '>=')
    - name: Clear root password
      shell: >-
        echo "UPDATE user SET Password = '', authentication_string = ''
          WHERE user = 'root'; FLUSH PRIVILEGES;" | mysql mysql
      when: mariadb_version is version('10.4', '<')
    - name: Redact recovery settings
      file:
        path: "/etc/my.cnf.d/{{ mysql_recovery_config }}"
        state: absent
      notify: Restart mariadb
    - meta: flush_handlers
    - name: Enable root MySQL user (rescue)
      include_tasks: enable-root-user.yml
- name: Save MySQL password
  template:
    src: templates/my.cnf.j2
    dest: "{{ mysql_pass_file }}"
    mode: 0600
  when: user_enabled is defined and user_enabled.changed
- mysql_user:
    login_user: root
    name: root
    host: localhost
    # ALL excludes GRANT. GRANT itself yields syntax error
    priv: "*.*:ALL,GRANT"
    append_privs: yes
    state: present
- name: Find password column in mysql.user
  shell: >
    echo "SELECT column_name FROM INFORMATION_SCHEMA.columns WHERE
      table_schema = 'mysql' AND table_name = 'user' AND column_name =
      'authentication_string'" | mysql --column-names=false
  environment:
    HOME: /root
  register: o
  changed_when: false
- set_fact: mysql_delete_clause="password = ''"
  when: o.stdout == ''
- set_fact: mysql_delete_clause="password = '' AND authentication_string = ''"
  when: o.stdout != ''
- name: Remove passwordless users
  shell: >
    echo "DELETE FROM user WHERE {{ mysql_delete_clause }} AND user != 'mariadb.sys';
      SELECT ROW_COUNT(); FLUSH PRIVILEGES;" | mysql mysql --column-names=false
  register: o
  changed_when: (o.stdout | int) > 0
  when: user_enabled is defined and user_enabled.changed
- name: Remove test database if present
  mysql_db:
    name: test
    state: absent
  when: user_enabled is defined and user_enabled.changed
  register: test
- name: Remove empty user grants
  mysql_user:
    name: ""
    host: "%"
    state: absent
  when: test is defined and test.changed or
    user_enabled is defined and user_enabled.changed
- name: Remove test db grants
  shell: >
    echo "DELETE FROM db WHERE db IN('test', 'test\_%') AND host = '%';
      SELECT ROW_COUNT(); FLUSH PRIVILEGES;" | mysql mysql --column-names=false
  register: o
  changed_when: (o.stdout | int) > 0
  when: test is defined and test.changed or
    user_enabled is defined and user_enabled.changed

- name: Merge custom config
  include_tasks: "{{ playbook_dir }}/roles/common/tasks/implicitly-import-overrides.yml"
  vars:
    base: "{{ mysql_config }}"
    varname: __config
    prefix: ''
    name: mysql

- name: Reconfigure mysql
  ini_file:
    path: /etc/my.cnf.d/apnscp.cnf
    section: "{{ item.value.section | default('mysqld') }}"
    option: "{{ item.key }}"
    value: "{{ item.value.value | default(item.value) }}"
    state: "{{ (item.value | default('') != '') | ternary('present', 'absent') }}"
  with_dict: "{{ __config }}"
  loop_control:
    label: "{{ (item.value | default('') != '') | ternary('Setting', 'Removing') }} MySQL {{ item.key }} = {{ item.value }}"
  notify: Restart mariadb

- meta: flush_handlers

- name: Link MySQL log to /var/log
  file:
    path: /var/log/mysqld.log
    state: link
    src: /var/lib/mysql/mysqld.log

- name: Record MySQL version
  include_role: name=common tasks_from=record-runtime-setting.yml
  vars:
    setting: mariadb_version
