---
# INTERNAL
pgsql_fst_path: "{{ apnscp_shared_root }}/pgsql"
pgsql_ssl_pem: /etc/ssl/certs/pgsql-server.pem
# Locations where PostgreSQL UDS is available
pgsql_unix_socket_directory:
  - /tmp
  - "{{ pgsql_fst_path }}"
  - /var/run/postgresql
passwdfile: /root/.pgpass
pgsql_config_file: /var/lib/pgsql/{{pgversion}}/data/postgresql.conf
timescaledb_repo: /etc/yum.repos.d/timescaledb.repo
# Automatically optimize configuration when this task is run
# A separate Synchronizer hook exists to optimize whenever Timescale changes
# Controlled via [telemetry] => autotune
pgsql_optimize_configuration: false
pgsql_auth_delay: "{{ pgsql_remote_connections | ternary(500, 0) }}"
pgsql_bind_address: "{{ pgsql_remote_connections | ternary('*', '127.0.0.1, ::1') }}"
timescaledb_packages:
  - "{{ pgversion is version('13', '>=') | ternary('timescaledb-2-postgresql-' + pgversion, 'timescaledb-postgresql-' + pgversion) }}"
# off,
timescaledb_telemetry_reporting: "off"
pg_authlog_settings:
  pg_log_authfail.log_destination: syslog
  pg_log_authfail.syslog_facility: authpriv
  pg_log_authfail.syslog_ident: pgsql
  pg_log_authfail.use_log_line_prefix: "false"
  pg_log_authfail.log_success: "false"
  pg_log_authfail.log_aborted: "true"
pgsql_install_marker: yes
# Protects against worker thread deadlocks
pgsql_shutdown_timeout: 5m
# Maximum concurrent PostgreSQL connections.
# Each connection runs its own process space.
pgsql_max_connections: 50
# Memory to assume in tuning
timescale_memory_tune: "{{ has_low_memory | ternary('16MB', '64MB') }}"
# Storage to set aside for write-ahead logging ("B" suffix required)
timescale_wal_tune: 128MB
timescale_tuning_parameters: >-
  -max-conns {{ pgsql_max_connections | int }}
  -memory {{ timescale_memory_tune | quote }}
  -wal-disk-size {{ timescale_wal_tune | quote }}
# Override via pgsql_custom_config dictionary
pgsql_config:
  unix_socket_directories: "'{{ pgsql_unix_socket_directory | join(', ')  }}'"
  max_locks_per_transaction: "{{ pgsql_max_locks_per_transaction | default(128) }}"
  max_connections: "{{ pgsql_max_connections | int | default(20) }}"
  autovacuum_naptime: 1min
  ssl: "on"
  ssl_cert_file: "'{{ pgsql_ssl_pem }}'"
  ssl_key_file: "'{{ pgsql_ssl_pem }}'"

# Enable PostGIS support
pgsql_has_postgis: False
# Default PostGIS version
postgis_version: "{{ (pgversion is version('13', '>=') | ternary('3.3', pgversion is version('12', '<') | ternary('2.4','2.5'))) }}"
# INTERNAL PostGIS package name
postgis_package: "postgis{{ pgsql_has_postgis | ternary(postgis_version | string | regex_replace('\\.', '') + '_' + (pgversion | regex_replace('\\..+', '')), '*') }}"
