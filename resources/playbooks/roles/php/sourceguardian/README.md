# Ansible Role: sourceguardian-loader

Installs SourceGuardian on Linux.

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values:

    workspace: /root
    
    php_version: '7.1'
    php_sourceguardian_loader_module_path: "/usr/lib/php5/modules"
    php_sourceguardian_loader_module_filename: "ixed.{{ php_version }}.lin"
    php_sourceguardian_loader_config_filename: "10-sourceguardian.ini"
    php_sourceguardian_user: root
    php_sourceguardian_group: root
    php_extension_conf_paths:
      - "/etc/php/{{ php_version }}/fpm/conf.d"
      - "/etc/php/{{ php_version }}/apache2/conf.d"
      - "/etc/php/{{ php_version }}/cli/conf.d"

## Dependencies

None.

## Example Playbook

    - hosts: all
      roles:
        - sourceguardian

*Inside `vars/main.yml`*:

    workspace: /root
    php_version: '7.2'

## License

MIT / BSD

## Author Information

This role is based on work by Alexander Kapitman (akman.ioncube-loader)
