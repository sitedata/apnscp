# Set specific flags with:
# php72_build_flags: --foo --bar
- stat:
    path: "{{ php_bin }}"
    follow: yes
  register: s2
- command: >
    {{ php_bin }} -nr 'print(PHP_VERSION);'
  changed_when: false
  ignore_errors: True
  when: s2.stat.exists
  register: installed_php_version

- set_fact:
    do_install_php: >-
      {{ not remove_only | bool and (( (php_force_build | default(false)) or force or not s2.stat.exists or
      installed_php_version.failed or installed_php_version.stdout != __php_version)) }}

- name: Validate multiPHP build is not system build
  assert:
    that: __php_version | regex_replace('^([0-9]+)\\.([^\\.]*).*$', '\\1.\\2') != system_php_version
    fail_msg: "multiPHP build duplicates system PHP build {{ system_php_version }}. Remove this version by running: cpcmd scope:set php.multi '[{{ system_php_version }}: false]'"
  when: multiphp_build | bool

- name: Cleanup older PHPs
  find:
    file_type: directory
    paths: "{{ work_dir }}"
    excludes: "{{ (remove_all_versions or remove_only | bool) | ternary(omit, 'php-' + lookup('vars', '__php_version')) }}"
    patterns: "php-*"
  register: phps
- name: Remove older PHPs
  file:
    path: "{{item.path}}"
    state: absent
  loop_control:
    label: "Removing {{ item.path | basename }}"
  with_items: "{{ phps.files }}"
  when: >-
    remove_all_versions | bool or
    -1 != item.path.find('php-%s.' % lookup('vars', '__php_version') | regex_replace('^(\\d+\.\\d+)\\..*$', '\\1'))
- name: Remove PHP configuration directory
  file:
    path: "{{ apnscp_filesystem_template }}/{{ multiphp_fst_service }}/etc/php{{ lookup('vars', '__php_version') | regex_replace('^(\\d+)\\.(\\d+)', '\\1\\2') }}.d"
    state: absent
  when: remove_only | bool and multiphp_build
  notify: Reload filesystem template

- name: Reduce max concurrent jobs
  set_fact: compile_max_jobs=1
  when: ansible_memory_mb.nocache.free < 128 or ansible_memory_mb.nocache.free < 512*(compile_max_jobs | int) and __php_version is version('8.0', '>=')
- name: Assist PHP compilation on low-memory machines
  block:
    - name: Attach temporary swap
      include_role: name=filesystem/swap
      vars:
        # Required for compiling PHP on 1 GB machines
        swapfile_size: 2048
        swapfile_location: "{{ temporary_swap_path }}"
        # fallocate reports holes in C7
        swapfile_use_dd: "{{ ansible_distribution_major_version == '7' }}"
        swapfile_add_fstab: False
    - name: Flag swap handler
      command: /bin/true
      notify:
        - Disable temporary swap
        - Remove temporary swap
  when: ansible_memory_mb.nocache.free < 128 or ansible_memory_mb.nocache.free < 768 and __php_version is version('8.0', '>=')

- set_fact: force_module_rebuild="{{ force | bool }}"
- include_tasks: build-php.yml
  when: do_install_php | bool
