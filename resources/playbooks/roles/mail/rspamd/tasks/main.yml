---
- name: Enable rspamd tasks
  include_tasks: enable-rspamd.yml
  when: rspamd_enabled | bool

- block:
    - name: Disable rspamd
      systemd: name=rspamd state=stopped enabled=false
      failed_when: false
  when: not rspamd_enabled | bool

- include_role: name=software/argos tasks_from=toggle-service.yml
  vars:
    name: rspamd
    state: "{{ rspamd_enabled | bool }}"

- name: "Set rspamd installation state in apnscp"
  include_role: name="apnscp/bootstrap" tasks_from="set-config.yml"
  vars:
    section: mail
    option: "{{ item.key }}"
    value: "{{ item.value }}"
    check_value: True
  with_dict:
    rspamd_present: "{{ rspamd_enabled | bool }}"
    dkim_signing: "{{ rspamd_enabled | bool | ternary(rspamd_dkim_signing, False) }}"

- name: Replicate rspamd package
  include_role:
    name: apnscp/initialize-filesystem-template
    tasks_from: "install-package.yml"
  vars:
    package: "{{ rspamd_packages.packages | join(' ') }}"
    service: "{{ rspamd_packages.service }}"
    include_dependencies: true
  when: rspamd_enabled | bool

- block:
  - name: Remove rspamd package
    include_role:
      name: apnscp/initialize-filesystem-template
      tasks_from: "remove-package.yml"
    vars:
      package: "{{ item }}"
    with_items: "{{ rspamd_packages.packages }}"
  - name: Disable redis storage
    systemd: name=redis-rspamd state=stopped enabled=false
    register: r
    failed_when:
      - r.failed
      - '"Could not find the requested service" not in r.msg'
      # Masked
      - '"Invalid argument" not in r.msg'
    environment:
      LANGUAGE: en_US
  when: not rspamd_enabled or not rspamd_redis_local

- name: "{{ rspamd_enabled | ternary('Add', 'Remove') }} dpt:25 access to {{ rspamd_group }}"
  include_role: name=network/setup-firewall tasks_from=set-direct-rule.yml
  vars:
    permanent: true
    mode: "{{ (rspamd_enabled and lookup('vars', 'has_' + proto)) | ternary('add','remove') }}"
    net: "{{ proto }}"
    filter: OUTPUT
    priority: 1
    rule: "-p tcp -m tcp --dport 25 -m owner --gid-owner {{ rspamd_group }} -j ACCEPT"
  with_items:
    - ipv4
    - ipv6
  loop_control:
    loop_var: proto
# General spam filtering
# @TODO move to separate role?

- name: Remove spamassassin rules configuration
  file: path="{{ rspamd_local_config_dir }}/spamassassin.conf" state=absent
  notify: Restart rspamd
  when: mail_enabled and (not rspamd_use_spamassassin_rules | bool)

- name: "{{ (spamfilter == 'spamassassin') | ternary('Enable', 'Disable') }} spamc usage"
  replace:
    path: "{{ item }}"
    regexp: '^(\s*){{ (spamfilter == "spamassassin") | ternary("#\s*", "") }}(xfilter\s+.*?(?<=[\/\s])spamc\s+.*)$'
    replace: '\1{{ (spamfilter == "spamassassin") | ternary("", "#") }}\2'
  with_items: "{{ maildrop_files }}"

- name: Ensure X-Spam-Score header present in piggyback mode
  replace:
    path: /etc/mail/spamassassin/local.cf
    regexp: '^\s*add_header\s+(?!all\S+)\s+[sS]core\s+.*$'
    replace: 'add_header all Score _SCORE_'
  notify: Restart spamassassin
  when: rspamd_method == "piggyback" and spamassassin_enabled

- name: '{{ rspamd_enabled | ternary("Add", "Remove") }} learning aliases'
  include_role:
    name: mail/configure-postfix
    tasks_from: manage-alias.yml
  vars:
    email: "{{ item.key }}"
    destination: "{{ rspamd_enabled | ternary(item.value, None) }}"
  loop: "{{ rspamd_helper_aliases | dict2items }}"

- name: '{{ rspamd_enabled | ternary("Add", "Remove") }} milter configuration for Postfix'
  include_role: name=mail/configure-postfix tasks_from=set-configuration.yml
  vars:
    config_file: "{{ postfix_config_file }}"
    config: "{{ rspamd_postfix_config }}"

- name: "{{ rspamd_passive_learning_mode | ternary('Set', 'Remove' ) }} piggyback SpamAssassin results"
  blockinfile:
    path: "{{ item }}"
    marker: "# {mark} MANAGED BLOCK - DO NOT TOUCH"
    block: "{{ rspamd_piggyback_block }}"
    insertafter: '^DELETE_THRESHOLD=[\d\.]+$'
    state: "{{ rspamd_passive_learning_mode | ternary('present', 'absent') }}"
  with_items: "{{ maildrop_files }}"

- include_tasks: tasks/setup-dkim.yml
  when: rspamd_enabled and rspamd_dkim_signing
