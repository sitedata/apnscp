<?php declare(strict_types=1);
	/**
	 * Hook facility (ImportDomain)
	 * Called AFTER  a domain is imported
	 * Parameters:
	 *   - string site
	 *   - string format
	 *   - string backup basedir
	 *
	 * This file, if present in config/custom/hooks/ named either
	 * importDomain.sh or importDomain.php will be called AFTER
	 * the account has been imported
	 */

	$n = 0;
	do {
		$path = dirname(__FILE__, ++$n);
	} while (!file_exists("$path/lib/config.php"));
	define('INCLUDE_PATH', realpath($path));
	include(INCLUDE_PATH . '/lib/CLI/cmd.php');

	$args = cli\parse();
	$c = cli\cmd(null, $args[0]);
	if (!is_object($c)) {
		fatal("Failed to create instance of `%s'", $args[0]);
	}

	/**
	 * Do whatever
	 */
	$ctx = \Auth::profile();
	echo "Hello as ", $c->common_whoami(), " on ", $ctx->domain, " (", $ctx->site, ")\n";
	printf("Import type: %s, backup file location: %s\n", $args[1], $args[2]);
	// post hook formatting
	$buffer = Error_Reporter::get_buffer();
	dlog("Import hooks %s (%x) %s",
		$ctx->site,
		Error_Reporter::error_type(Error_Reporter::get_severity()),
		Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
	);
	\cli\dump_buffer($buffer);

	exit (Error_Reporter::is_error());