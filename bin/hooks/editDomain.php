<?php declare(strict_types=1);
	/**
	 * Hook facility (EditDomain)
	 * Called AFTER a domain is modified
	 * Parameters:
	 *   - string site
	 *
	 * This file, if present in config/custom/hooks/ named either
	 * editDomain.sh or editDomain.php will be called AFTER
	 * the account has been created
	 */

	$n = 0;
	do {
		$path = dirname(__FILE__, ++$n);
	} while (!file_exists("$path/lib/config.php"));
	define('INCLUDE_PATH', realpath($path));
	include(INCLUDE_PATH . '/lib/CLI/cmd.php');

	$args = cli\parse();
	$c = cli\cmd(null, $args[0]);
	if (!is_object($c)) {
		fatal("Failed to create instance of `%s'", $args[0]);
	}

	/**
	 * Do whatever
	 */
	$ctx = \Auth::profile();
	echo "Hello as ", $c->common_whoami(), " on ", $ctx->domain, " (", $ctx->site, ")\n";
	echo "Following changes were noted:\n";
	// show all features enabled
	echo \Symfony\Component\Yaml\Yaml::dump(
		\Util_PHP::array_diff_assoc_recursive(
			$ctx->getAccount()->new,
			$ctx->getAccount()->cur
		)
	);

	// post hook formatting
	$buffer = Error_Reporter::get_buffer();
	dlog("Edit hook %s (%x) %s",
		$ctx->site,
		Error_Reporter::error_type(Error_Reporter::get_severity()),
		Error_Reporter::is_error() ? 'Failed' : 'Succeeded'
	);
	\cli\dump_buffer($buffer);

	exit (Error_Reporter::is_error());