<?php
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
 */

ini_set('register_argc_argv', '1');
	ini_set('soap.wsdl_cache_enabled','1');

	class Parse_Ticket {
		const ENDPOINT = 'https://localhost:2083/';
		const TESTING_ENDPOINT = 'http://192.168.0.5/';
		const EFATAL = 75;
		const CRM_FROM_NAME = 'Support';

		private $_endpoint;
		private $_soapClient;
		private $_body;
		private $_attachments = array();
		// mail user agent
		private $_mua = null;

		// @var Object
		protected $structure;
		// @var Object
		protected $mpmsg;

		public function __construct($key = null) {
			$endpoint = getenv('ENDPOINT') ?: self::ENDPOINT;
			$wsdl = $endpoint . '/apnscp.wsdl';
			$location = $endpoint . 'soap?authkey=' . $key ?? getenv('KEY');
			$opts = array(
				'connection_timeout' => 5,
				'location' => $location,
				'uri' => 'urn:net.apnscp.soap',
				'trace' => true);
			$this->_soapClient = new SoapClient($wsdl, $opts);
		}

		public function __destruct() {
			if ($this->structure) {
				$this->structure = null;
			}
			if ($this->mpmsg) {
				$this->mpmsg = null;
			}
		}

		private function _isDev(): bool
		{
			return getenv('DEBUG') !== false;
		}
		/**
		 * Get ticket has from either reply-to or in-reply-to
		 *
		 * @param array $headers
		 * @return string
		 */
		private function _getHash($headers)
		{
			$regex = '([a-f0-9]{32,48})';

			$inreplyto = null;
			if (isset($headers['in-reply-to'])) {
				$inreplyto = $headers['in-reply-to'];
			}
			if (is_array($inreplyto)) {
				$inreplyto = array_pop($inreplyto);
			}

			if (preg_match('/' . $regex . '-\d+@/', $inreplyto, $match)) {
				$hash = $match[1];
				return $hash;
			}

			$references = null;
			if (isset($headers['references'])) {
				$references = $headers['references'];
			}
			if (is_array($references)) {
				$references = array_pop($references);
			}

			if (preg_match('/' . $regex . '-\d+@/', $references, $match)) {
				$hash = $match[1];
				return $hash;
			}

			$replyto = null;
			if (isset($headers['reply-to'])) {
				$replyto = $headers['reply-to'];
			}
			if (preg_match('/\+' . $regex .'@/', $replyto, $match)) {
				$hash = $match[1];
				return $hash;
			}
			if (preg_match('/\+' . $regex . '@/', $headers['to'], $match)) {
				$hash = $match[1];
				return $hash;
			}
			return false;
		}

		/**
		 * Interpret a format=flowed message body according to RFC 2646
		 *
		 * @param string $text Raw body formatted as flowed text
		 * @param string $mark Mark each flowed line with specified character
		 *
		 * @return string Interpreted text with unwrapped lines and stuffed space removed
		 */
		public static function unfold_flowed($text, $mark = null)
		{
			$text    = preg_split('/\r?\n/', $text);
			$last    = -1;
			$q_level = 0;
			$marks   = array();
			foreach ($text as $idx => $line) {
				if ($q = strspn($line, '>')) {
					// remove quote chars
					$line = substr($line, $q);
					if (!$line) {
						// empty line
						continue;
					}
					// remove (optional) space-staffing
					if ($line[0] === ' ') $line = substr($line, 1);
					// The same paragraph (We join current line with the previous one) when:
					// - the same level of quoting
					// - previous line was flowed
					// - previous line contains more than only one single space (and quote char(s))
					if ($q == $q_level
						&& isset($text[$last]) && $text[$last][strlen($text[$last])-1] == ' '
						&& !preg_match('/^>+ {0,1}$/', $text[$last])
					) {
						$text[$last] .= $line;
						unset($text[$idx]);
						if ($mark) {
							$marks[$last] = true;
						}
					}
					else {
						$last = $idx;
					}
				}
				else {
					if ($line == '-- ') {
						$last = $idx;
					}
					else if (isset($line[0])) {
						// remove space-stuffing
						if ($line[0] === ' ') $line = substr($line, 1);
						if (isset($text[$last]) && $line && !$q_level
							&& $text[$last] != '-- '
							&& $text[$last][strlen($text[$last])-1] == ' '
						) {
							$text[$last] .= $line;
							unset($text[$idx]);
							if ($mark) {
								$marks[$last] = true;
							}
						}
						else {
							$text[$idx] = $line;
							$last = $idx;
						}
					}
				}
				$q_level = $q;
			}
			if (!empty($marks)) {
				foreach (array_keys($marks) as $mk) {
					$text[$mk] = $mark . $text[$mk];
				}
			}
			return implode("\r\n", $text);
		}

		public function parse($msg)
		{
			$sender = 'site';
			$hash = null;
			$this->mpmsg = mailparse_msg_create();
			if (!mailparse_msg_parse($this->mpmsg, $msg)) {
				echo "could not parse email!\n";
				exit(self::EFATAL);
			}

			if (!($msg_info = mailparse_msg_get_part_data($this->mpmsg))) {
				echo "could not get message info!\n";
				exit(self::EFATAL);
			} // $msg_info['headers'] is the headers associated array
			$this->structure = mailparse_msg_get_structure($this->mpmsg);
			// in the unfortunate event that 2 To: headers are specified...
			$to = $msg_info['headers']['to'];
			if (is_array($to)) {
				$to = array_pop($to);
			}
			$addr = mailparse_rfc822_parse_addresses($to);
			$to = array_pop($addr);
			$from = $msg_info['headers']['from'];
			if (is_array($from)) {
				$from = array_pop($from);
			}
			$addr = mailparse_rfc822_parse_addresses($from);
			$from = array_pop($addr);

			$hash = $this->_getHash($msg_info['headers']);
			if (!$hash) {
				echo "malformed ticket id";
				exit (self::EFATAL);
			}
			if (strstr($from['address'], getenv("ADMIN_ADDR"))) {
				$sender = 'admin';
			}
			$body = ''; $atms = array();
			foreach ($this->structure as $element) {
				$part_mime = mailparse_msg_get_part($this->mpmsg, $element);

				$part = mailparse_msg_get_part_data($part_mime);
				if (!$part) continue;
				// mime-encoded data
				$part_content = substr($msg,
					$part['starting-pos-body'],
					$part['ending-pos-body'] - $part['starting-pos-body']
				);
				// transform encoded data into file
				$this->_parseAttachment($part_content, $part);
			}
			$this->mpmsg = null;

			if (!$body && isset($part_content)) {
				$i = 0;
				$n = sizeof($this->_attachments);
				do {
					$a = current($this->_attachments);
					if ($a === false || $i > $n) break;
					$header = $a['headers'];
					$content = rtrim($a['content']);
					if (isset($header['content-name'])) {
						$foundAttachments = true;
					} else {
						$foundAttachments = $this->_postProcessBug($content);
					}

					if ($foundAttachments && !isset($header['filename']) &&
							!isset($header['disposition-filename']))
					{
						$key = key($this->_attachments);
						unset($this->_attachments[$key]);
					}
					next($this->_attachments);
					$i++;
				} while (true);

			}

			try {
				$attachments = array();
				if ($this->_attachments) {
					foreach ($this->_attachments as $a) {
						$headers = $a['headers'];
						$filename = null;
						if (isset($headers['filename'])) {
							$filename = $headers['filename'];
						} else if (isset($headers['disposition-filename'])) {
							$filename = $headers['disposition-filename'];
						} else if (isset($headers['content-name'])) {
							// inline file attachment
							$filename = $headers['content-name'];
						}
						// no filename, likely HTML attachment
						if (!$filename) {
							continue;
						}
						$attachments[] = array(
							'name' => $filename,
							'content' => base64_encode($a['content'])
						);
					}
				}
				$this->sniffMUA($msg_info['headers']);

				$body = $this->_filterBody($this->_body);
				$rid = null;
				if (getenv('MPDEBUG') || $this->_isDev()) {
					print $body;
					//die();
				}

				try {
					$rid = call_user_func(array($this->_soapClient, 'crm_append_ticket_via_email'), $hash, $body, $sender, $attachments);
				} catch (Exception $e) {
					print "EXCEPTION: " . $e->getMessage();
					print $this->_soapClient->__getLastResponse();
				}
				if (!$rid) {
					echo "Posting failed for $hash";
					exit(self::EFATAL);
				}
			} catch (Exception $e) {
				echo $e->getMessage();
				exit (self::EFATAL);
			}
			exit(0);
		}

		private function sniffMUA($headers) {
			if (isset($headers['x-google-dkim-signature'])) {
				$this->_mua = 'gmail';
				return true;
			}
			return -1;
		}
		private function _parseAttachment($content, $info)
		{
			$encoding = null;
			if (isset($info['transfer-encoding'])) {
				$encoding = $info['transfer-encoding'];
			}
			$content = $this->_decodeAttachment($content, $encoding);
			if ($info['content-type'] == 'text/plain' &&
				(!isset($info['content-disposition']) || $info['content-disposition'] != 'attachment')) {
				$this->_body .= $content;
			} elseif ($info['content-type'] == 'text/html' && empty($this->_body)) {
				$content = str_ireplace(array("<br />","<br>","<br/>"), "\r\n", $content);
				$this->_body .= strip_tags($content,"<br><code><strong><b><u>");
			} elseif (!empty($info['content-disposition']) && $info['content-disposition'] == 'attachment') {
				$this->_attachments[] = array('headers' => $info, 'content' => $content);
			} else { //inline attachments
				$this->_attachments[] = array('headers' => $info, 'content' => $content);
			}
		}

		/**
		 * Decode attachment using specific encoding process
		 *
		 * @param type $content
		 * @param type $encoding encoding type specified by transfer-encoding
		 */
		private function _decodeAttachment($content, $encoding) {
			switch(strtolower($encoding))
			{
				case 'base64':
					return base64_decode($content);
				case 'quoted-printable':
					$content = preg_replace('/^(.{74,})=$[\r\n]{1,2}^(.*)$/m', '$1$2', $content);
					return quoted_printable_decode($content);
				// 7 and 8-bit use no encoding mechanism
				// 8-bit, however, may contain non-ASCII chars
				case '8bit':
				case '7bit':
				case '':
					return $content;
			}

			echo "unknown encoding specified: " . $encoding;
			exit(self::EFATAL);
		}

		private function _postProcessBug($content) {
			if (substr($content,-2) != '--') {
				return false;
			}
			$x = strrpos($content,"\n");
			$border = substr($content, $x+1);
			$delimiter = '';
			$tok = strtok($border,'-');
			while ($tok !== false) {
				if ($tok != '-') {
					$delimiter .= $tok;
				}
				$tok = strtok('-');
			}
			// no boundary delimiter found
			if (!isset($delimiter[5])) return false;
			$line = strtok($content, "\r\n");
			$capture = false;
			$headers = $body = array();
			$n = 0;
			while ($line !== false) {
				if ($capture && isset($line[1]) && $line[0] == '-' && $line[1] == '-'
					&& strstr($line, $delimiter)) {
					$capture = false;
					$n++;
				}
				// parse headers
				if (isset($line[1]) && $line[0] == '-' && $line[1] == '-'
					&& strstr($line, $delimiter)) {
					$body[$n] = $headers[$n] = array();
					while (true) {
						$line = strtok("\r\n");
						if (!$line) {
							break;
						}
						$a = strpos($line,":");
						$b = strpos($line,"=");
						if ($a === false && $b === false) break;
						if (!$a || ($b > 0 && $a > $b)) $sep = "=";
						else $sep = ":";
						$tmp = explode($sep, $line);
						if (count($tmp) > 1) {
							list ($k,$v) = $tmp;
							$k = trim(strtolower($k));
							$headers[$n][$k] = trim($v," \";");
						}

					}
					$capture = true;
				}

				if ($capture) {
					$body[$n][] = $line;
				}

				$line = strtok("\r\n");
			}
			foreach ($headers as $k => $v) {
				$b = join("\r\n", $body[$k]);
				if (!$b) continue;
				$h = $headers[$k];
				if (isset($h['content-type'])) {
					if (!strncmp($h['content-type'],'text/plain',10))
						$h['content-type'] = 'text/plain';
					else if (!strncmp($h['content-type'], 'text/html', 9))
						$h['content-type'] = 'text/html';

				}
				if (!isset($h['transfer-encoding']) && isset($h['content-transfer-encoding'])) {
					$h['transfer-encoding'] = $h['content-transfer-encoding'];
				} else if (!isset($h['transfer-encoding'])) {

				}
				//echo "WTF";
				//$this->_parseAttachment($b, $h);
			}
			return isset($h);
		}

		private function _filterBody($body)
		{
			if (!$this->_isDev()) {
				if (!mb_check_encoding($body, 'UTF-8')) {
					$body = utf8_encode($body);
				}
			}

			// reflow flowed-text to one line
			if ($this->_mua === "gmail") {
				/**
				 * GMail doesn't join "On DDD, MMM, YYY <xyz@domain.com" + ">:"
				 */
				$body = preg_replace('/=?$[\r\n]{1,2}^([^:]+:)$/m', '$1', $body, 1);
			}

			$body = self::unfold_flowed($body);


			/*
			 * @TODO tokenize messages instead of regex
			 */
			$filtered = trim(preg_replace(array(
				// maildrop on v4 platforms delivers as \n
				'~\R~u',
				// consolidate flexible wordstops to whole lines
				'/<\s*$[\r\n]{1,2}^(?!>|)/m',
				/**
				 *  fringe case: mail client linebreaks on closing email bracket
				 *  Apis Networks Support <help@apisnetworks.com
				 *  > wrote:
				 */
				'/(?!>)$[\n\r]{1,2}>([^\n\r]*?):[\n\r]{1,2}$(?=^$^[\n\r]{1,2}>)/m',
				'/ $[\r\n]{1,2}^/m',
				// strip replies
				'/(^>.*+$(?=[\r\n]{1,2}>|$))/ms'),
				array("\r\n", '<', '>\1:',' ', ''),
				$body));

			if (substr($filtered,-1) == ':') {
				// mail should be delivered as \r\n ...
				$posunix = strrpos($filtered, "\n");
				$poswin  = strrpos($filtered, "\r");
				$pos = max($posunix, $poswin);
				if ($pos) $filtered = substr($filtered, 0, $pos-1);
			}
			// strip out signature if it exists
			$filtered = preg_replace('/^-{1,}\s*$[\r\n]{1,2}^.+$/ms', '', $filtered);
			// Outlook client
			$filtered = preg_replace('/^[[:alpha:]]+:\s*' . preg_quote(getenv('FROM_NAME') ?: self::CRM_FROM_NAME, '/') . '.*$/ms','', $filtered);
			return trim($filtered);
		}
	}

	if (!extension_loaded('mailparse') && function_exists('dl')) {
		// still available in CLI SAPI
		dl('mailparse.' . PHP_SHLIB_SUFFIX);
	}
	$msg = file_get_contents('php://stdin');
	$parser = new Parse_Ticket();
	$parsed = $parser->parse($msg);
