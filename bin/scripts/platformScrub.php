#!/usr/bin/env apnscp_php
<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
 */

include(__DIR__ . '/../../lib/CLI/cmd.php');

if (!\Lararia\JobDaemon::checkState()) {
	warn("Skipping integrity check, job runner inactive");
	exit(1);
}

$job = \Lararia\Jobs\Job::create(\Lararia\Jobs\MonthlyIntegrityJob::class);
$job->dispatch();
$job = null;
