<?php declare(strict_types=1);

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */


	namespace Module;

	use Module\Provider\Contracts\ProviderInterface;

	class Provider
	{
		use \NamespaceUtilitiesTrait;

		/**
		 * Get provider module implementation
		 *
		 * @param string          $module
		 * @param string          $provider
		 * @param \Auth_Info_User $user
		 * @return ProviderInterface
		 */
		public static function get(string $module, string $provider, \Auth_Info_User $user): ProviderInterface
		{
			return static::getClass($module, $provider, 'Module')::instantiateContexted($user);
		}

		/**
		 * Get class name from module provider
		 *
		 * @param string $module   provider type
		 * @param string $provider provider name
		 * @param string $class    class name of provider
		 * @return string
		 */
		public static function getClass(string $module, string $provider, string $class): string
		{
			$cmodule = studly_case($module);
			$custom = call_user_func("\Opcenter\\$cmodule::customLoader", $provider);
			if ($custom) {
				return $custom . '\\' . $class;
			}
			$parts = [
				'Opcenter',
				$cmodule,
				'Providers',
				studly_case($provider),
				$class
			];

			return implode('\\', $parts);
		}
	}