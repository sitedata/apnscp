<?php declare(strict_types=1);
	/*
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2022
	 */

	namespace Module\Support\Webapps;

	use Module\Support\Webapps\Traits\WebappUtilities;
	use Opcenter\Http\Php\Fpm\MultiPhp;
	use Opcenter\Php;

	class PhpWrapper
	{
		use \ContextableTrait;
		use \apnscpFunctionInterceptorTrait;
		use WebappUtilities;

		protected function __construct()
		{

		}

		public function exec(?string $path, string $cmd, array $args = [], array $env = []): array
		{
			// client may override tz, propagate to bin
			$cmd = $this->getPhpWrapper($path) . $cmd;

			$user = $this->getAuthContext()->username;
			if ($path) {
				$cmd = 'cd %(_chdir)s && ' . $cmd;
				if ($path[0] === '~') {
					$path = ($this->user_get_home($user) ?: '/') . substr($path, 1);
				}
				$args['_chdir'] = $path;
			}

			return $this->pman_run($cmd, $args, $env, ['user' => $user]);
		}

		/**
		 * Get PHP wrapper for executables
		 *
		 * @return string
		 */
		private function getPhpWrapper(string $path = null): string
		{
			$name = (string)$this->php_pool_name($path);
			$binary = MultiPhp::cliFromVersion($this->php_pool_get_version($name)) ?? Php::PHP_BIN;

			$poolPolicyMemory = $this->php_pool_get_policy('php_settings.memory_limit', $this->php_pool_name($path));
			$memory = (int)$poolPolicyMemory === -1 ? $poolPolicyMemory : max(
				512,
				\Formatter::changeBytes($poolPolicyMemory, 'M')
			) . 'M';

			$tz = $this->common_get_timezone();
			return $binary . ' -d display_errors=' . (is_debug() ? 'on' : 'off') .
				' -d mysqli.default_socket=' . escapeshellarg(ini_get('mysqli.default_socket')) .
				' -d date.timezone=' . $tz . ' -d memory_limit=' . $memory . ' ';
		}
	}