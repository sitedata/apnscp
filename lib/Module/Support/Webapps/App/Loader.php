<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2017
	 */

	namespace Module\Support\Webapps\App;

	use Module\Support\Webapps;
	use Module\Support\Webapps\MetaManager;

	/**
	 * Class Loader
	 *
	 * Webapp mediator
	 *
	 *
	 * @package apps\webapps
	 *
	 */
	abstract class Loader
	{
		use \NamespaceUtilitiesTrait;

		/**
		 * Create new webapp from docroot
		 *
		 * @param null|string     $type    webapp type
		 * @param null|string     $docroot document root (unresolved)
		 * @param \Auth_Info_User $context
		 * @return Webapps\App\Type\Unknown\Handler
		 */
		public static function fromDocroot(?string $type, ?string $docroot, \Auth_Info_User $context = null): Webapps\App\Type\Unknown\Handler
		{
			if (null === $type) {
				$type = array_get(MetaManager::factory($context ?? \Auth::profile())->get($docroot), 'type');
			} else if (!$type) {
				return error("Unknown/missing app type specified");
			}

			if ($type === 'webapp') {
				$type = 'unknown';
			}
			$c = Webapps::handlerFromApplication($type ?? 'unknown');

			if (!class_exists($c)) {
				fatal("unsupported web app type `%s'", $c);
			}

			return $c::factory($docroot, $context ?? \Auth::profile());
		}

		/**
		 * Create new webapp from hostname/path pair
		 *
		 * @param string|null          $type
		 * @param string               $hostspec hostname or docroot
		 * @param string               $path
		 * @param \Auth_Info_User|null $context
		 * @return Type\Unknown\Handler
		 */
		public static function fromHostname(?string $type, string $hostspec, string $path = '', \Auth_Info_User $context = null): Webapps\App\Type\Unknown\Handler
		{
			$context = $context ?? \Auth::profile();
			$afi = \apnscpFunctionInterceptor::factory($context);
			$docroot = $hostspec;
			if ($hostspec[0] !== '/') {

				$docroot = $afi->call('web_get_docroot', [$hostspec, $path]);
			}

			return static::fromDocroot($type, $docroot, $context)->setHostname($hostspec)->setPath($path);
		}

		public static function getKnownApps(): array
		{
			/**
			 * Localize apps per account per featureset in the future?
			 */
			return Webapps::knownApps();
		}

		public static function isApp(string $docroot, string $type, \Auth_Info_User $context = null): bool
		{
			$c = Webapps::handlerFromApplication($type);
			$class = (new \ReflectionClass($c))->newInstanceWithoutConstructor();
			if ($context) {
				$class->setContext($context);
			}
			if (!$class->display()) {
				return false;
			}
			return $class->detect($docroot);
		}
	}