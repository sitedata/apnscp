<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
 */

namespace Module\Support\Webapps\MetaManager;

use Illuminate\Contracts\Support\Arrayable;

class Options implements \ArrayAccess, Arrayable
{
	protected $options;

	public function __construct(array &$options = [])
	{
		$this->options = $options;
	}

	public function toArray(): array
	{
		return (array)$this->options;
	}

	public function __serialize(): array
	{
		return ['options' => \serialize($this->options)];
	}

	public function __unserialize(array $data): void
	{
		$this->options = isset($data['options']) ? \Util_PHP::unserialize($data['options']) : [];
	}


	/**
	 * Replace keys recursively
	 *
	 * @param array $options
	 * @return $this
	 */
	public function replace(array $options): self
	{
		$this->options = array_replace_recursive($this->options, $options);

		return $this;
	}

	/**
	 * Return copy of array of options differing from $values
	 *
	 * @param array $values
	 * @return array
	 */
	public function diff(array $values): array
	{
		return array_diff_assoc($values, array_intersect_key($this->options, $values));
	}

	/**
	 * Merge keys recursively appending same-keys
	 *
	 * @param array $options
	 * @return $this
	 */
	public function merge(array $options): self
	{
		$this->options = array_merge_recursive($this->options, $options);

		return $this;
	}

	#[\ReturnTypeWillChange]
	public function offsetExists($offset)
	{
		return isset($this->options[$offset]);
	}

	#[\ReturnTypeWillChange]
	public function offsetGet($offset)
	{
		return $this->options[$offset];
	}

	#[\ReturnTypeWillChange]
	public function offsetSet($offset, $value)
	{
		if ($offset === 'password') {
			// @todo debug references
			return;
		}
		$this->options[$offset] = $value;
	}

	#[\ReturnTypeWillChange]
	public function offsetUnset($offset)
	{
		unset($this->options[$offset]);
	}
}

