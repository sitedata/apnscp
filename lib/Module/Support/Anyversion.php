<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, December 2023
 */


namespace Module\Support;

use Module\Provider\Contracts\AnyversionInterface;
use Opcenter\Versioning;

abstract class Anyversion extends \Module_Skeleton implements AnyversionInterface
{
	protected const HASH_KEY = 'anyversion';

	protected $exportedFunctions = [
		'*'             => PRIVILEGE_SITE | PRIVILEGE_USER
	];

	/**
	 * Merge interface methods
	 *
	 * @return array
	 */
	public function getExportedFunctions(): array
	{
		$base = array_column((new \ReflectionClass(AnyversionInterface::class))->getMethods(\ReflectionMethod::IS_PUBLIC), 'name');

		return $this->exportedFunctions + array_fill_keys($base, PRIVILEGE_SITE | PRIVILEGE_USER);
	}

	/**
	 * Get any-version root
	 *
	 * @return null|string
	 */
	protected function environmentRoot(bool $reset = false): ?string
	{

		$cache = \Cache_Global::spawn();
		$module = \apnscpFunctionInterceptor::get_module_from_class(static::class);
		if (!$reset && false !== ($res = $cache->hGet(static::HASH_KEY, "$module.root"))) {
			return $res;
		}

		$bin = $this->shimScript();
		if (!file_exists($bin)) {
			return null;
		}

		$ret = \Util_Process_Safe::exec(['/bin/bash', '-ic', ". $bin ; {$this->shimExecutable()} root"]);
		if (!$ret['success']) {
			return nerror("Failed to query `%(what)s': %(err)s", [
				'what' => basename($bin),
				'err'  => coalesce($ret['stderr'], $ret['stdout'])
			]);
		}

		$cache->hSet('anyversion', "$module.root", $res = trim($ret['stdout']));

		return $res;
	}

	/**
	 * Execute any-version command within the scope of a version
	 *
	 * @param null|string $version
	 * @param null|string $pwd  pwd, defaults to ~
	 * @param string      $command
	 * @param array       $args optional command arguments
	 * @return array process output from pman_run
	 */
	public function do(?string $version, ?string $pwd, string $command, array $args = [], array $env = []): array
	{
		$envPrefix = strtoupper($this->shimExecutable());

		if ($version) {
			$env += [
				"{$envPrefix}_VERSION" => $version
			];
		}

		$ret = $this->exec($pwd, "exec $command", $args, $env);

		if (!$ret['success']) {
			// no job control warning
			error(coalesce($ret['stderr'], $ret['stdout']));
		}

		return $ret;
	}

	/**
	 * Run any-version wrapper command
	 *
	 * @param null|string $name
	 * @param null|string $command
	 * @param array       $args optional args
	 * @return array
	 */
	protected function exec(?string $pwd, string $command, array $args = [], array $env = []): array
	{
		if (!$pwd) {
			$pwd = '~';
		}

		$envPrefix = strtoupper($this->shimExecutable());
		$args['_CMD_PWD'] = $pwd ?? '~';
		$args['_BIN'] = $this->shimExecutable();
		$args['_SHIM_SCRIPT'] = $this->shimScript();

		$env += ["{$envPrefix}_ROOT" => $this->environmentRoot()];

		return $this->pman_run(
			'/bin/sh -c -- ' . escapeshellarg('[[ $(type -t %(_BIN)s) == function ]] || . %(_SHIM_SCRIPT)s && cd %(_CMD_PWD)s && %(_BIN)s ' . $command),
			$args,
			$env
		);
	}


	/**
	 * Any-version is installed
	 *
	 * @param string $version
	 * @return string|null installed ver
	 */
	public function installed(string $version, string $comparator = '='): ?string
	{
		$available = $this->list();

		foreach ($available as $alias => $localVersion) {
			if ($version === $localVersion ||
				(false !== strpos($localVersion, '.') && Versioning::compare($localVersion, $version, $comparator))) {
				return $localVersion;
			}
		}

		return null;
	}

	/**
	 * Assign any-version version to directory
	 *
	 * @param string $version
	 * @param string $path
	 * @return bool
	 */
	public function make_default(string $version, string $path = '~'): bool
	{

		$path .= "/.{$this->canonicalName()}-version";

		return $this->file_put_file_contents($path, $version);
	}

	/**
	 * Version inferred from path
	 *
	 * @param string $path
	 * @return string
	 */
	public function version_from_path(string $path): string
	{
		$controlFile = "{$path}/.{$this->canonicalName()}-version";

		if ($this->file_exists($controlFile)) {
			return trim($this->file_get_file_contents($controlFile));
		}

		$output = $this->exec($path, 'version');

		return $output['success'] ? strtok($output['stdout'], ' ') : 'system';
	}

	public function install(string $version): ?string
	{
		if (Versioning::asMajor($version) === $version || Versioning::asMinor($version) === $version) {
			$versions = array_filter($this->get_available(), static function ($v) use ($version) {
				return str_starts_with($v, $version);
			});

			$version = Versioning::maxVersion($versions, $version);
		}

		$ret = $this->exec(null, 'install %s', [$version]);
		if (!$ret['success']) {
			return nerror('failed to install %(lang)s %(version)s, error: %(err)s', [
				'lang' => $this->canonicalName(),
				'version' => $version,
				'err' => coalesce($ret['stderr'], $ret['stdout'])
			]);
		}

		return $version;
	}

	/**
	 * Get available Rubies
	 *
	 * @return array
	 */
	public function get_available(): array
	{
		$cache = \Cache_Super_Global::spawn();
		$key = $this->canonicalName() . '.rem';
		if (false !== ($res = $cache->get($key))) {
			return $res;
		}
		$flag = $this->shimExecutable() === 'rbenv' ? '-L' : '-l';
		$ret = $this->exec(null, 'install ' . $flag);
		if (!$ret['success']) {
			error('failed to query %(lang)s - is %(bin)s installed?', [
				'lang' => $this->canonicalName(),
				'bin' => $this->shimExecutable()
			]);

			return [];
		}
		$versions = [];
		strtok($ret['output'], "\n ");
		while (false !== ($version = strtok("\n "))) {
			$versions[] = $version;
		}

		$cache->set($key, $versions);

		return $versions;
	}

	/**
	 * List installed any-version versions
	 *
	 * @return array
	 */
	public function list(): array
	{
		// RC 3 = no versions installed
		$ret = $this->exec(null, 'versions');
		$versions = [];
		if (preg_match_all('/^(?>(?<default>\S+)|\s+)\s*(?<version>\S+)(?>$|\s*)(?<misc>[^\r\n]*)$/m',
			$ret['output'], $matches, PREG_SET_ORDER)) {
			foreach ($matches as $v) {
				$versions[] = $v['version'];
				if (isset($v['default'])) {
					$versions['active'] = $v['version'];
				}
			}
		}
		return $versions;
	}

	/**
	 * Remove an installed any-version
	 *
	 * @param string $version
	 * @return bool
	 */
	public function uninstall(string $version): bool
	{

		$ret = $this->exec(null, 'uninstall -f %s', [$version]);
		if (!$ret['success']) {
			return error('failed to uninstall %(lang) %(version)s: %(err)s', [
				'lang' => strtoupper($this->canonicalName()),
				'version' => $version,
				'err' => coalesce($ret['stderr'], $ret['stdout'])
			]);
		}

		return true;
	}

	private function canonicalName(): string
	{
		$name = \apnscpFunctionInterceptor::get_module_from_class(static::class);
		return match ($name) {
			'ruby', 'go', 'python', 'node' => $name,
			default => fatal("Unknown any-version")
		};
	}

	private function shimScript(): string
	{
		return '/etc/profile.d/' . $this->shimExecutable() . '.sh';
	}

	private function shimExecutable(): string
	{
		return match(\apnscpFunctionInterceptor::get_module_from_class(static::class)) {
			'ruby' => 'rbenv',
			'go' => 'goenv',
			'python' => 'pyenv',
			'node' => 'nvm',
			default => fatal("Unknown any-version")
		};
	}
}