<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2022
 */

namespace Lararia\View;

use Illuminate\View\FileViewFinder;
use InvalidArgumentException;
use Lararia\NamespaceResolverTrait;

class NamespacedViewFinder extends FileViewFinder
{
	use NamespaceResolverTrait;

	/**
	 * Get the segments of a template with a named path.
	 *
	 * @param string $name
	 * @return array
	 *
	 * @throws \InvalidArgumentException
	 */
	protected function parseNamespaceSegments($name)
	{
		$segments = explode(static::HINT_PATH_DELIMITER, $name);

		if (count($segments) !== 2) {
			throw new InvalidArgumentException("View [{$name}] has an invalid name.");
		}

		if (isset($this->hints[$segments[0]])) {
			return $segments;
		}

		if (!($path = $this->resolveDynamicNamespace($segments[0]))) {
			throw new InvalidArgumentException("No hint path defined for [{$segments[0]}].");
		}

		$this->addNamespace($segments[0], [
			config_path('custom/' . $path),
			base_path($path)
		]);

		return $segments;
	}
}