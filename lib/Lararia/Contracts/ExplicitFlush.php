<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
 */

namespace Lararia\Contracts;

interface ExplicitFlush {
	/**
	 * Trigger sync on write in array operations
	 */
	public function setDirtyFlag(): void;

	/**
	 * Synchronize dirty buffer
	 *
	 * @return mixed
	 */
	public function sync();
}
