<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2022
 */

	namespace Lararia\Routing;

	use Illuminate\View\ViewFinderInterface;
	use Lararia\NamespaceResolverTrait;

	class NamespacedRouteCollection extends \Illuminate\Routing\RouteCollection
	{
		const NAMESPACE_DELIMITER = ViewFinderInterface::HINT_PATH_DELIMITER;

		private array $resolved = [];

		use NamespaceResolverTrait;

		public function getByName($name)
		{
			if (null !== ($route = parent::getByName($name)) || $name[0] !== '@') {
				return $route;
			}

			[$ns, $route] = explode(self::NAMESPACE_DELIMITER, $name, 2);
			if (isset($this->resolved[$ns])) {
				return null;
			}

			$this->resolved[$ns] = 1;
			$parameter = null;
			$resolved = $this->resolveDynamicNamespace($ns, $parameter);
			if (!isset($resolved[0]['as'])) {
				$resolved[0]['as'] = "$ns($parameter)::";
			}
			app('router')->group(...$resolved);
			$this->refreshNameLookups();

			return $this->getByName($name);
		}

	}