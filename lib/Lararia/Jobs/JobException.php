<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
 */

namespace Lararia\Jobs;

class JobException extends \RuntimeException {
	// @var string error report backtrace
	protected $erbt;

	public function setApiTrace(?string $bt) {
		$this->erbt = (string)$bt;
		return $this;
	}

	public function getApiTrace(): string {
		return $this->erbt;
	}
}