<?php

	namespace Lararia\Providers;

	use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
	use Lararia\Jobs\Listeners\JobSubscriber;

	class EventServiceProvider extends ServiceProvider
	{
		/**
		 * The event listener mappings for the application.
		 *
		 * @var array
		 */
		protected $listen = [
		];

		protected $subscribe = [
			JobSubscriber::class
		];

		/**
		 * Register any events for your application.
		 *
		 * @return void
		 */
		public function boot()
		{
			parent::boot();

			//
		}
	}
