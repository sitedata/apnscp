<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2017
	 */

	declare(strict_types=1);

	namespace Lararia;

	use Laravel\Horizon\Contracts\SupervisorRepository;
	use Opcenter\Apnscp;
	use Opcenter\Process;
	use Queue;

	/**
	 * Class JobDaemon
	 *
	 * @package Lararia
	 */
	class JobDaemon implements \Util\Process\Contracts\StandaloneService
	{
		const QUEUE_ORDER = 'high,default';
		const PID_FILE = 'run/horizon.pid';
		/**
		 * @var \JobDaemon instance
		 */
		protected static $instance;
		private $pid;

		protected function __construct()
		{
			self::shimPanelPhp();
		}

		/**
		 * Add apnscp PHP path for Horizon
		 *
		 * @return void
		 */
		public static function shimPanelPhp(): void
		{
			$phpbin = INCLUDE_PATH . '/bin/php-bins';
			$path = (string)getenv('PATH');
			if (0 !== strpos($path, $phpbin . PATH_SEPARATOR)) {
				putenv('PATH=' . $phpbin . PATH_SEPARATOR . getenv('PATH'));
			}
		}

		/**
		 * Launch Horizon manager
		 *
		 * @return bool
		 */
		public function start(): bool
		{
			$this->kill();
			return $this->launch() && $this->restartOnFailure();
		}

		public static function get(): self
		{
			if (null === self::$instance) {
				self::$instance = new static;
			}

			return self::$instance;
		}

		/**
		 * Kill Horizon processes
		 *
		 * @return bool
		 */
		public function kill(): bool
		{
			$cmd = [
				PHP_BINARY,
				INCLUDE_PATH . DIRECTORY_SEPARATOR .
				'artisan', 'horizon:terminate'
			];
			$ret = \Util_Process::exec($cmd);

			return $ret['success'];
		}

		/**
		 * Launch Horizon daemon
		 *
		 * @return bool
		 */
		protected function launch(): bool
		{

			$cmd = $this->getCommandName();
			$pid = array_get(\Util_Process_Fork::exec($cmd, null, [0], ['euser' => APNSCP_SYSTEM_USER]), 'return', false);

			if (!$pid) {
				return error('failed to launch Horizon process');
			}
			$this->savePid($pid);
			return $pid > 0;
		}

		protected function restartOnFailure(): bool
		{
			pcntl_async_signals(true);

			pcntl_signal(SIGCHLD, function (int $signo, array $signinfo) {
				static $count = 0;
				$status = null;
				$relaunch = false;
				do {
					$pid = pcntl_waitpid(-1, $status, WNOHANG);
					if ($this->pid !== $pid || !pcntl_wifexited($status) ||
						pcntl_wtermsig($status) === SIGKILL)
					{
						continue;
					}

					$relaunch = true;
				} while ($pid > 0);

				// processes exited, no relaunch or has_low_memory
				if (static::isStandalone() || !$relaunch) {
					return true;
				}
				// this process may orphan if apnscpd terminates unexpectedly
				// verify it hasn't been promoted to session leader (apnscpd died) and
				// that apnscpd is still running
				if (!Apnscp::running() || !Process::pidMatches(posix_getsid(posix_getpid()), PHP_BINARY)) {
					dlog('Controlling apnscpd process shutdown - terminating job worker PID %d', posix_getpid());
					exit;
				}
				// collect exit
				\assert($status === $signinfo['status'], 'Signal status matches waitpid exit status');
				warn('Job worker died (PID %d) - restarting in 30 seconds. Count %d', $this->pid, ++$count);
				if ($count > 100) {
					dlog('Too many daemon restarts - exiting');
					return false;
				}
				sleep(30);
				return $this->launch();
			});
			return true;
		}

		/**
		 * Get job runner command name
		 *
		 * @return string
		 */
		public function getCommandName(): string {
			$cmd = PHP_BINARY . ' ' . INCLUDE_PATH . DIRECTORY_SEPARATOR . 'artisan ';
			if (!static::isStandalone()) {
				// queue:listen works different, --timeout=n applies to all jobs processed
				// and does not respect job $timeout property!
				return $cmd . 'queue:work --queue=' . static::QUEUE_ORDER;
			}
			return $cmd . 'horizon';
		}

		/**
		 * Job daemon runs as own process
		 *
		 * @return bool
		 */
		public static function isStandalone(): bool
		{
			return !CRON_LOW_MEMORY;
		}

		/**
		 * Save Horizon process ID
		 *
		 * @param int $pid
		 * @return bool
		 */
		protected function savePid(int $pid): bool
		{
			$this->pid = $pid;
			$file = storage_path(self::PID_FILE);

			return (bool)file_put_contents($file, $pid);
		}

		/**
		 * Perform a queue snapshot of Horizon
		 *
		 * @return bool
		 */
		public static function snapshot(): bool
		{
			if (!static::isStandalone()) {
				return true;
			}
			$ret = \Util_Process::exec([PHP_BINARY, INCLUDE_PATH . DIRECTORY_SEPARATOR . 'artisan', 'horizon:snapshot']);

			return $ret['success'];
		}

		/**
		 * Check job runner state
		 *
		 * @return bool
		 */
		public static function checkState(): bool
		{
			if (static::get()->running()) {
				return true;
			}
			if (CRON_NO_DEBUG && is_debug()) {
				return warn('Panel currently in debug mode. Job runner may not be active in debug mode. ' .
					'Disable debug mode or run job runner manually via %s', JobDaemon::get()->getCommandName());
			} else {
				warn('Job runner inactive. Tasks will not process. Manually start job runner via %s',
					JobDaemon::get()->getCommandName());
			}
			return false;
		}

		public function once(): bool
		{
			$cmd = [
				PHP_BINARY,
				INCLUDE_PATH . DIRECTORY_SEPARATOR . 'artisan',
				'queue:work',
				'--once',
				'--queue=' . static::QUEUE_ORDER
			];
			$ret = \Util_Process::exec($cmd);

			return $ret['success'];
		}

		/**
		 * Verify Horizon running
		 *
		 * @return bool
		 */
		public function running(): bool
		{
			$pid = $this->getPid();

			if ($pid && Process::pidMatches($pid, PHP_BINARY)) {
				return true;
			}

			// check if Horizon socket available, ./artisan horizon
			// query PID from worker & update
			// queue:work can't determine the PID without checking process table
			if (!file_exists(run_path('redis.sock'))) {
				return false;
			}
			$repository = app(SupervisorRepository::class);

			if (!$supervisors = $repository->all()) {
				return false;
			}
			if (null === ($newpid = data_get($supervisors, '0.pid'))) {
				return false;
			}
			debug('PID for Horizon lists %d, but discovered %d. Updated PID.', $pid, $newpid);
			$this->savePid((int)$newpid);

			return $this->running();
		}

		/**
		 * Get Horizon PID
		 *
		 * @return int|null
		 */
		public function getPid(): ?int
		{
			$file = storage_path(self::PID_FILE);
			if (!file_exists($file)) {
				return null;
			}

			return (int)file_get_contents($file);
		}

		/**
		 * @param bool $force
		 * @return bool
		 */
		public function stop(bool $force = false): bool
		{
			if (!$this->running()) {
				return true;
			}
			$pid = $this->getPid();

			return Process::kill($pid, $force ? SIGKILL : SIGTERM);
		}
	}
