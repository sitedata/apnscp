<?php
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */

	declare(strict_types=1);

	namespace Lararia\Mail;

	use Illuminate\Mail\Mailable;
	use Illuminate\Queue\SerializesModels;

	class BandwidthDigest extends Mailable
	{
		use SerializesModels;
		protected $args;

		/**
		 * Create a new message instance.
		 *
		 * @param array $args
		 */
		public function __construct(array $args)
		{
			$this->args = $args;
		}

		public function build()
		{
			$subject = $this->getSubject();
			return $this->markdown('email.bandwidth.admin.overage-digest',
				['items' => $this->args]
			)->subject($subject);
		}

		private function getSubject(): string
		{
			return 'Bandwidth digest [' . SERVER_NAME_SHORT . ']';
		}
	}