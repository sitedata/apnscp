<?php

	namespace Lararia\Console\Commands;

	use Illuminate\Database\Console\Migrations\StatusCommand as StatusBase;
	use Illuminate\Database\Migrations\Migrator;
	use Illuminate\Support\Collection;
	use Symfony\Component\Console\Input\InputOption;

	class StatusCommand extends StatusBase
	{
		private array $migrators;

		public function __construct()
		{
			$migrator = app('migrator');
			parent::__construct($migrator);

			$this->migrators = [
				'db' => $this->migrator,
				'platform' => app('platformmigrator')
			];
		}

		protected function getOptions()
		{
			return parent::getOptions() + append_config([
					['dbonly', null, InputOption::VALUE_NONE, 'Show database migration only.'],

					[
						'platform',
						null,
						InputOption::VALUE_NONE,
						'Show platform migrations only.'
					],

					[
						'pending',
						null,
						InputOption::VALUE_NONE,
						'Show pending migrations.'
					],
				]);
		}


		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			if ($this->option('platform')) {
				$this->migrator = $this->migrators['platform'];
			}

			if (!$this->option('pending')) {
				return parent::handle();
			}

			$ran = [
				'db' => $this->migrators['db']->getRepository()->getRan(),
				'platform' => $this->migrators['platform']->getRepository()->setType('platform')->getRan()
			];

			return $this->output->writeln(Collection::make($this->getAllMigrationFiles())->filter(function ($migration) use ($ran) {
				$migrationName = $this->migratorFromFile($migration)->getMigrationName($migration);
				return !in_array($migrationName, str_ends_with($migration, '.yml') ? $ran['platform'] : $ran['db'], true);
			}));

		}

		private function migratorFromFile(string $file): Migrator
		{
			return str_ends_with($file, '.yml') ? $this->migrators['platform'] : $this->migrators['db'];
		}

		protected function getStatusFor(array $ran, array $batches)
		{
			if (!$this->option('dbonly')) {
				$ran = array_merge($ran, app('platformmigrator')->getRepository()->getRan());
			}

			return Collection::make($this->getAllMigrationFiles())
				->map(function ($migration) use ($ran, $batches) {
					$migrationName = $this->migratorFromFile($migration)->getMigrationName($migration);

					return in_array($migrationName, $ran, true)
						? ['<info>Yes</info>', $migrationName, $batches[$migrationName]]
						: ['<fg=red>No</fg=red>', $migrationName];
				});
		}


		protected function getAllMigrationFiles()
		{
			if ($this->option('dbonly')) {
				return parent::getAllMigrationFiles();
			}

			if ($this->option('platform')) {
				return app('platformmigrator')->getMigrationFiles($this->getPlaybookMigrationPath());
			}

			return array_merge(parent::getAllMigrationFiles(), app('platformmigrator')->getMigrationFiles($this->getPlaybookMigrationPath()));
		}

		protected function getPlaybookMigrationPath()
		{
			if ($path = $this->option('path')) {
				return !$this->usingRealPath() ? resource_path($path) : $path;
			}

			return resource_path('playbooks/migrations');
		}
	}
