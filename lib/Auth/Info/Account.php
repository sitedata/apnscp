<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	class Auth_Info_Account
	{
		const CACHE_KEY = 'info:svc';
		const CACHE_DURATION = 7200;

		public $conf;
		public $new;
		public $cur;
		public $old;
		public $site;
		public $gen;
		// cheat to avoid round-trip check
		public $active = true;

		private bool $volatile = false;
		public function __construct($site = null)
		{
			// timestamp of creation
			$this->gen = time();
			$this->site = $site;
		}

		public static function flushCache(\Auth_Info_User $context): void
		{
			$c = Cache_Global::spawn($context);
			$c->del(self::cacheKeyFromContext($context->site));
		}

		private static function cacheKeyFromContext(string $site): string
		{
			return self::CACHE_KEY . ':' . $site;
		}

		public function __clone() {
			$this->volatile = true;
		}

		public static function factory(\Auth_Info_User $context): ?self
		{
			if (!$context->site) {
				return null;
			}

			// pull from cache if available
			$c = Cache_Global::spawn($context);
			$svc = $c->get(self::cacheKeyFromContext($context->site));

			if ($svc) {
				$svc = \Error_Reporter::silence(static function () use ($svc) {
					return \Util_PHP::unserialize($svc, [__CLASS__]);
				});
				if ($svc && $svc->site !== $context->site) {
					report('Serialized data failed sanity test? Prefix: %s. Expected %s, got: %s',
						$c->getOption(Redis::OPT_PREFIX),
						$context->site,
						var_export($svc, true)
					);
					$svc = null;

				}
			}
			/**
			 * mtime is updated on a directory when files are created, removed
			 * or renamed within a directory. new/ always houses pending changes
			 * stat this directory to see if changes have or are about to occur
			 *
			 * (NB: truncate + write does not alter mtime)
			 */
			$path = $context->domain_info_path();
			clearstatcache(true, $path);
			if (!file_exists($path)) {
				fatal("Unknown or invalid site `%s'", $context->site);
			}
			$mtime = filemtime($path);
			if (!\is_object($svc) || ($mtime > $svc->gen)) {
				$svc = new static($context->site);

				return $svc->reset($context);
			}

			return $svc;
		}


		public function reset(\Auth_Info_User $context = null): self
		{
			/* prevent deadlock on held Memcached by import() */
			$ds = DataStream::get($context);
			$ds->setOption(apnscpObject::UNCHARGED);
			$conf = $ds->query('common_get_services');
			if (!$conf) {
				return $this;
			}
			$s = array_keys($conf);
			$this->gen = time();
			[$this->cur, $this->new, $this->old, $this->active] =
				$ds->multi('common_get_current_services', $s)->
				multi('common_get_new_services', $s)->
				multi('common_get_old_services', $s)->
				multi('auth_is_inactive')->sendMulti();
			$this->conf = $conf;
			$this->active ^= 1;

			// new data may be journaled separately to info/new/
			// waiting on EditVirtDomain call (adding domain via Addon Domains)
			$this->_saveChanges();

			return $this;
		}

		private function _saveChanges()
		{
			$c = Cache_Global::spawn();

			return $c->set(
				self::cacheKeyFromContext($this->site),
				serialize($this),
				self::CACHE_DURATION
			);

		}

		public function change($svc, $conf, $inactive = 0): self
		{
			if (!$inactive) {
				$type = &$this->cur;
			} else {
				$type = &$this->new;
			}
			$type[$svc] = array_merge((array)($type[$svc] ?? []), $conf);
			$this->conf[$svc] = $type[$svc];
			if (!$this->volatile) {
				$this->_saveChanges();
			}

			return $this;
		}

		public function changeMulti($conf, $inactive = 0)
		{
			if (!$inactive) {
				$type = &$this->cur;
			} else {
				$type = &$this->new;
			}
			$type = array_replace_recursive($type, $conf);

			return $this->_saveChanges();
		}
	}