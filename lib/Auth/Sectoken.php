<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace Auth;

use Opcenter\Auth\Password;

class Sectoken
{
	public const SECURITY_TOKEN = 'sectoken';

	/**
	 * @var string token
	 */
	private $token;

	use \apnscpFunctionInterceptorTrait;
	use \ContextableTrait;

	public function __toString()
	{
		return $this->token ?? $this->create();
	}

	protected function __construct()
	{
		$this->token = $this->get();
	}

	/**
	 * Security token exists
	 *
	 * @return bool
	 */
	public function exists(): bool
	{
		return $this->token !== null;
	}

	/**
	 * Get security token
	 *
	 * @return string|null
	 */
	public function get(): ?string
	{
		$prefs = \Preferences::factory($this->getAuthContext());
		return array_get($prefs, self::SECURITY_TOKEN);
	}

	/**
	 * Create security token
	 *
	 * @return string
	 */
	public function create(): string
	{
		$key = $this->getAuthContext()->username;
		if ($this->getAuthContext()->level & (PRIVILEGE_SITE|PRIVILEGE_USER)) {
			$key = $this->getApnscpFunctionInterceptor()->billing_get_invoice();
		}

		if (!$key) {
			// I give up
			$key = Password::generate(32);
		}

		$this->token = $this->getApnscpFunctionInterceptor()->auth_crypt($key) ?: null;
		\Preferences::factory($this->getAuthContext())->
			unlock($this->getApnscpFunctionInterceptor())->offsetSet(self::SECURITY_TOKEN, $this->token);

		return $this->token;
	}

	/**
	 * Validate token against known
	 *
	 * @param string $challenge
	 * @return bool
	 */
	public function check(string $challenge = ''): bool
	{
		if (!$this->exists()) {

			$this->create();
			return true;
		}

		return $challenge === $this->token;
	}

	/**
	 * Hash value
	 *
	 * @param string $val
	 * @return string
	 */
	public function hash(string $val): string
	{
		return hash_hmac('sha512', $val, (string)$this);
	}

	/**
	 * Hashed value matches
	 *
	 * @param string $hashed
	 * @param string $val
	 * @return bool
	 */
	public function verifyHash(string $hashed, string $val): bool
	{
		return $hashed === hash_hmac('sha512', $val, (string)$this);
	}
}

