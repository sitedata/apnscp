<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */

class ModuleCache extends ArrayObject
{
	const TYPE_PERMISSION = 'p';
	const TYPE_MINARG = 'm';
	const TYPE_MAXARG = 'q';
	const TYPE_FUNCTIONS = 'f';
	const TYPE_INSTANCE = 'i';

	public function getModule(string $module): \Module_Skeleton
	{
		return $this[$module][self::TYPE_INSTANCE];
	}

	public function __destruct()
	{
		$this->cleanDynamicCompositions();
	}

	public function forget(string $module): void
	{
		unset($this[$module]);
	}

	public function serialize(): string
	{
		$this->cleanDynamicCompositions();

		return parent::serialize();
	}

	public function cleanDynamicCompositions(): void
	{
		foreach ($this->getIterator() as $module => $data) {
			if (empty($data[self::TYPE_INSTANCE])) {
				continue;
			}
			$instance = $this->getModule($module);
			$rfxn = new ReflectionClass($instance);
			if (empty($data['swap']) && $rfxn->isAnonymous()) {
				// can't serialize swap() statements yet
				// breaks unit tests such as GitTest::testPartialRollbackFeature
				$this[$module] = null;
			}
		}
	}
}