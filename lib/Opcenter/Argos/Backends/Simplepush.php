<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Simplepush extends Backend
	{
		protected $key;
		protected $event;

		public function getAuthentication()
		{
			return $this['key'];
		}

		public function setAuthentication(...$vars): bool
		{
			$this['key'] = $vars[0];

			return true;
		}
	}