<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace Opcenter\Argos\Backends;

	use Opcenter\Argos\Backend;

	class Pushover extends Backend
	{
		protected $userKey;
		protected $apiToken;
		protected $title;
		/**
		 * @var int priority between -2 and 2
		 */
		protected $priority;
		/**
		 * @var int expiration in seconds for high priority
		 */
		protected $expire;
		/**
		 * @var int retry retry in seconds for high priority events
		 */
		protected $retry;

		/**
		 * @var string
		 */
		protected $sound;
		protected $callback;
		/**
		 * @var string device name
		 */
		protected $device;
		protected $url;
		protected $urlTitle;
		/**
		 * @var bool send as HTML
		 */
		protected $html;

		public function getAuthentication()
		{
			return $this['user_key'];
		}

		public function setAuthentication(...$vars): bool
		{
			$this['user_key'] = $vars[0];

			return true;
		}

		public function setApiToken($val): bool
		{
			$this->apiToken = $val;

			return true;
		}

		public function setPriority(int $priority)
		{
			if ($priority < -2 || $priority > 2) {
				return error('Priority out of range [-2, 2]');
			}
			$this->priority = $priority;
		}

		public function setRetry(int $retry)
		{
			if ($retry > 7200) {
				warn('Retry cannot exceed 3600 seconds, capping to 3600');
				$retry = 3600;
			} else if ($retry < 30) {
				warn('Retry cannot be less than 30 seconds, setting to 30 seconds');
				$retry = 30;
			}
			$this->retry = $retry;
		}

		public function setHtml(bool $yes)
		{
			$this->html = $yes;
		}

	}