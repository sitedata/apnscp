<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2019
 */


namespace Opcenter\Net;

class Protocol {
	public const UDP = 'udp';
	public const TCP = 'tcp';
	public const BOTH = 'both';

	/**
	 * Get port from /etc/services
	 *
	 * @param string $service service name
	 * @param string $protocol protocol
	 * @return int|null
	 */
	public static function getPortFromService(string $service, string $protocol = self::BOTH): ?int {
		foreach ((array)static::normalizeProtocol($protocol) as $proto) {
			if (false !== ($svc = getservbyname($service, $proto))) {
				return $svc;
			}
		}

		return null;
	}

	/**
	 * Get service from port in /etc/services
	 *
	 * @param int    $port
	 * @param string $protocol
	 * @return null|string
	 */
	public static function getServiceFromPort(int $port, string $protocol = self::BOTH): ?string {
		foreach ((array)static::normalizeProtocol($protocol) as $proto) {
			if (false !== ($svc = getservbyport($port, $proto))) {
				return $svc;
			}
		}

		return null;
	}

	/**
	 * Validate protocol
	 *
	 * @param string $protocol
	 * @return string|array|null
	 */
	public static function normalizeProtocol(string $protocol)
	{
		$protocol = strtolower($protocol);
		if ($protocol === self::BOTH) {
			return [self::TCP, self::UDP];
		}

		if ($protocol !== self::TCP && $protocol !== self::UDP) {
			error("Unknown protocol `%s' specified", $protocol);
			return null;
		}
		return $protocol;
	}
}