<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */


	namespace Opcenter\Net\Firewall\Ipset;

	use Opcenter\Net\Firewall\Contracts\FirewallInterface;
	use Opcenter\Net\Firewall\Ipset;

	class FirewallConnector implements FirewallInterface
	{
		public static function getEntriesFromGroup(string $group = null): array
		{
			if (!$group) {
				$groups = array_fill_keys(self::groups(), []);
			} else {
				$groups = [
					$group => []
				];
			}
			foreach (array_keys($groups) as $gname) {
				$groups[$gname] = [];
				foreach (Ipset::getSetMembers($gname) as $set) {
					$groups[$gname][] = new Rule([
						'group'   => $gname,
						'host'    => $set['host'],
						'timeout' => $set['timeout']
					]);
				}
			}

			return $group ? $groups[$group] ?? [] : $groups;
		}

		public static function groups(): array
		{
			return Ipset::sets();
		}

		public static function insert(\Opcenter\Net\Firewall\Rule $rule): bool
		{
			return self::append($rule);
		}

		public static function append(\Opcenter\Net\Firewall\Rule $rule): bool
		{
			return Ipset::add($rule->getGroup(), $rule->getHost(), $rule->getTimeout());
		}

		public static function remove(\Opcenter\Net\Firewall\Rule $rule): bool
		{
			return Ipset::remove($rule->getGroup(), $rule->getHost());
		}

	}