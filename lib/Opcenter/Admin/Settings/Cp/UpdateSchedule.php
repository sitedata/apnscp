<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
	 */

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class UpdateSchedule implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();
			if (!$val || $val === 'system') {
				$cfg['apnscp_nightly_update'] = (bool)$val;
			} else {
				// timespec... hope it works!
				// systemd-analyze calendar available in RHEL8
				$cfg['apnscp_nightly_update'] = $val;
			}
			unset($cfg);
			Bootstrapper::run('apnscp/install-services', 'apnscp/crons');

			return true;
		}

		public function get()
		{
			$config = new Config();

			$val = $config['apnscp_nightly_update'];
			if ($val === true) {
				return 'system';
			} else if (!$val) {
				return null;
			}
			return $val;
		}

		public function getHelp(): string
		{
			return 'Set cp update window';
		}

		public function getValues()
		{
			return ['system', 'null', 'calendar spec'];
		}

		public function getDefault()
		{
			return 'system';
		}
	}