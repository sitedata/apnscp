<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Cp;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class WhitelistAccess implements SettingsInterface
	{
		const KEY = 'always_permit_panel_login';
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Config();
			$cfg[self::KEY] = (bool)$val;
			unset($cfg);
			Bootstrapper::run('network/setup-firewall');

			return true;
		}
		public function get()
		{
			$cfg = new Config();
			return array_get($cfg, self::KEY, $this->getDefault());
		}

		public function getHelp(): string
		{
			return 'Always permit panel access';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
