<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mail;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SpamDeletionThreshold implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!is_int($val) && !is_float($val) || $val < 1) {
				return error("Invalid deletion threshold");
			}

			if ($val < ($default = $this->getDefault())) {
				warn("Value less than default `%d'", $default);
			}

			(new Config)->offsetSet('maildrop_delete_threshold', $val);
			Bootstrapper::run('mail/maildir');

			return true;
		}

		public function get()
		{
			$config = new Config;
			if ($val = $config['maildrop_delete_threshold']) {
				return $val;
			}

			return $this->getDefault();
		}

		public function getHelp(): string
		{
			return 'Set score limitation for spam deletion';
		}

		public function getValues()
		{
			return 'int';
		}

		public function getDefault()
		{
			return (new SpamFilter)->get() === 'rspamd' ? 25 : 7;
		}

	}