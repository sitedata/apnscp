<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Dns;

	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class PowerdnsVersion implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}
			if (!in_array($val, $this->getValues(), false)) {
				return error("Invalid %(brand)s value: %(val)s", ['brand' => 'PowerDNS', 'val' => $val]);
			}
			$cfg = new Config();
			$cfg['powerdns_enabled'] = (bool)$val;
			if (!$val) {
				info("Disabling PowerDNS server on this machine");
			} else if ($cfg['powerdns_version'] === true) {
				// revert to default
				unset($cfg['powerdns_version']);
			} else {
				$cfg['powerdns_version'] = $val;
			}
			$cfg->sync();

			\Opcenter\Admin\Bootstrapper::run('software/powerdns', ['force' => true]);
			return true;
		}

		public function get()
		{
			$cfg = new Config();
			if (!$cfg['powerdns_enabled']) {
				return null;
			}
			return (string)($cfg['powerdns_version'] ?? $this->getDefault());
		}

		public function getHelp(): string
		{
			return 'Installed PowerDNS server version';
		}

		public function getValues()
		{
			return ['4.1','4.2','4.3','4.4','4.5','4.6', null];
		}

		public function getDefault()
		{
			$cfg = new Config();
			$supportRole = array_get(
				$cfg->loadRole('software/powerdns'),
				'powerdns_module_path',
				'software/powerdns-support'
			);
			return (string)array_get($cfg->loadRole($supportRole), 'powerdns_version', '4.4');
		}

	}
