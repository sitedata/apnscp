<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\Dns;

	use IPTools\Range;
	use Lararia\Jobs\SimpleCommandJob;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\Net\Iface;

	class Ip4Pool implements SettingsInterface
	{
		use \NamespaceUtilitiesTrait;

		const HARD_LIMIT = 64;

		public function get(...$var)
		{
			$pool = '\\Opcenter\\Net\\' . $this->getPoolType();
			return $pool::nb_pool();
		}

		/**
		 * Set pool
		 *
		 * @param       $val
		 * @return bool
		 */
		public function set($val): bool
		{
			if (!\is_array($val)) {
				$val = [$val];
			}

			if ($val === $this->get()) {
				return true;
			}

			$pool = '\\Opcenter\\Net\\' . $this->getPoolType();
			$addrs = [];

			foreach ($val as $v) {
				// expand /n CIDR notation
				if (false !== strpos($v, '/')) {
					$cidr = $v;
					$v = Range::parse($cidr);
					$v = array_map(function ($ip) { return (string)$ip; }, iterator_to_array($v));
					warn("Expanding CIDR notation in pool - %(count)d IPs detected in %(cidr)s", [
						'count' => \count($v),
						'cidr'  => $cidr
					]);
					if (\count($v) > self::HARD_LIMIT) {
						return error("Expanded CIDR exceeds hard limit %d - probably a typo. Bailing.", self::HARD_LIMIT);
					}
				}

				foreach ((array)$v as $addr) {
					if (!$pool::valid($addr)) {
						return error("Invalid %s address specified `%s'", $pool, $addr);
					}
					if (!Iface::bound($addr)) {
						return error("IP address `%s' is not bound to any interface on the server", $addr);
					}

					$addrs[] = $addr;
				}
			}

			$path = \Opcenter::mkpath($pool::NB_POOL);
			if (file_put_contents($path, implode("\n", $addrs)) === false) {
				return false;
			}

			$context = \Auth::context(\Auth::get_admin_login());
			\Opcenter\Admin\Bootstrapper::job('apnscp/bootstrap','apache/configure', 'network/hostname', ['force' => true])->dispatch([
				(new SimpleCommandJob($context, function () use ($context) {
					$afi = \apnscpFunctionInterceptor::factory($context);
					$serviceClass = static::class === self::class ? 'ipinfo' : 'ipinfo6';
					$sites = $afi->admin_collect(["$serviceClass.namebased" => 1]);
					foreach (array_keys($sites) as $site) {
						$afi->admin_edit_site($site, ["$serviceClass.nbaddrs" => "[]", "$serviceClass.namebased" => 1]);
					}
				}))]
			);

			return true;
		}

		public function getHelp(): string
		{
			$type = $this->getPoolType();
			return "Set ${type} namebased pool";
		}

		public function getValues()
		{
			return 'array';
		}

		public function getDefault()
		{
			$pool = '\\Opcenter\\Net\\' . $this->getPoolType();
			return (array)$pool::my_ip();
		}

		/**
		 * Get pool type
		 *
		 * @return string
		 */
		protected function getPoolType(): string {
			return static::class === self::class ? 'Ip4' : 'Ip6';
		}
	}
