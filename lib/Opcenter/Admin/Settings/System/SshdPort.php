<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2019
	 */

	namespace Opcenter\Admin\Settings\System;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class SshdPort implements SettingsInterface
	{
		const SYSPORT = 22;

		public function set($val): bool
		{
			if ($val === $this->get()) {
				return true;
			}

			foreach ((array)$val as $v) {
				if (!\is_int($v)) {
					return error('Port must be integer, %s given', $v);
				}
				if ($v < 1 || $v > 65535) {
					return error('SSH port must be within [1,65535]');
				}
				if ($v !== self::SYSPORT && !\Opcenter\Net\Port::free($v)) {
					return error("Port `%d' is already in use", $v);
				}
				if ($v >= 40000 && $v <= 49999) {
					warn('TCP port range [40000,499999] is reserved for accounts - conflicts may arise');
				}
			}

			$cfg = new Bootstrapper\Config();
			$cfg['sshd_port'] = $val;
			$cfg->sync();
			Bootstrapper::run('system/sshd');

			return true;
		}

		public function get()
		{
			$config = new Bootstrapper\Config();
			return $config['sshd_port'] ?? 22;
		}

		public function getHelp(): string
		{
			return 'Set SSH ports';
		}

		public function getValues()
		{
			return 'int|array';
		}

		public function getDefault()
		{
			return 22;
		}
	}