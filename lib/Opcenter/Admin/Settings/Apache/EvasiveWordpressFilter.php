<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

	namespace Opcenter\Admin\Settings\Apache;

	class EvasiveWordpressFilter extends EvasiveStaticBypass
	{
		protected const MARKER = 'WP BRUTE-FORCE BLOCK';
		protected const TEMPLATE = 'rampart.evasive.wordpress-filter';

		public function getHelp(): string
		{
			return 'Apply strict mod_evasive counters on protected WordPress resources';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}
	}