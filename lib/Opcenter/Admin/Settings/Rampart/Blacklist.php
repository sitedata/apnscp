<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	namespace Opcenter\Admin\Settings\Rampart;

	use Opcenter\Admin\Settings\SettingsInterface;

	class Blacklist implements SettingsInterface
	{
		use \apnscpFunctionInterceptorTrait;

		public function __construct()
		{
			$this->setApnscpFunctionInterceptor(\apnscpFunctionInterceptor::factory(\Auth::context(\Auth::get_admin_login())));
		}

		public function set($val = ''): bool
		{
			if (!$val) {
				return error('Missing IP address');
			}
			warn('Scope is deprecated. Use API command cpcmd rampart:ban %s recidive to temporarily reject an IP address ' .
				'or cpcmd rampart:blacklist %s to permanently drop an IP address. Applying command as ' .
				'cpcmd rampart:ban %s recidive', $val, $val, $val);
			return $this->getApnscpFunctionInterceptor()->call('rampart_ban', [$val, 'recidive']);
		}

		public function get()
		{
			warn('Scope is deprecated. Use API command cpcmd rampart:get_jail_entries recidive to get temporary rejections ' .
				'or cpcmd rampart:get_plist blacklist to get permanent drops. Applying command as ' .
				'cpcmd rampart:get_jail_entries recidive');
			return $this->getApnscpFunctionInterceptor()->call('rampart_get_jail_entries', ['recidive']);
		}

		public function getHelp(): string
		{
			return 'Temporarily blacklist an IP address in Rampart';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '';
		}
	}