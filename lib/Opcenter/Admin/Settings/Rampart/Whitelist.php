<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter\Admin\Settings\Rampart;

	class Whitelist extends Fail2banWhitelist
	{
		public const FORWARDED = true;

		public function set($val = '', string $mode = 'append'): bool
		{
			warn('Scope is deprecated. Use config:set rampart.fail2ban-whitelist Scope ' .
				'or cpcmd rampart_whitelist API command to manage Fail2ban and global whitelist');
			return parent::set($val, $mode);
		}

		public function get()
		{
			warn('Scope is deprecated. Use config:set rampart.fail2ban-whitelist Scope or ' .
				'cpcmd rampart_whitelist API command to manage Fail2ban and global whitelist');
			return parent::get();
		}

		public function getHelp(): string
		{
			return 'Whitelist an IP address from Rampart brute-force detection';
		}

		public function getValues()
		{
			return 'string';
		}

		public function getDefault()
		{
			return '127.0.0.1/8';
		}
	}