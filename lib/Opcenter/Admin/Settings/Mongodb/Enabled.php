<?php declare(strict_types=1);

	namespace Opcenter\Admin\Settings\Mongodb;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Enabled implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				return true;
			}

			$cfg = new Bootstrapper\Config();
			$cfg['mongodb_enabled'] = (bool)$val;
			$cfg->sync();
			Bootstrapper::run('packages/install', 'apnscp/initialize-filesystem-template');

			return true;
		}

		public function get()
		{
			$cfg = new Bootstrapper\Config();
			return $cfg['mongodb_enabled'];
		}

		public function getHelp(): string
		{
			return 'Enable MongoDB support';
		}

		public function getValues()
		{
			return 'bool';
		}

		public function getDefault()
		{
			return false;
		}

	}
