<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, June 2023
 */

namespace Opcenter\Admin\Settings\Kernel;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;
	use Opcenter\System\Sysctl;

	class PanicAsr implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!is_int($val) || $val < 0) {
				return error("Invalid panic asr setting");
			}

			$cfg = new Config();
			$cfg['kernel_panic_asr'] = (int)$val;

			unset($cfg);
			Bootstrapper::run('system/sysctl');

			return true;
		}

		public function get()
		{
			return (int)data_get(new Config, 'kernel_panic_asr', Sysctl::read('kernel.panic'));
		}

		public function getHelp(): string
		{
			return 'Kernel panic automated system recovery timeout';
		}

		public function getValues()
		{
			return 'int';
		}

		public function getDefault()
		{
			return 0;
		}

	}
