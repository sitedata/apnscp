<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, June 2023
 */

namespace Opcenter\Admin\Settings\Kernel;

	use Opcenter\Admin\Bootstrapper;
	use Opcenter\Admin\Bootstrapper\Config;
	use Opcenter\Admin\Settings\SettingsInterface;

	class Variant implements SettingsInterface
	{
		public function set($val): bool
		{
			if ($val == $this->get()) {
				// no need to set the value again
				return true;
			}

			if (!\in_array($val, $this->getValues(), true)) {
				return error('Unknown kernel setting');
			}

			$cfg = new Config();
			$cfg['prefer_experimental_kernel'] = $val === 'system' ? false : true;
			if ($val !== 'system') {
				$cfg['custom_kernel_rpm'] = $val === 'stable' ? 'kernel-lt' : 'kernel-ml';
			}
			unset($cfg);
			Bootstrapper::run('system/kernel');

			return true;
		}

		public function get()
		{
			$cfg = new Config();
			if (!$cfg['prefer_experimental_kernel']) {
				return 'system';
			}

			return array_get($cfg, 'custom_kernel_rpm', 'kernel-lt') === 'kernel-lt' ? 'stable' : 'experimental';
		}

		public function getHelp(): string
		{
			return 'Change default kernel';
		}

		public function getValues()
		{
			return ['system', 'experimental', 'stable'];
		}

		public function getDefault()
		{
			return 'system';
		}

	}