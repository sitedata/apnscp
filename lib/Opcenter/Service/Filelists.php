<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2018
	 */

	namespace Opcenter\Service;

	class Filelists
	{

		/**
		 * Install a filelist collection for a site
		 *
		 * @param string $root site root
		 * @param string $svc  service name
		 * @return bool
		 */
		public static function install(string $root, string $svc): bool
		{
			$path = self::getListPath($svc);
			if (!is_dir($path)) {
				return error("Filelist service for `%s' is not installed", $path);
			}
			$chown = null;
			$cmd = ['rsync', '-lrx', '--ignore-existing', '--relative', '%s', "${path}/./", $root];
			$ret = \Util_Process::exec($cmd, $chown);

			return $ret['success'];
		}

		/**
		 * Get filelisting
		 *
		 * @param string $svc optional service name
		 * @return string
		 */
		private static function getListPath($svc = ''): string
		{
			return storage_path("opcenter/filelists/${svc}");
		}

		/**
		 * Service filelists is installed'
		 *
		 * @param string $svc
		 * @return bool
		 */
		public static function exists(string $svc): bool
		{
			return is_dir(self::getListPath($svc));
		}

		/**
		 * Remove installed filelist from account
		 *
		 * @param string $root site root
		 * @param string $svc  service name
		 * @return bool
		 */
		public static function remove(string $root, string $svc): bool
		{
			return warn('Removing filelists not supported');
		}
	}
