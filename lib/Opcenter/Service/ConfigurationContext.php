<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	namespace Opcenter\Service;

	use Event\Cardinal;
	use Event\Events;
	use Illuminate\Contracts\Support\Arrayable;
	use Opcenter\CliParser;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceExplicitReconfiguration;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\SquashValue;
	use Opcenter\Service\Contracts\Validator;
	use Opcenter\SiteConfiguration;

	/**
	 * Class Service
	 *
	 * Service composition and validation
	 *
	 * $config is merged configuration, $new => $cur => $old => $default
	 *
	 * @package Opcenter
	 */
	class ConfigurationContext implements \ArrayAccess, Arrayable, \Iterator
	{
		use \NamespaceUtilitiesTrait;
		/**
		 * Merged (cur) config
		 *
		 * @var array
		 */
		protected $config;

		/**
		 * Old configuration values
		 *
		 * @var array|null
		 */
		protected $old;

		/**
		 * New configuration values
		 *
		 * @var array
		 */
		protected $new;

		/**
		 * Base service values
		 *
		 * @var array
		 */
		protected $default;

		/**
		 * @var string module name
		 */
		protected $module;

		/**
		 * @var SiteConfiguration container
		 */
		protected $container;

		/**
		 * @var bool force revalidation of all subsequent
		 */
		private $forceRevalidation = false;

		/**
		 * @var array service callback registration index
		 */
		protected $callbackRegistration = [];

		/**
		 * ConfigurationContext constructor.
		 *
		 * @param string            $svc current service name
		 * @param SiteConfiguration $container
		 */
		public function __construct(string $svc, SiteConfiguration $container)
		{
			$this->module = $svc;
			$this->container = $container;
			// allow in place updating
			$this->new = $container->getNewConfiguration($svc);
			$this->old = $container->getOldConfiguration($svc);
			// union of new -> old -> default
			$this->default = $container->getDefaultConfiguration($svc);
			$this->config = array_replace($this->default, $this->old, $this->new);
			$this->callbackRegistration = array_fill_keys(array_keys($this->config), false);
		}

		public function __debugInfo()
		{
			return [
				'config' => $this->config,
				'svc'    => $this->module
			];
		}

		/**
		 * Set configuration parameter in active service class
		 *
		 * @param string     $name
		 * @param mixed|null $value
		 * @return void
		 */
		public function set(string $name, $value): void
		{
			$this->new[$name] = $this->config[$name] = $value;
		}

		/**
		 * Get default service value
		 *
		 * Reads from templates/plans/default/
		 *
		 * @param null|string $svc service name, null for current service
		 * @param null|string $name
		 * @return mixed
		 */
		public function getDefaultServiceValue(string $svc = null, string $name = null)
		{
			return $this->getServiceClassWrapper('default', $svc, $name);
		}

		/**
		 * Perform basic validations in _verify_conf
		 *
		 * @return bool
		 */
		public function preflight(): bool
		{
			$b = '';
			if (($a = $this->key()) !== 'version' || ($b = $this->key($this->next())) !== 'enabled') {
				return error('configuration failed sanity check, order of configuration variables in ' .
					"template must follow 'version', then 'enabled' followed by other service variables. " .
					"`%s', `%s' detected", $a, $b);
			}
			$this->rewind();
			foreach ($this as $k => $v) {
				// --reconfig passed to EditDomain
				if ($this->container->hasValidatorOption('reconfig')) {
					$this->registerCallback($k);
				}

				if (!$this->needsValidation($k)) {
					continue;
				}
				if (!$this->check($k, $v)) {
					return \Error_Reporter::is_error() ? false :
						error("check on `%s',`%s' failed", $this->getModuleName(), $k);
				}

				if (!$this->callbackRegistered($k)) {
					$this->registerCallback($k);
				}
				// can't use iterator by reference, so update post-edit
				$this[$k] = $this->new[$k] = $v;
			}

			return true;
		}

		#[\ReturnTypeWillChange]
		public function key()
		{
			return key($this->config);
		}

		public function next(): void
		{
			next($this->config);
		}

		public function rewind(): void
		{
			reset($this->config);
		}

		/**
		 * Register a callback if supported
		 *
		 * enabled (ServiceLayer) and all other config vars are mutually exclusive
		 *
		 * @param string $cfg
		 */
		private function registerCallback(string $cfg): void
		{
			if ($this->callbackRegistered($cfg)) {
				return;
			}

			$this->callbackRegistration[$cfg] = true;
			if (null === ($validator = $this->getValidator($cfg))) {
				return;
			}

			// @see revalidateService()
			if ($this->requiresReconfiguration($validator, $cfg)) {
				// enabled=[0,1] must flow through to populate/depopulate
				$cb = new ReconfigurationCallback($validator, $this, $cfg);
				Cardinal::register(
					[SiteConfiguration::HOOK_ID, Events::SUCCESS],
					$cb,
					Cardinal::OPT_ATOMIC
				);
				Cardinal::preempt(
					[SiteConfiguration::HOOK_ID, Events::FAILURE],
					$cb
				);

				return;
			}

			Cardinal::register([SiteConfiguration::HOOK_ID, Events::SUCCESS], [$validator, 'update'],
				Cardinal::OPT_ATOMIC);
			if ($validator instanceof ServiceInstall) {
				// @see \Opcenter\Account\Create::installServices
				// preempt failures to allow events to unwind, e.g.
				// success: siteinfo -> ipinfo -> apache -> weblogs
				// failure: weblogs -> apache -> ipinfo -> siteinfo
				Cardinal::preempt([SiteConfiguration::HOOK_ID, Events::FAILURE], [$validator, 'update']);
			}
		}

		/**
		 * Service parameter requires reconfiguration
		 *
		 * @param Validator $validator
		 * @param string    $parameter
		 * @return bool
		 *
		 * Enabled can implement AlwaysValidate, which is passed on by preflight(). Call reconfigure() if enabled=1
		 * and no toggle exists between states (i.e. enabled=0 -> 1) or if --reconfig is passed and no change in state
		 *
		 * SI falls through on Add/Delete unless service value is 0 or 1 (enabled change)
		 *
		 * +------+---+---+---+---+
		 * | EDIT | O | O | O | O |
		 * | SR   | O | O | O | O |
		 * | SI   | X | O | O | X |
		 * | CHG  | O | X | X | - |
		 * | AV   | - | O | - | - |
		 * | ALL  | - | - | O | O |
		 * +------+---+---+---+---+
		 */
		private function requiresReconfiguration(Validator $validator, string $parameter): bool
		{
			if (!$validator instanceof ServiceReconfiguration || !$this->isEdit()) {
				return false;
			}

			$changed = false;
			if (!$validator instanceof ServiceInstall) {
				$changed = $this->serviceValueChanged(null, $parameter) || $this->container->hasValidatorOption('reconfig');
			} else {
				$changed = !\in_array($this->getServiceValue(null, $parameter, 1), [0, 1], true) ||
					( !$this->serviceValueChanged(null, 'enabled') && ($validator instanceof AlwaysValidate || $this->container->hasValidatorOption('reconfig')) );
			}
			return $changed;
		}

		/**
		 * Named service already has callback registered
		 *
		 * @param string $service
		 * @return bool
		 */
		private function callbackRegistered(string $service): bool
		{
			return $this->callbackRegistration[$service] ?? false;
		}

		protected function getValidator(string $cfgvar): ?Validator
		{
			if (!isset($this->checkers[$cfgvar])) {
				if (null === ($checker = $this->getValidatorClass($cfgvar))) {
					return null;
				}
				$c = new $checker($this, $this->container->getSite());

				$this->checkers[$cfgvar] = $c;
			}

			return $this->checkers[$cfgvar];
		}

		/**
		 * Get checker for configuration variable
		 *
		 * @param string configuration variable
		 * @return string
		 */
		public function getValidatorClass($var): ?string
		{
			$class = studly_case($var);
			$checkers = [
				static::appendNamespace('Validators\\' . ucwords($this->getModuleName()) . '\\' . $class),
				static::appendNamespace('Validators\\Common\\' . $class)
			];
			foreach ($checkers as $check) {
				if (class_exists($check)) {
					return $check;
				}
			}

			return null;
		}

		/**
		 * Service configuration is edit state
		 *
		 * @return bool
		 */
		public function isEdit(): bool
		{
			return $this->hasNew() && $this->hasOld();
		}

		/**
		 * Service value has changed
		 *
		 * @param string|null           $service
		 * @param string|null           $type
		 * @return bool
		 */
		public function serviceValueChanged(?string $service, ?string $type): bool
		{
			if (!$this->hasOld() && !$this->hasNew()) {
				return false;
			}

			$new = $this->getNewServiceValue($service, $type);

			return (null !== $new) && $new !== $this->getOldServiceValue($service, $type);
		}

		/**
		 * Get pending service changes
		 *
		 * @param string      $svc
		 * @param null|string $name
		 * @return mixed service or null if deleting
		 */
		public function getNewServiceValue(string $svc = null, string $name = null)
		{
			return array_get($this->new, $name, $this->getServiceClassWrapper('new', $svc, $name));
		}

		/**
		 * Service value wrapper
		 *
		 * @param string      $ctype configuration type [old, new, default]
		 * @param null|string $svc   configuration service
		 * @param null|string $name  configuration name
		 * @return array|mixed
		 */
		private function getServiceClassWrapper(string $ctype, ?string $svc, ?string $name)
		{
			if ($ctype === 'old' && !$this->hasOld()) {
				return null;
			}
			if ($ctype === 'new' && !$this->hasNew()) {
				return null;
			}
			if (null === $svc) {
				$svc = $this->getModuleName();
			}
			$config = $this->container->{'get' . ucwords($ctype) . 'Configuration'}($svc);
			if (null === $name) {
				return $config;
			}

			return array_get($config, $name);
		}

		/**
		 * Service configuration has old values
		 *
		 * Present during a delete or edit, but not add
		 *
		 * @return bool
		 */
		public function hasOld(): bool
		{
			return $this->container->getOldConfiguration() !== [];
		}

		/**
		 * Service configuration has new values
		 *
		 * Present during an add or edit, but not delete
		 *
		 * @return bool
		 */
		public function hasNew(): bool
		{
			return $this->container->getNewConfiguration() !== [];
		}

		/**
		 * Get original service changes
		 *
		 * @param null|string $svc
		 * @param null|string $name
		 * @return mixed service value or null if creating
		 */
		public function getOldServiceValue(string $svc = null, string $name = null)
		{
			return $this->getServiceClassWrapper('old', $svc, $name);
		}

		/**
		 * Get composite service value
		 *
		 * new => old => default => $default parameter
		 *
		 * @param string|null $svc     service class
		 * @param string      $name    service name
		 * @param mixed       $default default value
		 * @return mixed
		 */
		public function getServiceValue(string $svc = null, string $name = null, $default = null)
		{
			if (null === $svc) {
				$svc = $this->getModuleName();
			}

			return $this->container->getServiceValue($svc, $name, $default);
		}

		/**
		 * Get module name of current service
		 *
		 * @return string
		 */
		public function getModuleName(): string
		{
			return $this->module;
		}

		/**
		 * Get SiteConfiguration container
		 *
		 * @return SiteConfiguration
		 */
		public function getConfigurationContainer(): SiteConfiguration
		{
			return $this->container;
		}

		/**
		 * Force revalidation of all variables within service
		 */
		public function revalidateService() {
			// @XXX calling revalidateService without guarding via
			//      if (!$this->isRevalidation()) { ... }
			//      will cause an infinite loop
			debug("Revalidation forced on `%s'", $this->getModuleName());
			$this->forceRevalidation = true;
			// ensure global uniqueness of event order to prevent duplicates
			 $this->rewind();
		}

		/**
		 * Fill omitted configuration with defaults
		 */
		public function applyDefaults(): void {
			debug('Defaults applied to %s', $this->getModuleName());
			foreach ($this->getConfigurationContainer()->getDefaultConfiguration($this->getModuleName()) as $k => $v) {
				if ($this->config[$k] === null) {
					info("Applied default value %s to `%s,%s'", $v, $this->getModuleName(), $k);
					$this->config[$k] = $v;
				}
			}
		}

		/**
		 * Service class is being revalidated
		 *
		 * @return bool
		 */
		public function isRevalidation(): bool {
			return $this->forceRevalidation;
		}

		/**
		 * Configuration parameter requires validation
		 *
		 * @param string $key
		 * @return bool
		 */
		protected function needsValidation(string $key): bool
		{
			/*
			 * @XXX old/default values should never cause conflict
			 * if old is equal to new, bypass validation but register ServiceInstall::update()
			 * by instantiating validator
			 */
			if (null === ($checker = $this->getValidator($key))) {
				warn("service `%s' value `%s' lacks checker", $this->getModuleName(), $key);

				return false;
			}
			if ($this->isCreate()) {
				return true;
			}

			if ($this->forceRevalidation || $checker instanceof AlwaysValidate) {
				return true;
			}

			/**
			 * @TODO
			 * Allow ServiceExplicitReconfiguration to flag reconfigure/rollback tasks when
			 * service value goes from non-null to null
			 */
			return $checker instanceof ServiceExplicitReconfiguration ?
				$this->getOldServiceValue(null, $key) !== $this->getNewServiceValue(null, $key) :
				$this->serviceValueChanged(null, $key);
		}

		/**
		 * Configuration in creation stage
		 *
		 * @return bool
		 */
		public function isCreate(): bool
		{
			return $this->hasNew() && !$this->hasOld();
		}

		/**
		 * Check proposed value
		 *
		 * @param string $key
		 * @param mixed  $value
		 * @return bool
		 */
		protected function check(string $key, &$value): bool
		{
			$validator = $this->getValidator($key);
			if (\is_array($value) && is_subclass_of($validator, SquashValue::class)) {
				$value = CliParser::collapse($value);
			}
			return $validator->valid($value);
		}

		/**
		 * Get module service name
		 *
		 * @return string
		 */
		public function getServiceName(): ?string
		{
			return key($this->config);
		}

		/**
		 * Configuration in deletion stage
		 *
		 * @return bool
		 */
		public function isDelete(): bool
		{
			return !$this->hasNew() && $this->hasOld();
		}

		public function commitService(): bool
		{
			$myconfig = $this->config;
			foreach ($myconfig as $k => $v) {
				if (null === ($checker = $this->getValidator($k))) {
					continue;
				}
				if (!method_exists($checker, 'write')) {
					continue;
				}
				$myconfig[$k] = $checker->write();
			}

			if (!$this->container->write('current', [$this->getModuleName() => $myconfig])) {
				return error("failed to commit service configuration for `%s'", $this->getModuleName());
			}

			/**
			 * cleaning cannot fail, but it's necessary to delay commit journaling so that
			 * edit hooks fire and pick up new/cur/old config differences otherwise if siteinfo
			 * fires first, ssl can't pickup new/siteinfo
			 */
			Cardinal::register([SiteConfiguration::HOOK_ID, Events::END], function() {
				return $this->commit();
			});

			return true;
		}

		private function commit(): bool
		{
			debug("Commiting service `%s'", $this->getModuleName());

			return $this->container->cleanService($this->getModuleName());
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return isset($this->config[$offset]);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return $this->config[$offset];
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			$this->config[$offset] = $value;
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			unset($this->config[$offset]);
		}

		public function valid(): bool
		{
			return $this->current() !== false;
		}

		#[\ReturnTypeWillChange]
		public function current()
		{
			return current($this->config);
		}

		public function toArray()
		{
			return $this->config;
		}

		/**
		 * Get recomputed values after preflight()
		 *
		 * @return array
		 */
		public function getRecomputedValues()
		{
			return $this->new;
		}
	}