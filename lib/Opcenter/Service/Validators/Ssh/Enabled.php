<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ssh;

	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Provisioning\Pam;
	use Opcenter\Provisioning\Ssh;
	use Opcenter\Service\Contracts\MountableLayer;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceLayer;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements MountableLayer, ServiceInstall, ServiceReconfiguration
	{
		use \FilesystemPathTrait;

		public function valid(&$value): bool
		{
			if (!parent::valid($value)) {
				return false;
			}

			if (!$value) {
				if (!$this->ctx->isRevalidation() && $this->ctx->isEdit()) {
					$this->ctx['port_index'] = null;
					$this->ctx->revalidateService();
				}
			}

			return true;
		}


		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			Ssh::createGroups($svc->getAccountRoot());
			Ssh::populateFilesystem($svc, 'ssh');
			(new Pam($svc))->enable('ssh');
			Cardinal::register([ServiceLayer::HOOK_ID, Events::END],
				static function () use ($svc) {
					Ssh::unblockSsh($svc);
				});

			return (new PortIndex($this->ctx, $svc->getSite()))->populate($svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			Ssh::removeGroups($svc->getAccountRoot(), false);
			(new Pam($svc))->disable('ssh')->terminate('ssh');

			if ($this->ctx->getServiceValue('crontab', 'permit') && !$this->ctx->isDelete()) {
				Cardinal::register([ServiceLayer::HOOK_ID, Events::END],
					static function () use ($svc) {
						Ssh::blockSsh($svc);
					});
			}

			return (new PortIndex($this->ctx, $svc->getSite()))->depopulate($svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function getLayerName(): string
		{
			return 'ssh';
		}

		protected function manageLayer(int $value, string $layer): void
		{
			if (!$value && $this->ctx->getServiceValue('crontab', 'enabled')) {
				// avoid unmounting ssh layer when crontab is enabled
				return;
			}
			parent::manageLayer($value, $layer);
		}


	}

