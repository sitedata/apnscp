<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Ssh;

	use Opcenter\Account\Create;
	use Opcenter\Database\PostgreSQL;
	use Opcenter\Map;
	use Opcenter\Provisioning\Apache;
	use Opcenter\Service\Contracts\AlwaysValidate;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\GenericMap;
	use Opcenter\SiteConfiguration;

	class PortIndex extends GenericMap implements ServiceReconfiguration, ServiceInstall
	{
		const MAP_FILE = 'ssh.portmap';
		const DESCRIPTION = 'TCP daemon port index';
		const VALUE_RANGE = '[1,999]';

		public function valid(&$value): bool
		{
			if (!$this->ctx->getServiceValue(null, 'enabled') || !SSH_USER_DAEMONS) {
				$value = null;

				return true;
			}
			if (null === $value) {
				$value = $this->findPort();
				if ($value === null) {
					return error('unable to allocate SSH port for account');
				}
				info('allocated port range [4%03u0,4%03u9] for account', $value, $value);
			}
			if (!\is_array($value)) {
				$value = [$value];
			}
			$bad = array_filter($value, function ($val) {
				return !\is_int($val) || $val < 1 || $val > 999 || !parent::valid($val);
			});
			if ($bad) {
				return error('invalid or non-numeric ports found: %s', implode(',', $bad));
			}

			return true;
		}

		/**
		 * Find available port in port range index
		 *
		 * @return int|null
		 */
		private function findPort(): ?int
		{
			$map = Map::load(self::MAP_FILE, 'cd');
			$n = min(Create::MAX_SITES, 999);
			for ($i = 1; $i < $n; $i++) {
				$map->next();
				if (!$map->exists((string)$i)) {
					return $i;
				}
			}

			return null;
		}

		public function populate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure(null, $this->ctx->getNewServiceValue(null, 'port_index'), $svc);
		}

		public function reconfigure($oldvalue, $newvalue, SiteConfiguration $svc): bool
		{
			if ($oldvalue) {
				$query = PostgreSQL::vendor('ssh')->deletePortIndex($svc->getSiteId(), $oldvalue);
				foreach ((array)$oldvalue as $value) {
					parent::removeMap((string)$value);
					\PostgreSQL::pdo()->exec($query);
				}
			}

			if (!$newvalue) {
				return true;
			}

			$query = PostgreSQL::vendor('ssh')->assignPortIndex($svc->getSiteId(),
				$newvalue);
			$ret = \PostgreSQL::pdo()->exec($query);
			foreach ((array)$newvalue as $value) {
				if (!parent::addMap((string)$value, $svc->getSite())) {
					$ret = false;
					error("failed to assign port `%d' to `%s'", $value, $svc->getSite());
				}
			}

			return (bool)$ret;
		}

		public function rollback($oldvalue, $newvalue, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($newvalue, $oldvalue, $svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			return $this->reconfigure($this->ctx->getOldServiceValue(null, 'port_index'), null, $svc);
		}




	}
