<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\SiteConfiguration;

	class Enabled extends \Opcenter\Service\Validators\Common\Enabled implements ServiceInstall
	{
		use \FilesystemPathTrait;

		public function valid(&$value): bool
		{
			if (!parent::valid($value)) {
				return false;
			}

			if ($this->ctx->isRevalidation()) {
				// provider changed
				// @TODO ambiguous properties, conflicts with forceRevalidation
				$this->force = true;

			}

			return true;
		}

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{
			$ctx = $svc->getAuthContext();
			$class = \apnscpFunctionInterceptor::get_autoload_class_from_module('dns');
			$module = $class::instantiateContexted($ctx)->_proxy();
			if (!$module->configured()) {
				return info("DNS not configured for `%s', bypassing DNS hooks", $ctx->domain);
			}

			$ip = (array)$module->get_public_ip();
			foreach ($svc->getSiteFunctionInterceptor()->dns_zones() as $domain) {
				if (!$module->add_zone($domain, $ip[0])) {
					return false;
				}

				if (!$module->domain_uses_nameservers($domain)) {
					warn("Domain `%s' doesn't use assigned nameservers. Change nameservers to %s",
						$domain, implode(',', $module->get_hosting_nameservers($domain))
					);
				}
			}


			return true;
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			// @var Dns_Module $dns
			if ($this->ctx->isEdit() && $this->ctx['provider'] !== 'null')
			{
				return warn("DNS is preserved when setting dns,enabled=0 on edit. " .
					"To remove DNS for zone set both dns,enabled=0 and dns,provider=null.");
			}
			$class = \apnscpFunctionInterceptor::get_autoload_class_from_module('dns');
			$module = $class::instantiateContexted($svc->getAuthContext())->_proxy();
			$rfxn = new \ReflectionProperty($module, 'permission_level');
			$rfxn->setAccessible(true);
			$rfxn->setValue($module, $rfxn->getValue($module) | PRIVILEGE_SERVER_EXEC);
			foreach ($svc->getSiteFunctionInterceptor()->dns_zones() as $domain) {
				debug("Try-remove zone `%s'", $domain);
				try {
					if (!$module->remove_zone($domain)) {
						dlog("Could not remove zone `%s'", $domain);
					}
				} catch (\Throwable $e) {
					if (!$svc->hasValidatorOption('force')) {
						throw $e;
					}
					warn("Failed to remove zone, force applied. Ignoring: %s", $e->getMessage());
				}
			}

			return true;
		}
	}

