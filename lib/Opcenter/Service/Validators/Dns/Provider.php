<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */


	namespace Opcenter\Service\Validators\Dns;

	use Opcenter\Dns;
	use Opcenter\Service\Contracts\DefaultNullable;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\Service\Validators\Common\GenericProvider;
	use Opcenter\SiteConfiguration;

	class Provider extends GenericProvider implements ServiceReconfiguration, DefaultNullable
	{

		public function valid(&$value): bool
		{
			if ($value === DefaultNullable::NULLABLE_MARKER) {
				$value = $this->getDefault();
			}

			if ($value === null) {
				// -c dns,provider=null gets converted to literal type
				$value = 'null';
			}

			if (!$value || !\Opcenter\Dns::providerValid($value)) {
				return error("Unknown dns provider `%s'", $value);
			}

			if (!$this->ctx->isRevalidation() && $this->hasChange()) {
				$this->ctx->revalidateService();
			}

			return true;
		}

		public function getDescription(): ?string
		{
			return 'Assign DNS handler for account';
		}

		public function getValidatorRange()
		{
			return \Opcenter\Dns::providers();
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if ($old === $new) {
				return true;
			}
			$this->freshenSite($svc);

			if (!DNS_MIGRATE) {
				return debug("[dns] => migrate disabled. Skipping migration.");
			}

			debug("Provider change. Migrating zones");

			$afi = $svc->getSiteFunctionInterceptor();
			$oldCtx = $svc->getAuthContext()->mock($svc->getAuthContext()->getAccount()->old);
			/** @var \Dns_Module $swapped */
			$swapped = \Module\Provider::get('dns', $old, $oldCtx);
			$oldAfi = \apnscpFunctionInterceptor::factory($oldCtx);
			$oldAfi->swap('dns', $swapped);

			foreach ($afi->dns_zones() as $domain) {
				debug("Migrating %(domain)s", ['domain' => $domain]);
				if (null === ($data = $oldAfi->dns_export($domain)) || !$afi->dns_zone_exists($domain)) {
					continue;
				}

				if ($afi->dns_import($domain, $data)) {
					info("Migrated %(domain)s to %(provider)s", ['domain' => $domain, 'provider' => $new]);
					if ($swapped->remove_zone($domain)) {
						info("Removed zone %(domain)s from %(provider)s", ['domain' => $domain, 'provider' => $old]);
					}
				}
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return true;
		}

		public function getDefault()
		{
			return Dns::default();
		}


	}
