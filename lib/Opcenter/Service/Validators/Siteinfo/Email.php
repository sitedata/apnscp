<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Siteinfo;

	use Opcenter\Database\PostgreSQL\Opcenter;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Email extends ServiceValidator implements ServiceReconfiguration
	{
		const DESCRIPTION = 'Contact address on account';
		const VALUE_RANGE = '[email,[email1,email2...]]';

		public function valid(&$value): bool
		{
			if (empty($value)) {
				$afi = \apnscpFunctionInterceptor::factory(\Auth::profile());
				if (! ($value = $afi->common_get_email())) {
					return error('Missing contact address');
				}
				warn("No email set for account, defaulting to admin default `%s'", $value);
			}
			if (!\is_array($value)) {
				$value = preg_split('/\s*,\s*/', $value);
			}

			$bad = array_filter($value, static function ($email) {
				if (!preg_match(\Regex::EMAIL, $email)) {
					error("invalid email address `%s'", $email);

					return true;
				}

				return false;
			});
			$value = implode(',', $value);

			return !$bad;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			return (new Opcenter(\PostgreSQL::pdo()))->changeEmail($svc->getSiteId(), $new);
		}


	}

