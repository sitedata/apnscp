<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
	 */

	namespace Opcenter\Service\Validators\Diskquota;

	use Opcenter\Service\Validators\Common\Units as UnitPure;

	class Units extends UnitPure
	{
		public function valid(&$value): bool
		{
			if ($this->ctx->isEdit() && !$this->ctx->isRevalidation()) {
				$this->ctx->revalidateService();
				return true;
			}

			return parent::valid($value);
		}
	}