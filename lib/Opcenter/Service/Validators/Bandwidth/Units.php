<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
	 */

	namespace Opcenter\Service\Validators\Bandwidth;

	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Validators\Common\Units as UnitPure;
	use Opcenter\SiteConfiguration;

	class Units extends UnitPure implements ServiceReconfiguration
	{
		public function valid(&$value): bool
		{
			if ($this->ctx->isEdit() && !$this->ctx->isRevalidation()) {
				$this->ctx->revalidateService();

				return true;
			}

			return parent::valid($value);
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			// force reconfiguration when unit changes, e.g. B => GB
			return (new Threshold($this->ctx, $svc->getSite()))->reconfigure(
				$svc->getOldConfiguration(null, 'threshold'),
				$svc->getNewConfiguration(null, 'threshold'),
				$svc
			);
		}


	}