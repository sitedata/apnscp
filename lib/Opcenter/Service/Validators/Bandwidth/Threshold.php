<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Bandwidth;

	use Opcenter\Bandwidth\Site;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Threshold extends ServiceValidator implements ServiceReconfiguration
	{
		const DESCRIPTION = 'Maximum bandwidth for a service month';
		const VALUE_RANGE = '[null, 0-∞]';
		/**
		 * Avoid duplicate calculation
		 *
		 * @var float
		 */
		private float $sum = 0;

		public function valid(&$value): bool
		{
			if ($value === null && $this->ctx->getServiceValue('bandwidth', 'enabled')) {
				$this->ctx->set('enabled', false);
				warn('disabling bandwidth service');

				return true;
			}

			if (!is_numeric($value)) {
				if ($this->ctx->getServiceValue('bandwidth', 'enabled')) {
					return error("bandwidth threshold must be numeric, `%s' given", $value);
				}

				$value = 0;
			}

			$units = $this->ctx->getServiceValue('bandwidth', 'units');
			$tmp = \Formatter::changeBytes($value, 'B', $units);

			if ($tmp >= PHP_INT_MAX) {
				warn('bandwidth threshold nonsensible, assuming unlimited bandwidth (setting bandwidth,enabled=0)');
				$this->ctx['enabled'] = 0;
				$value = 0;
				$this->ctx->revalidateService();
				return true;
			}

			if ($tmp < 0) {
				return error("bandwidth must be a non-negative number, `%s' found", $value);
			}

			return $this->checkBandwidth($tmp);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			$pg = \PostgreSQL::pdo();
			$threshold = $this->ctx->getServiceValue(null, 'threshold');
			$byteThreshold = !$threshold ? null : \Formatter::changeBytes($threshold, 'B',
				$this->ctx->getServiceValue(null, 'units', 'B'));
			$stmt = $pg->prepare('INSERT INTO bandwidth (threshold, site_id, rollover) VALUES (:threshold, :site_id, :rollover) 
				ON CONFLICT (site_id) DO UPDATE SET threshold = :threshold, rollover = :rollover');
			if (!$stmt->execute([
				':site_id'   => $svc->getSiteId(),
				':threshold' => (int)$byteThreshold,
				':rollover'  => (int)$this->ctx['rollover']
			])) {
				return error('failed to alter bandwidth metadata: %s', $stmt->errorInfo()[2]);
			}

			$handler = new Site($svc->getSiteId());

			if (!$handler->suspended()) {
				return true;
			}

			if (!$byteThreshold || $this->sum < $byteThreshold) {
				info('Unsuspending site - bandwidth threshold %d exceeds current usage %d for period ending %s',
					$byteThreshold, $this->sum, date('Y-m-d', $handler->getCycleEnd())
				);
				return $handler->unsuspend() && $svc->getAuthContext()->reset();
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			return $this->reconfigure($new, $old, $svc);
		}

		private function checkBandwidth(?float $quota): bool
		{
			if (!$quota || !$this->ctx['enabled'] || !$this->ctx->isEdit()) {
				return true;
			}

			$svc = $this->ctx->getConfigurationContainer();

			// check if overages forced suspension, then unsuspend site accordingly
			$handler = new Site($svc->getSiteId());
			$this->sum = array_sum(array_map(static function ($v) {
				return $v['in'] + $v['out'];
			}, $handler->getByRange($handler->getCycleBegin())));

			if ($quota >= $this->sum || $handler->suspended()) {
				return true;
			}

			if ($svc->hasValidatorOption('force')) {
				return warn(':bandwidth_adj_suspension_forced',
					'Available bandwidth has been reduced for site from %(old).2f GB while consuming %(new).2f GB. This site will be suspended next check.',
					[
						'old' => \Formatter::changeBytes($quota, 'GB'),
						'new' => \Formatter::changeBytes($this->sum, 'GB')
					]);
			}

			return error(':bandwidth_adj_suspension',
				'Available bandwidth has been reduced for site from %(old).2f GB while consuming %(new).2f GB. Suspend site before performing operation.',
				[
					'old' => \Formatter::changeBytes($quota, 'GB'),
					'new' => \Formatter::changeBytes($this->sum, 'GB')
				]);
		}
	}