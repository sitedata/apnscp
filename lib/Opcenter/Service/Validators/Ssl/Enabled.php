<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Ssl;

	use Event\Cardinal;
	use Event\Events;
	use Module\Support\Letsencrypt;
	use Opcenter\Filesystem;
	use Opcenter\Provisioning\Ssl;
	use Opcenter\Service\Contracts\ServiceInstall;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\ServiceToggle;
	use Opcenter\Service\Validators\Common\Enabled as Base;
	use Opcenter\SiteConfiguration;

	class Enabled extends Base implements ServiceInstall, ServiceReconfiguration, ServiceToggle
	{
		use \FilesystemPathTrait;

		public function valid(&$value): bool
		{
			return parent::valid($value);
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				return $this->depopulate($svc);
			}

			return $this->populate($svc);
		}

		public function depopulate(SiteConfiguration $svc): bool
		{
			Cardinal::register([SiteConfiguration::HOOK_ID, Events::END], static function () {
				\Opcenter\Http\Apache::activate();
			});
			$path = \Opcenter\Http\Apache::siteStoragePath($svc->getSite()) . '.ssl';
			if (is_dir($path)) {
				Filesystem::rmdir($path);
			}

			// /etc/httpd/conf/ssl.* renaming handled by ssl_module
			return true;
		}

		/**
		 * Mount filesystem, install users
		 *
		 * @param SiteConfiguration $svc
		 * @return bool
		 */
		public function populate(SiteConfiguration $svc): bool
		{

			Ssl::populateFilesystem($svc);
			$path = \Opcenter\Http\Apache::siteStoragePath($svc->getSite()) . '.ssl';
			if (!is_dir($path)) {
				Filesystem::mkdir($path);
			}

			Cardinal::register([SiteConfiguration::HOOK_ID, Events::END], function () use ($svc) {
				\Opcenter\Http\Apache::activate();

				// attempt SSL bootstrap
				if ($this->ctx->isEdit() && (!LETSENCRYPT_AUTO_BOOTSTRAP || !$this->ctx->serviceValueChanged(null, 'enabled')))
				{
					return;
				}

				if ($this->ctx->isCreate() && !$svc->getValidatorOption('bootstrap', LETSENCRYPT_AUTO_BOOTSTRAP)) {
					return;
				}

				$svc->getSiteFunctionInterceptor()->letsencrypt_bootstrap();
			});



			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			// cannot discard the certificate and recover
			return true;
		}

		public function suspend(SiteConfiguration $svc): bool
		{
			return true;
		}

		public function activate(SiteConfiguration $svc): bool
		{
			$afi = $svc->getSiteFunctionInterceptor();
			if (!$afi->ssl_enabled() || !$afi->letsencrypt_supported() || !$afi->ssl_cert_exists()) {
				return true;
			}

			$cert = $afi->ssl_get_certificate();
			if (!$afi->letsencrypt_is_ca($cert)) {
				return true;
			}

			$x509 = $afi->ssl_parse_certificate($cert);
			$daysUntil = \Opcenter\Crypto\Letsencrypt::daysUntilExpiry($x509);

			if ($daysUntil > \Opcenter\Crypto\Letsencrypt::MAX_EXPIRY_DAYS) {
				return true;
			}

			dlog("Scheduling renewal on suspended site %s", $svc->getSite());
			$afi->pman_schedule_api_cmd('letsencrypt_renew', null, 'now');

			return true;
		}


	}

