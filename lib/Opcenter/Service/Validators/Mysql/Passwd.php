<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, March 2018
	 */

	namespace Opcenter\Service\Validators\Mysql;

	use Opcenter\Auth\Password;
	use Opcenter\Service\Contracts\ServiceReconfiguration;
	use Opcenter\Service\Contracts\SquashValue;
	use Opcenter\Service\ServiceValidator;
	use Opcenter\SiteConfiguration;

	class Passwd extends ServiceValidator implements ServiceReconfiguration, SquashValue
	{
		public function valid(&$value): bool
		{
			if (!$this->ctx['enabled']) {
				$value = null;
				return true;
			}
			if (!$value) {
				$value = Password::generate(16);
			} else if (!Password::strong($value)) {
				return false;
			}

			return true;
		}

		public function write()
		{
			return null;
		}

		public function getDescription(): ?string
		{
			return 'Plain-text password for ' . $this->ctx->getModuleName() . ' user.';
		}

		public function reconfigure($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				return true;
			}

			$new = (string)$new;
			if (!Password::strong($new)) {
				return false;
			}

			$afi = $svc->getSiteFunctionInterceptor();
			$type = $this->ctx->getModuleName();
			$user = $this->ctx->getServiceValue($type, 'dbaseadmin');
			if ($type === 'pgsql') {
				if (!$afi->pgsql_edit_user($user, $new)) {
					return error('failed to update password for user %s@%s', $user, 'localhost');
				}
			} else {
				$grants = $afi->{$type . '_list_users'}()[$user] ?? [];

				foreach ($grants as $host => $data) {
					if (!$afi->{$type . '_edit_user'}($user, $host, ['password' => $new])) {
						return error('failed to update password for user %s@%s', $user, $host);
					}
				}
			}

			return true;
		}

		public function rollback($old, $new, SiteConfiguration $svc): bool
		{
			if (!$new) {
				return true;
			}
			// Sorry charlie, can't rollback password
			return warn('Cannot rollback database password');
		}


	}