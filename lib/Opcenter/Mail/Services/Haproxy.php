<?php declare(strict_types=1);

namespace Opcenter\Mail\Services;

use Opcenter\Mail\Contracts\ServiceDaemon;
use Opcenter\System\GenericSystemdService;

class Haproxy extends GenericSystemdService implements ServiceDaemon
{
	protected const SERVICE = 'haproxy';

	/**
	 * Reload Haproxy service
	 *
	 * @param string|null $timespec
	 * @return bool
	 */
	public static function reload(string $timespec = null): bool
	{
		if (!static::exists()) {
			return true;
		}

		return parent::reload($timespec ?? HTTPD_RELOAD_DELAY);
	}

	/**
	 * Restart Haproxy
	 *
	 * @param string|null $timespec
	 * @return bool
	 */
	public static function restart(string $timespec = null): bool
	{
		if (!static::exists()) {
			return true;
		}

		return parent::restart($timespec ?? HTTPD_RELOAD_DELAY);
	}

	/**
	 * @inheritDoc
	 */
	public static function present(): bool
	{
		return MAIL_PROXY === 'haproxy';
	}


}
