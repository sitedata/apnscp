<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, September 2019
 */

namespace Opcenter\Bandwidth;

use Daphnie\Connector;
use Opcenter\Database\PostgreSQL;

class Bulk {

	/**
	 * Get sites matching rollover date
	 *
	 * @param int|null $day day of month, omit for current day
	 * @return array
	 */
	public static function rollover(int $day = null): array
	{
		if (null === $day) {
			$day = (int)date('d');
		} else if ($day < 0 || $day > 31) {
			fatal('Nonsense day provided: %d', $day);
		}
		$db = \PostgreSQL::initialize();
		$sites = [];
		if (!($res = $db->query('SELECT site_id FROM bandwidth WHERE rollover = ' . $day))) {
			return $sites;
		}

		while (null !== ($row = $res->fetch_object())) {
			$sites[] = (int)$row->site_id;
		}

		return $sites;
	}

	/**
	 * Find sites with eligible rollover
	 *
	 * @return array indexed by site ID, array values begin/end (epoch TS)
	 * @throws \PostgreSQLError
	 */
	public static function findRollovers(): array
	{
		$q = 'SELECT site_id, EXTRACT(epoch FROM begindate::TIMESTAMPTZ) AS begin, ' .
			'EXTRACT(epoch FROM begindate::TIMESTAMPTZ + INTERVAL \'1 month\') AS end FROM bandwidth_spans ' .
			"WHERE enddate IS NULL AND begindate::TIMESTAMPTZ + INTERVAL '1 month' <= CURRENT_TIMESTAMP::DATE";
		$db = \PostgreSQL::initialize();
		$rs = $db->query($q);
		$sites = [];
		while (null !== ($row = $rs->fetch_object())) {
			$sites[(int)$row->site_id] = [
				'begin' => (int)$row->begin,
				'end'   => (int)$row->end
			];
		}

		return $sites;
	}

	public static function getCurrentUsage(): array
	{
		$query = 'SELECT
				bandwidth.site_id,
				EXTRACT(epoch FROM begindate::TIMESTAMPTZ)::INTEGER AS begin_ts,
				threshold::BIGINT,
				SUM(in_bytes)::BIGINT AS in,
				SUM(out_bytes)::BIGINT AS out,
				SUM(total)::BIGINT,
				(SUM(total)-threshold)::BIGINT AS over
			FROM
				site_totals_daily_view
			JOIN bandwidth USING (site_id)
			JOIN bandwidth_spans USING (site_id)
			WHERE
				ts_bucket >= begindate
				AND
				enddate IS NULL
			GROUP BY (bandwidth.site_id, begindate)';
		$db = \PostgreSQL::pdo();
		if (!$rs = $db->query($query)) {
			return [];
		}

		return $rs->fetchAll(\PDO::FETCH_ASSOC);
	}

	public static function findOverages(): array
	{
		$query = 'SELECT 
				EXTRACT(epoch FROM begindate::TIMESTAMPTZ)::INTEGER AS begin_ts,
				bandwidth.site_id, 
				threshold::BIGINT,
				SUM(in_bytes)::BIGINT AS in,
				SUM(out_bytes)::BIGINT AS out,
				SUM(total)::BIGINT, 
				(SUM(total)-threshold)::BIGINT AS over 
			FROM 
				site_totals_daily_view 
			JOIN bandwidth USING (site_id) 
			JOIN bandwidth_spans USING (site_id) 
			WHERE  
				ts_bucket >= begindate 
				AND 
				enddate IS NULL 
			GROUP BY (bandwidth.site_id, begindate)
			HAVING ((SUM(total)-threshold)*' . (int)BANDWIDTH_NOTIFY . ' > 0);';
		$db = \PostgreSQL::pdo();
		if (!$rs = $db->query($query)) {
			return [];
		}
		return $rs->fetchAll(\PDO::FETCH_ASSOC);
	}

	/**
	 * Synchronize journaled data to c.agg
	 *
	 * @return bool
	 * @throws \PostgreSQLError
	 */
	public static function sync(): bool
	{
		$pdo = \PostgreSQL::pdo();
		$vendor = (new Connector($pdo))->vendor();
		if (false === $pdo->exec($vendor->refreshContinuousAggregate("site_totals_hourly_view"))) {
			return error("Sync error: %s", $pdo->errorInfo()[2]);
		}

		$query = PostgreSQL::vendor()->materialViewDefinition('site_totals_daily_view');
		if (!$pdo->query($query)->rowCount()) {
			return true;
		}
		$res = $pdo->exec($vendor->refreshMaterializedView("site_totals_daily_view"));

		return $res !== false ?: error("Sync error: %s", $pdo->errorInfo()[2]);
	}
}