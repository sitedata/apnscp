<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2023
	 */

	namespace Opcenter\Logging\Rotation;

	use Opcenter\Filesystem;

	class ConfigFile
	{
		private const BLOCK_REGEX = '!^(["/][^{]+){?([^}]+)}?!m';
		/**
		 * @var ConfigSection[]
		 */
		private array $collection = [];
		private ConfigSection $head;

		public function __construct(private string $path) {
			// @TODO allow log group + top-level config per-file
			// either/or to preserve comments within markup
			$contents = file_get_contents($path);
			preg_match_all('!^(["/][^{]+){?([^}]+)}?!m', $contents, $groups, PREG_SET_ORDER);
			foreach ($groups as $group) {
				$section = new ConfigSection(trim($group[1]), trim($group[2]));;
				$this->collection[] = $section;
			}

			$regex = self::BLOCK_REGEX;
			// strip comment markup if no blocks detected to avoid setting top-level directives
			$regex = !empty($this->collection) ? '}^\s*#.*$|' . \Regex::recompose($regex, '}', true) . '}m' : $regex;
			$outside = trim(preg_replace($regex, '', $contents));
			if ($outside) {
				$this->collection[] = new ConfigSection(null, $outside);
			}

			$this->head = current($this->collection);
		}

		/**
		 * Set logrotation parameters
		 *
		 * @param array|string|ConfigSection $item
		 * @param mixed|null                 $val
		 * @return self
		 */
		public function set(array|string|ConfigSection $item, mixed $val = null): self
		{
			if (is_string($item)) {
				$item = [$item => $val];
			}

			$block = null;
			if ($item instanceof ConfigSection) {
				if ($item->logName()) {
					$block = $this->match(strtok((string)$item->logName(), ' '));
				}
			}

			$block ??= $this->head;

			foreach ($item as $k => $v) {
				$block->set($k, $v);
			}

			return $this;
		}

		/**
		 * Match a section by log fragment
		 *
		 * @param string $logPattern
		 * @return ConfigSection|null
		 */
		public function match(string $logPattern): ?ConfigSection
		{
			foreach ($this->collection as $c) {
				if ($c->match($logPattern)) {
					return $c;
				}
			}

			return null;
		}

		public function save(): bool
		{
			return Filesystem::atomicWrite($this->path(), (string)$this);
		}

		public function path(): string
		{
			return $this->path;
		}

		public function __toString(): string
		{
			return implode("\n", array_map(strval(...), $this->collection));
		}
	}