<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */


namespace Opcenter\Migration\Import;

use Event\Contracts\Publisher;
use Event\Contracts\Subscriber;

class LateContextPublisher implements Publisher, Subscriber {
	protected $bill;

	public function getEventArgs()
	{
		return ['bill' => $this->bill];
	}

	public function update($event, Publisher $caller)
	{
		$this->bill = $caller->getBill();
	}

}