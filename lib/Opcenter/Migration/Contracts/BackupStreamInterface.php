<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

namespace Opcenter\Migration\Contracts;

use Opcenter\Migration\Locker;

interface BackupStreamInterface extends \RecursiveIterator, \SeekableIterator, \Countable, \ArrayAccess {
	public function getPrefix(): string;
	public function bind(Locker $lock): void;
	public function archivePath(string $file): \DirectoryIterator;
	public function externalExtract(string $subdir, string $dest, int $uid = 0, int $gid = 0): bool;
}