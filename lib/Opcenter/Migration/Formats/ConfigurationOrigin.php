<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Opcenter\Migration\Formats;

use Opcenter\Filesystem;
use Opcenter\Map;
use Opcenter\Migration\Bill;
use Opcenter\Migration\Contracts\BackupStreamInterface;
use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
use Opcenter\Migration\Helpers\PathTranslate;
use Opcenter\Migration\Import;
use Opcenter\SiteConfiguration;

abstract class ConfigurationOrigin {
	// @var string override to disable guessing
	const PATHMAP_PATH = '';


	protected $task;
	/**
	 * @var BackupStreamInterface
	 */
	protected $stream;

	/**
	 * @var Bill migration bill
	 */
	protected $bill;

	public function __construct(Import $task, BackupStreamInterface $archiveStream)
	{
		$this->task = $task;
		$this->stream = $archiveStream;
	}

	/**
	 * Set migration bill
	 *
	 * @param Bill $bill
	 * @return ConfigurationBuilder
	 */
	public function setBill(Bill $bill): self
	{
		$this->bill = $bill;

		return $this;
	}

	/**
	 * Get reader for current directory
	 *
	 * @return \SplFileInfo
	 */
	public function getReader(): \SplFileInfo
	{
		$path = $this->asPath();
		if (is_dir($path)) {
			debug("Path encountered in `%s'. getDirectoryEnumerator() should be used instead of getReader()!", $path);

			return $this->getDirectoryEnumerator();
		}

		return new \SplFileInfo($path);
	}

	/**
	 * Same as above
	 *
	 * @return \DirectoryIterator
	 */
	public function getDirectoryEnumerator(): \DirectoryIterator
	{
		$path = $this->asPath();

		return new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS);
	}

	/**
	 * Convert pathmap to path
	 *
	 * @return string
	 */
	private function asPath(): string
	{
		$file = static::PATHMAP_PATH;
		if (!static::PATHMAP_PATH) {
			$file = snake_case(basename(static::getBaseClassName()), '_');
		}

		return $this->stream->getPrefix() . DIRECTORY_SEPARATOR . $file;
	}

	/**
	 * Move path taking into account path remapping
	 *
	 * @param \apnscpFunctionInterceptor $afi
	 * @param string                     $src
	 * @param string                     $dest
	 * @return bool
	 */
	protected function transmigratePath(\apnscpFunctionInterceptor $afi, string $src, string $dest): bool
	{
		if ($src === $dest) {
			return true;
		}
		if (!$afi->file_exists($src)) {
			return warn('Failed to move %s - file does not exist or moved earlier', $src);
		}
		if ($afi->file_exists($dest)) {
			$afi->file_delete($dest, true);
		}
		$stat = $afi->file_stat($src);

		if ($stat['link']) {
			$path = readlink($afi->file_make_path($src));
			$afi->file_delete($src);

			return $afi->file_symlink($path, $dest);
		}

		if (!$afi->file_move($src, $dest, true)) {
			return warn('Failed to move %s to %s', $src, $dest);
		}

		return true;
	}

	/**
	 * Convert old filesystem path to new
	 *
	 * @param SiteConfiguration $s
	 * @param string            $src
	 * @param string            $dest
	 * @return bool
	 */
	protected function convertPath(SiteConfiguration $s, string $src, string $dest): bool
	{
		$translator = new PathTranslate();
		// sticky + chown makes this immune to trickery
		$tmp = tempnam($s->getAccountRoot() . '/tmp', 'trans');
		$master = $s->getAccountRoot() . '/tmp/panel-conversion-files.txt';
		if (!file_exists($master)) {
			touch($master);
		}
		chown($master, 0);
		$stat = [$master, $s->getAuthContext()->user_id, $s->getAuthContext()->group_id, 0600];
		defer($_, static function () use ($tmp, $stat) {
			file_exists($tmp) && unlink($tmp);
			Filesystem::chogp(...$stat);
		});
		$translator->setLogPath($tmp);
		$translator->translate($src, $dest,$s->getAccountRoot() . $dest);
		return file_put_contents($master, file_get_contents($tmp), FILE_APPEND) !== false;
	}

	/**
	 * Parse mapped file as ini
	 *
	 * @param string|null $path optional pathname
	 * @return array
	 */
	protected function readFromIni(string $path = null): array
	{
		if (!$path) {
			$reader = $this->getDirectoryEnumerator();
			$path = $reader->getPathname();
		} else if (is_dir($path)) {
			// @XXX ugly
			$path = (new \DirectoryIterator($path))->getPathname();
		}

		$fp = tmpfile();
		$fname = stream_get_meta_data($fp)['uri'];
		copy($path, $fname);
		$map = Map::load($fname, 'r-', 'inifile');

		return $map->section('')->fetchAll();
	}

	abstract public function parse(): bool;

}