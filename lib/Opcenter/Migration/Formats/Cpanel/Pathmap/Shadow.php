<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2019
 */

	namespace Opcenter\Migration\Formats\Cpanel\Pathmap;


	use Event\Cardinal;
	use Event\Events;
	use Opcenter\Account\Suspend;
	use Opcenter\Migration\Formats\Cpanel\ConfigurationBuilder;
	use Opcenter\SiteConfiguration;

	class Shadow extends ConfigurationBuilder
	{
		public function parse(): bool
		{
			$path = $this->getReader()->getPathname();
			if (!file_exists($path)) {
				return warn('No password found - random password will be used');
			}
			$crypted = trim(file_get_contents($path));
			$enabled = true;
			if (strncmp($crypted, '!!', 2) === 0) {
				warn('cPanel goobledygook to disable login for user, stripping !! and forcing disablement');
				$crypted = substr($crypted, 2);
				$enabled = false;
			}
			$this->bill->set('auth', 'cpasswd', $crypted);
			if (!$enabled) {
				Cardinal::register(
					[\Opcenter\Account\Import::HOOK_ID, Events::SUCCESS],
					static function ($event, SiteConfiguration $s) {
						// suspend site
						register_shutdown_function(
							static function() use ($s) {
								// suspension terminates all active sessions
								// destroying any afi instantiations
								(new Suspend($s->getSite()))->exec();
							}
						);
					}
				);
			}

			return true;
		}
	}

