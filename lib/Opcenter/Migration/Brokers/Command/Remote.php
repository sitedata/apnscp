<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2022
	 */

	namespace Opcenter\Migration\Brokers\Command;

	use Opcenter\Migration\Brokers\Command;

	class Remote extends Command
	{
		protected string $hostname = 'localhost';

		public function __construct(string $hostname)
		{
			$this->hostname = $hostname;
		}


		/**
		 * @inheritDoc
		 */
		public function run(string $command, ...$args): array
		{
			$proc = new \Util_Process_Safe();

			// open OOB FD just in case this is specified. ssh doesn't allow multiple fds to pass back thru to client
			$localcmd = 'ssh ' . $this->hostname . " -- 'exec " .
				\Util_Account_Editor::OOB_FD . ">&1' \\; %s";
			$formatted = (new \Util_Process_Safe())->setOption('run', false)->run($command, ...$args)->getCommand(true);
			// nice perk to always using sh, no need to worry about wrapping
			return $proc->run($localcmd, $formatted);
		}
	}
