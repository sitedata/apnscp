<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace Opcenter;

	interface MapInterface
	{
		/**
		 * Filter term in key/value pairs by section
		 *
		 * @param string $term
		 * @return \Opcenter\Map
		 */
		public function section(?string $term): self;

		/**
		 * Value is quoted
		 *
		 * @param bool|null $val
		 * @return $this|bool
		 */
		public function quoted(bool $val = null);

		/**
		 * Close opened database
		 */
		public function close();

		/**
		 * Retrieve a key from database
		 *
		 * @param string $key
		 * @return string|false
		 */
		public function fetch($key);

		/**
		 * Remove a key from database
		 *
		 * @param $key
		 * @param
		 * @return bool
		 */
		public function delete($key): bool;

		/**
		 * Insert a new key into the database
		 *
		 * @param string $key
		 * @param string $value
		 * @return bool
		 */
		public function insert(string $key, string $value): bool;

		/**
		 * Insert or replace key depending upon presence
		 *
		 * @param string $key
		 * @param string $value
		 * @return bool
		 */
		public function set(string $key, string $value): bool;

		/**
		 * Get all values
		 *
		 * @return array
		 */
		public function fetchAll(): array;

		/**
		 * Update a key with new value
		 *
		 * @param string $key
		 * @param string $value
		 * @return bool
		 */
		public function replace(string $key, string $value): bool;

		/**
		 * Check if key exists
		 *
		 * @param string $key
		 * @return bool
		 */
		public function exists(string $key): bool;

		/**
		 * Synchronize contents to disk
		 *
		 * @return bool
		 */
		public function save(): bool;

		/**
		 * Truncate map
		 */
		public function truncate();

		/**
		 * Copy database contents into instance
		 *
		 * @param string|array $contents file or contents to copy
		 * @return bool
		 */
		public function copy($contents);

		/**
		 * Optimize database
		 */
		public function optimize();

		public function offsetExists($offset);

		public function offsetGet($offset);

		public function offsetSet($offset, $value);

		public function offsetUnset($offset);

		public function current();

		public function rewind();

		public function key();

		public function valid(): bool;

		public function next(): void;
	}