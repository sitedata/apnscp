<?php
	declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Opcenter\Http\Apache;

	use Illuminate\Contracts\Support\Arrayable;
	use Opcenter\Http\Apache;

	class Map implements \ArrayAccess, \Iterator, Arrayable
	{
		const MODE_READ = 'r';
		const MODE_WRITE = 'w';
		const COMMAND = ['httxt2dbm', '-i', '%(input)s', '-o', '%(output)s', '-f', '%(tformat)s'];
		const DOMAIN_MAP = 'domain_map';
		const DEFAULT_FORMAT = 'SDBM';
		/**
		 * @var array instance map
		 */
		protected static $instances = [];
		/**
		 * @var string filename
		 */
		protected $file;
		/**
		 * @var resource file pointer for locking
		 */
		protected $fp;
		/**
		 * @var array $lines parsed lines
		 */
		protected $lines;
		/**
		 * @var bool whether map needs to be updated
		 */
		protected $dirty = false;
		/**
		 * @var string mode map is opened in, read or write
		 */
		protected $mode;
		/**
		 * @var string httxt2dbm output file
		 */
		protected $output;
		/**
		 * httxt2dbm output format
		 *
		 * @var string
		 */
		protected $format = self::DEFAULT_FORMAT;

		/**
		 * Map constructor.
		 *
		 * @param string $file input filename
		 * @param string $mode mode
		 */
		protected function __construct(string $file, string $mode = self::MODE_READ)
		{
			$this->file = $file;
			$this->output = $file;
			if ($mode[0] !== self::MODE_READ && $mode[0] !== self::MODE_WRITE[0]) {
				\Error_Reporter::print_debug_bt();
				fatal("map mode must be %s or %s, `%s' given for `%s'",
					self::MODE_READ, self::MODE_WRITE, $mode, $file);
			}
			$this->mode = $mode[0] === self::MODE_READ ? 'read' : 'write';
			if ($this->mode === 'write') {
				$this->fp = $this->lock($file);
			}
			$this->lines = $this->parseFile($file);
		}

		protected function lock(string $file)
		{
			$locked = false;
			$fp = fopen($file, 'r+');
			for ($i = 0; $i < 100; $i++) {
				if (flock($fp, LOCK_EX | LOCK_NB, $locked)) {
					break;
				}
				usleep(500000);
			}
			if ($locked) {
				fatal("failed to acquire lock in `%s'", $file);
			}

			return $fp;
		}

		protected function parseFile(string $file)
		{
			$parsed = [];
			if (!file_exists($file)) {
				return $parsed;
			}
			$lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
			foreach ($lines as $line) {
				$tmp = explode(' ', $line, 2);
				$parsed[$tmp[0]] = $tmp[1];
			}

			return $parsed;
		}

		/**
		 * Open an Apache map
		 *
		 * @param string $mapFile
		 * @param string $mode r or w
		 * @return Map
		 */
		public static function open(string $mapFile, string $mode = self::MODE_READ): Map
		{
			if (!file_exists($mapFile)) {
				fatal("Map `%s' does not exist", $mapFile);
			}

			return new static($mapFile, $mode);
		}

		/**
		 * Get domain map from site
		 *
		 * @param string $infoPath
		 * @param string $site
		 * @param string $mode
		 * @return Map|null
		 */
		public static function fromSiteDomainMap(string $infoPath, string $site, string $mode = self::MODE_READ): ?Map
		{
			if ($mode === self::MODE_READ && !file_exists("${infoPath}/" . static::DOMAIN_MAP)) {
				return null;
			}

			$map = new static("${infoPath}/" . static::DOMAIN_MAP, $mode);
			$map->setOutput(Apache::DOMAIN_PATH . "/${site}");

			return $map;
		}

		public function setOutput(string $file)
		{
			$this->output = $file;
		}

		public function getOutput(): ?string
		{
			return $this->output;
		}

		public function __destruct()
		{
			if ($this->dirty) {
				$this->sync();
			}
			if ($this->fp) {
				$this->unlock($this->fp);
			}
		}

		/**
		 * Synchronize Apache map to compiled map
		 *
		 * @return bool
		 */
		public function sync(): bool
		{
			if ($this->mode !== 'write') {
				fatal("can't save map `%s': opened in read-only mode", $this->file);
			}
			$mapText = $this->getMapText();
			ftruncate($this->fp, 0);
			rewind($this->fp);
			fwrite($this->fp, $mapText);

			if (!$mapText) {
				// @xxx httxt2dbm doesn't write a zero-byte map
				foreach (glob($this->output . '.*', GLOB_NOSORT) as $file) {
					file_exists($file) && !is_link($file) && unlink($file);
				}
			}

			$args = [
				'input'   => $this->file,
				'output'  => $this->output,
				'tformat' => $this->format
			];
			$ret = \Util_Process::exec(self::COMMAND, $args);
			if (!$ret['success']) {
				return error("failed to update map `%s': %s", $this->file,
					coalesce($ret['stderr'], $ret['stdout']));
			}
			$this->dirty = false;

			return $ret['success'];
		}

		/**
		 * Transform map into text
		 *
		 * @return string
		 */
		protected function getMapText(): string
		{
			return implode("\n",
				array_map(static function ($k, $v) {
					return "${k} ${v}";
				}, array_keys($this->lines), $this->lines)
			);

		}

		protected function unlock($fp): bool
		{
			if (!\is_resource($fp)) {
				fatal("file pointer is not a resource, got `%s'", \gettype($fp));
			}
			flock($fp, LOCK_UN);

			return fclose($fp);
		}

		public function removeValues($values): array
		{
			$items = array_intersect($this->lines, (array)$values);
			$this->removeKeys(array_keys($items));

			return $items;
		}

		/**
		 * Remove all named keys
		 *
		 * @param array $keys
		 */
		public function removeKeys(array $keys): void
		{
			$this->dirty = true;
			foreach ($keys as $key) {
				unset($this->lines[$key]);
			}
		}

		public function next(): void
		{
			next($this->lines);
		}

		#[\ReturnTypeWillChange]
		public function key()
		{
			return key($this->lines);
		}

		public function valid(): bool
		{
			return $this->current() !== false;
		}

		#[\ReturnTypeWillChange]
		public function current()
		{
			return current($this->lines);
		}

		#[\ReturnTypeWillChange]
		public function rewind()
		{
			reset($this->lines);
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return isset($this->lines[$offset]);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return $this->lines[$offset];
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			$this->dirty = true;
			$this->lines[$offset] = $value;
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			$this->dirty = true;
			unset($this->lines[$offset]);
		}

		public function toArray()
		{
			return $this->lines;
		}

		public function getExtensionFromType(): string
		{
			switch ($this->format)
			{
				case 'SDBM':
					return '.pag';
				default:
					return '';
			}
		}
	}
