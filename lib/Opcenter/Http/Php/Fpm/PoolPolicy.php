<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2020
 */

namespace Opcenter\Http\Php\Fpm;

use Opcenter\Admin\Settings\Apache\PhpVersion;
use Opcenter\Contracts\VirtualizedContextable;
use Opcenter\Provisioning\ConfigurationWriter;
use Opcenter\SiteConfiguration;
use Symfony\Component\Yaml\Yaml;

class PoolPolicy implements VirtualizedContextable
{
	use \ContextableTrait;
	use \FilesystemPathTrait;

	private const CACHE_KEY = "php.policy";
	public const POLICY_FILE = "php-policy.yml";

	/**
	 * @var array
	 */
	protected $whitelist = [];
	/**
	 * @var array
	 */
	protected $blacklist = [];
	/**
	 * @var array
	 */
	protected $policy;

	protected $dirty = false;

	protected function __construct() {
		$cache = \Cache_Account::spawn($this->getAuthContext());
		$item = $cache->get(self::CACHE_KEY);

		if (!$item || filemtime($this->getPolicyPath()) > $item['ts']) {
			$this->build();
			$this->sync();
		} else {
			$this->policy = $item['policy'];
		}

		$this->blacklist = (array)$this->policy['blacklist'];
		if (\in_array('*', $this->blacklist, true)) {
			$this->blacklist = array_keys(MultiPhp::list());
		}
		$this->whitelist = (array)$this->policy['whitelist'];
		if (\in_array('*', $this->whitelist, true)) {
			$this->whitelist = [];
		}
	}

	public function __destruct() {
		if ($this->dirty) {
			$this->sync();
		}
	}

	private function build() {
		$file = $this->getPolicyPath();
		// @TODO cache
		$baseCfg = Yaml::parse(
			(string)(new ConfigurationWriter(
				'apache.php.policy',
				SiteConfiguration::shallow($this->getAuthContext())
			))
		);

		$this->policy = array_replace_recursive($baseCfg,
			file_exists($file) ? (array)Yaml::parse(file_get_contents($file)) : []);

	}

	public function sync(): self
	{
		$fp = fopen($this->getPolicyPath(), 'w');
		$would = false;
		for ($i = 0; $i < 10; $i++) {
			flock($fp, LOCK_EX|LOCK_NB, $would);
			if (!$would) {
				break;
			}
			usleep(50000);
		}
		if ($would) {
			fatal("Cannot acquire lock on PHP Pool");
		}
		ftruncate($fp, 0);
		if (false !== fwrite($fp, Yaml::dump($this->policy, 2, 4, Yaml::DUMP_OBJECT_AS_MAP))) {
			if (PHP_VERSION_ID >= 80100) {
				fsync($fp);
			}
			$this->dirty = false;
			$cache = \Cache_Account::spawn($this->getAuthContext());
			$cache->set(self::CACHE_KEY, ['policy' => $this->policy,  'ts' => filemtime($this->getPolicyPath())]);
		} else {
			warn("Failed to save policy map to %s", $this->getPolicyPath());
		}
		fclose($fp);
		return $this;
	}

	/**
	 * Get policy path file
	 *
	 * @return string
	 */
	private function getPolicyPath(): string
	{
		return $this->domain_info_path(self::POLICY_FILE);
	}

	/**
	 * PHP-FPM version allowed
	 *
	 * @param string $version
	 * @return bool
	 */
	public function allowed(string $version): bool
	{
		return !$this->whitelist && !in_array($version, $this->blacklist, true)
			|| \in_array($this->whitelist, $version, true);
	}

	/**
	 * Get configured pool version
	 *
	 * @param string $pool
	 * @return string
	 */
	public function getPoolVersion(string $pool = ''): string
	{
		return $this->get($pool, 'version') ?? $this->getSystemVersion();
	}

	/**
	 * Get default system interpreter
	 *
	 * @return string
	 */
	public function getSystemVersion(): string
	{
		static $version;
		if (null !== $version) {
			return $version;
		}
		return $version = (string)(new PhpVersion())->get();
	}

	/**
	 * Get policy setting
	 *
	 * @param string|null $pool
	 * @param string      $field
	 * @return array|\ArrayAccess|mixed|null
	 */
	public function get(?string $pool, string $field, $default = null)
	{
		if (isset($this->policy['pools'][$pool]) &&
			null !== ($val = array_get($this->policy['pools'][$pool], $field)))
		{
			return $val;
		}
		if (array_has($this->policy['global'], $field)) {
			return array_get($this->policy['global'], $field, $default);
		}

		warn("Field `%s' does not exist in policy map", $field);
		return $default;
	}

	public function getPhpAdminSettings(?string $pool): array
	{
		if (!$pool) {
			return (array)$this->get('', 'php_settings');
		}
		return (array)$this->get($pool, 'php_settings') +
			(array)$this->get('', 'php_settings');
	}

	/**
	 * Set policy value
	 *
	 * @param string|null $pool
	 * @param string      $field
	 * @param             $value
	 * @return $this
	 */
	public function set(?string $pool, string $field, $value): self
	{
		if ($pool) {
			if (!isset($this->policy['pools'][$pool])) {
				$this->policy['pools'][$pool] = [];
			}
			array_set($this->policy['pools'][$pool], $field, $value);
		} else {
			array_set($this->policy['global'], $field, $value);
		}

		$this->dirty = true;

		return $this;
	}
}
