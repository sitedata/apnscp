<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2023
	 */

	namespace Opcenter\System\Cgroup\v2\Attributes;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;
	use Opcenter\System\Cgroup\v2\Contracts\NestedKey;

	class NestedKeyAttribute extends BaseAttribute implements NestedKey
	{
		protected array $subattributes;

		public function getSubattributes(): array
		{
			return $this->subattributes;
		}

		public function setSubattribute(string $attr, mixed $val): bool
		{
			$this->subattributes[$attr] = $val;

			return true;
		}

		public function getSubattributeValue(string $attr): ?int
		{
			if (isset($this->subattributes[$attr])) {
				return (int)$this->subattributes[$attr];
			}

			return null;
		}

		public function mergeSubattributes(NestedKeyAttribute $attr): self
		{
			$this->subattributes = array_merge($this->subattributes, $attr->getSubattributes());

			return $this;
		}

	}