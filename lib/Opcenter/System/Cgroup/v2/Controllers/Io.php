<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, March 2023
 */

namespace Opcenter\System\Cgroup\v2\Controllers;

class Io extends \Opcenter\System\Cgroup\v1\Controllers\Blkio
{
	protected const METRIC_NS_OVERRIDE = 'blkio';

	protected const SERVICE_ATTRIBUTES = [
		'writeiops' => 'io.max=wiops',
		'readiops'  => 'io.max=riops',
		'writebw'   => 'io.max=wbps',
		'readbw'    => 'io.max=rbps',
		'ioweight'  => 'io.weight'
	];

	protected const V2_REMAPS = [
		'blkio.throttle.io_serviced:Read'       => 'io.stat=rios',
		'blkio.throttle.io_serviced:Write'      => 'io.stat=wios',
		'blkio.throttle.io_service_bytes:Read'  => 'io.stat=rbytes',
		'blkio.throttle.io_service_bytes:Write' => 'io.stat=wbytes',

		'blkio.throttle.write_iops_device' => 'io.max=wiops',
		'blkio.throttle.read_iops_device'  => 'io.max=riops',
		'blkio.throttle.read_bps_device'   => 'io.max=rbps',
		'blkio.throttle.write_bps_device'  => 'io.max=wbps',
	];

	public function reset(): bool
	{
		return debug("Unsupported");
	}

	protected function statsWrapper(array $stats): array
	{
		if (!$this->exists()) {
			return self::BLKIO_TEMPLATE;
		}

		$targetDevice = \Opcenter\Filesystem\Mount::getPhyBlockFromPath(FILESYSTEM_VIRTBASE);
		$raw = file($this->getPath() . '/io.stat', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
		array_first($raw, static function ($v) use ($targetDevice) {
			strtok($v, ' ');
			return $v === $targetDevice;
		});

		while (false !== ($statKey = strtok('='))) {
			$statVal = strtok(' ');
			$keyType = str_ends_with($statKey, 'bytes') ? 'bw' : 'iops';
			if ($statKey[0] === 'r') {
				$statKey = 'read';
			} else if ($statKey[0] === 'w') {
				$statKey = 'write';
			} else if ($statKey[0] === 'd') {
				$statKey = 'discard';
			} else {
				debug("Unknown prefix type: %s", $statKey);
				continue;
			}
			$stats[$keyType . '-' . $statKey] = (int)$statVal;
		}

		return $stats;
	}

	protected function getCounter(string $metric): string
	{
		$counter = parent::getCounter($metric);

		return static::V2_REMAPS[$counter] ?? $counter;
	}

	protected function readCounter(string $path, string $parameter = null): string
	{
		[$path, $type] = $this->tokenize($path);
		$raw = parent::readCounter($path, $parameter);
		$str = strtok($raw, "\n");
		$targetDevice = \Opcenter\Filesystem\Mount::getPhyBlockFromPath(FILESYSTEM_VIRTBASE);
		do {
			if (!str_starts_with((string)$str, $targetDevice)) {
				continue;
			}
			if (false === ($pos = strpos($str, " $type="))) {
				continue;
			}

			$counter = substr($str . ' ', $newpos = ++$pos + \strlen("{$type}="), strpos($str, " ", $newpos) - $newpos);
			if (!str_ends_with($type, 'bytes')) {
				return $counter;
			}
			return (string)((int)$counter / 1024);
		} while (false !== ($str = strtok("\n")));

		if (filesize($path) > 0) {
			// empty until populated
			debug("Unknown/invalid %(controllers)s counter attr `%(attr)s' in %(path)s", [
				'controller' => 'io', 'attr' => $type, 'path' => $path
			]);
		}

		return '0';
	}
}