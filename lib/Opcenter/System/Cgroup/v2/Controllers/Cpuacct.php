<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, March 2023
	 */

	namespace Opcenter\System\Cgroup\v2\Controllers;

	class Cpuacct extends \Opcenter\System\Cgroup\Controllers\Cpuacct
	{
		public function stats(): array
		{
			$stats = array(
				'used'     => null,
				'system'   => null,
				'free'     => null,
				'limit'    => null,
				'user'     => null,
				'procs'    => [],
				'maxprocs' => null
			);

			if (!$this->exists()) {
				return $stats;
			}

			// nano to seconds
			['usage' => $used, 'user' => $user, 'system' => $system] = $this->readMetrics([
				'usage',
				'user',
				'system'
			]);
			$stats['used'] = $used / 100;
			$stats['user'] = $user / 100;
			$stats['system'] = $system / 100;
			// CPU counters are delta counters, which requires knowing the previous val
			$stats['free'] = null;
			$stats['procs'] = $this->tasks();

			return $stats;
		}

		protected function getCounter(string $metric): string
		{
			$counter = parent::getCounter($metric);
			return dirname($counter) . '/cpu.stat';
		}

		/**
		 * Convert counter data into centiseconds
		 *
		 * @param string      $path
		 * @param string|null $parameter
		 * @return string
		 */
		protected function readCounter(string $path, string $parameter = null): string
		{
			// all counter values stored in cpu.stat
			// ascertain requested counter type from path for v1 compatibility
			$path = dirname($path) . '/cpu.stat';

			if (!file_exists($path)) {
				// fallback to /proc/stat
				$fp = fopen('/proc/stat', 'r');
				$aggregate = fgets($fp);
				fclose($fp);
				$fields = explode(" ", $aggregate);
				return (string)(match($parameter) {
					"user" => (int)$fields[2],
					"system" => (int)$fields[4],
					"usage" => (int)$fields[2] + (int)$fields[4]
				}/CPU_CLK_TCK*100);

				fatal("Unknown counter requested: %s", $parameter);
			}

			$label = $parameter === 'user' ? 'user_usec' : ($parameter === 'system' ? 'system_usec' : 'usage_usec');
			$val =  $this->readCounterLabel($path, $label);
			// usec to centisec
			return (string)((int)$val / 1e4);
		}
	}