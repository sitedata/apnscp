<?php declare(strict_types=1);
	/*
	 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * 	Unauthorized copying of this file, via any medium, is
	 * 	strictly prohibited without consent. Any dissemination of
	 * 	material herein is prohibited.
	 *
	 * 	For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * 	Written by Matt Saladna <matt@apisnetworks.com>, July 2023
	 */

	namespace Opcenter\System\Cgroup\Attributes\Pids;

	use Opcenter\System\Cgroup\Attributes\BaseAttribute;

	class Max extends BaseAttribute
	{
		const UNLIMITED = 'max';
	}