<?php declare(strict_types=1);
	/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, May 2023
 */

	namespace Opcenter\System\Cgroup\Controllers;

	use Opcenter\System\Cgroup\Controller;

	abstract class Memory extends Controller
	{
		// service attributes
		protected const SERVICE_ATTRIBUTES = [
			'memory' => 'memory.limit_in_bytes'
		];

		public const LOGGABLE_METRICS = [
			'used' => [
				'label'   => 'Memory used (KB)',
				'type'    => 'value',
				'counter' => 'memory.usage_in_bytes'
			],
			// available in v1 or v2 since 5.19
			'peak' => [
				'label'   => 'Memory peak (KB)',
				'type'    => 'value',
				'counter' => 'memory.max_usage_in_bytes'
			],
			'oom' => [
				'label'   => 'Memory maxed condition', 'type'    => 'monotonic',
				'counter' => 'memory.failcnt'
			]
		];

		public function stats(): array
		{

			$stats = array(
				'used'     => 0,
				'peak'     => 0,
				'free'     => null,
				'limit'    => null,
				'procs'    => array(),
				'detailed' => array(),
				'oom'      => null
			);

			if (!$this->exists()) {
				return $stats;
			}

			$stats = array_combine(['used', 'peak', 'limit', 'oom'],
				array_map('intval', $this->readMetrics(['used', 'peak', 'memory', 'oom'])));
			$stats['procs'] = $this->tasks();
			/*
			 * Odd issue on root controller. Normally used < peak < limit, except
			 * that peak < used < limit... cap
			 */
			$stats['used'] *= 1024;
			$stats['peak'] *= 1024;

			if (!(string)$this->group) {
				$stats['peak'] = max($stats['peak'], $stats['used']);
			}

			return $stats;
		}


		public function readCounter(string $path, string $parameter = null): string
		{
			$counter = parent::readCounter($path, $parameter);
			if (substr($path, -4) === '/oom') {
				return $counter;
			}

			return $counter;
		}

		public function readMetrics(array $params): array
		{
			$vals = parent::readMetrics($params);

			foreach(['peak','used'] as $k) {
				if (isset($vals[$k])) {
					$vals[$k] /= 1024;
				}
			}

			return $vals;
		}


		public function reset(): bool {
			foreach (['', 'memsw.', 'kmem.'] as $counter) {
				$this->write("memory.${counter}max_usage_in_bytes", 0);
			}

			return true;
		}

	}
