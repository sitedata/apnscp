<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup\v1;


	use Opcenter\System\Cgroup;

	abstract class Controller extends Cgroup\BaseController
	{

		public function getPath(): string
		{
			return CGROUP_HOME . '/' . $this->type . '/' . $this->group;
		}

		public function exists(): bool
		{
			return file_exists($this->getPath() . '/tasks');
		}

		public function tasks(): array
		{
			if (!file_exists($path = $this->getPath() . '/tasks')) {
				return [];
			}

			return array_map('intval', file($path, FILE_IGNORE_NEW_LINES));
		}

		public function charge(int $pid): bool
		{
			return file_put_contents($this->getPath() . '/tasks', $pid, FILE_APPEND) > 0;
		}
	}