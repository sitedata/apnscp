<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2018
	 */

	namespace Opcenter\System\Cgroup;

	use Opcenter\System\Cgroup;
	use Opcenter\System\Cgroup\Contracts\Renderable;

	class Group implements Renderable
	{
		// group default permissions

		// @var string group name
		protected $name;
		// @var Controllers[]
		protected array $controllers = [];
		// @var Permissions controller permission
		protected $permissions;

		public function __construct(?string $name, array $permissions = [])
		{
			$this->name = $name;
			$this->permissions = new Permissions($permissions);
		}

		public function __toString(): string
		{
			// suffix .slice for systemd compatibility
			return (string)$this->name;
		}

		/**
		 * Controllers bound to group
		 *
		 * @return Controller[]
		 */
		public function getControllers(): array
		{
			return $this->controllers;
		}

		public function exists(): bool
		{
			if (Cgroup::version() === 2) {
				// @XXX all controllers in 1 directory, API prevents passing null controller for now
				// mock up a dummy controlller to get path
				$controller = Controller::make($this, "freezer");

				return $controller->exists();
			}

			foreach (Cgroup::getControllers() as $controller) {
				$controller = Controller::make($this, $controller);
				if ($controller->exists()) {
					return true;
				}
			}

			return false;
		}

		/**
		 * Add controller to cgroup
		 *
		 * @param Controller $controller
		 */
		public function add(Controller $controller): void
		{
			$this->controllers[$controller->getName()] = $controller;
		}

		/**
		 * Get group permissions
		 *
		 * @return array
		 */
		public function getPermissions(): array
		{
			return $this->permissions->get();
		}

		/**
		 * Build configuration
		 *
		 * @return string
		 */
		public function build(): string
		{
			$config = 'group ' . $this . ' { ' .
				$this->permissions->build() . ' ';
			foreach ($this->controllers as $c) {
				$config .= $c->build();
			}

			return $config .
				'}';
		}


	}