<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2018
	 */

	declare(strict_types=1);

	namespace Opcenter\System;

	use Opcenter\Filesystem;
	use Opcenter\Filesystem\Mount;
	use Opcenter\System\Cgroup\Controller;
	use Opcenter\System\Cgroup\Controllers\Blkio;
	use Opcenter\System\Cgroup\Group;
	use Opcenter\System\Cgroup\v2\Controllers\Io;
	use Util_Process;

	class Cgroup
	{
		const CGROUP_HOME = CGROUP_HOME;
		const CGROUP_SERVER_DIR = '/etc/cgconfig.d';
		const CGROUP_SITE_CONFIG = '/etc/cgrules.conf';
		const CGROUP_CONTROLLERS = CGROUP_CONTROLLERS;

		/**
		 * Get CPU usage for controller
		 *
		 * @param string $group group name
		 * @return array
		 */
		public static function cpu_usage(?string $group): array
		{
			return Controller::make(new Group($group), 'cpuacct')->stats();
		}

		public static function io_usage(?string $group): array
		{
			return Controller::make(new Group($group), Cgroup::version() === 1 ? Blkio::class : Io::class)->stats();
		}

		public static function pid_usage(?string $group): array
		{
			return Controller::make(new Group($group), 'pids')->stats();

		}

		/**
		 * Get memory usage from controller
		 *
		 * @deprecated
		 * @param string $group group name
		 * @return array
		 */
		public static function get_memory(?string $group): array
		{
			deprecated_func('Use memory_usage');
			return static::memory_usage($group);
		}

		/**
		 * Get memory usage from controller
		 *
		 * @param string $group group name
		 * @return array
		 */
		public static function memory_usage(?string $group): array
		{
			return Controller::make(new Group($group), 'memory')->stats();
		}

		/**
		 * Create a new group within a controller
		 *
		 * @param Group $group
		 * @return bool
		 */
		public static function create(Group $group): bool
		{
			if (!$group->getControllers()) {
				return debug("No controllers detected in %s", $group);
			}
			foreach ($group->getControllers() as $controller) {
				if (!$controller->getAttributes()) {
					// no-op
					continue;
				}

				if (!self::exists((string)$controller)) {
					return error("cgroup `%s' doesn't exist", (string)$controller);
				}

				if (!$controller->exists()) {
					$controller->create();
				}
			}

			if (!self::addConfiguration($group)) {
				warn('failed to populate cgroup configuration!');
			}

			return true;
		}

		/**
		 * Add cgroup configuration
		 *
		 * @param Group $group
		 * @return bool
		 */
		protected static function addConfiguration(Group $group): bool
		{
			$config = self::configurationFromGroup($group);
			if (!is_file($config) && !is_dir(\dirname($config)) && !mkdir($concurrentDirectory = \dirname($config)) && !is_dir($concurrentDirectory)) {
					fatal('Directory "%s" was not created', $concurrentDirectory);
			}

			return file_put_contents($config, $group->build(), LOCK_EX) > 0;
		}

		public static function configurationFromGroup(string|Group $group): string
		{
			return self::CGROUP_SERVER_DIR . '/' . $group;
		}

		/**
		 * Delete a group from a controller
		 * s
		 *
		 * @param Group      $group
		 * @param Controller $controller
		 * @return bool
		 */
		public static function delete(Group $group, Controller $controller): bool
		{
			$cgpath = $controller->getPath();

			if (!file_exists($cgpath)) {
				return error("cgroup controller `%s' doesn't exist",
					$cgpath);
			}

			if (!static::exists($controller)) {
				return true;
			}

			$ret = Util_Process::exec(['cgdelete', '-r', '%(controller)s:%(group)s'],
				['controller' => $controller, 'group' => $group]);

			return $ret['success'];
		}

		/**
		 * Remove group from cgroup configuration
		 *
		 * @param Group      $group      group name
		 * @param Controller $controller optional group (not implemented)
		 * @return bool
		 */
		public static function removeConfiguration(Group $group): bool
		{
			$config = self::CGROUP_SERVER_DIR . '/' . $group;
			if (!is_file($config)) {
				return false;
			}

			return unlink($config) ?: error("failed to remove cg configuration `%s'", $group);
		}

		/**
		 * Controller exists
		 *
		 * @param Controller|string $controller controller name, group-specific or global
		 * @return bool
		 */
		public static function exists($controller): bool
		{
			if (self::version() === 2) {
				$systemHasController = in_array($controller, self::getControllers(), true);
				return is_string($controller) ? $systemHasController :
					is_file($controller->getPath() . '/cgroup.procs');
			}

			$path = (is_string($controller) ?
				self::CGROUP_HOME . '/' . $controller : $controller->getPath()) . '/tasks';

			return is_file($path);
		}

		/**
		 * Charge a process
		 *
		 * @param Group    $group
		 * @param int|null $pid
		 * @return bool
		 */
		public static function charge(Group $group, int $pid = null): bool
		{
			if (self::version() === 2) {
				// @XXX all controllers in 1 directory, API prevents passing null controller for now
				// mock up a dummy controlller to get path
				$controller = Controller::make($group, "freezer");
				return $controller->exists() && $controller->charge($pid ?? posix_getpid());
			}
			$ret = true;
			foreach (static::getControllers() as $controller) {
				$controller = Controller::make($group, $controller);
				if (!$controller->exists()) {
					continue;
				}
				if ($controller->immmediateBinding()) {
					$ret &= $controller->charge($pid ?? posix_getpid());
				}
			}

			return (bool)$ret;
		}

		/**
		 * Get all configured controllers
		 *
		 * @return array
		 */
		public static function getControllers(): array
		{
			return self::CGROUP_CONTROLLERS;
		}

		/**
		 * Get controller class name from cgroup service class
		 *
		 * @param string $svc
		 * @return string|null
		 */
		public static function resolveParameterController(string $svc): ?string
		{
			static $hash = [];
			if (isset($hash[$svc])) {
				return $hash[$svc];
			}

			$found = null;
			foreach (self::getControllers() as $controller) {
				$controller = \Opcenter\System\Cgroup\Controller::make(new Group(null), $controller);
				if ($controller->hasParameter($svc)) {
					$found = get_class($controller);
					break;
				}
			}

			return $hash[$svc] = $found;
		}

		/**
		 * cgroup controller interface
		 * @param string      $controller
		 * @param string|null $subtree
		 * @return bool
		 */
		public static function mounted(string $controller, string $subtree = null): bool
		{
			if (Cgroup::version() === 1) {
				return Mount::mounted(FILESYSTEM_SHARED . "/{$controller}");
			}

			$path = FILESYSTEM_SHARED . '/cgroup' . ($subtree ? "/{$subtree}" : '') . '/cgroup.controllers';
			if (!is_file($path)) {
				return false;
			}
			$contents = file_get_contents($path);
			$tok = strtok($contents, " \n");
			do {
				if ($tok === $controller) {
					return true;
				}
			} while (false !== ($tok = strtok(" \n")));

			return false;
		}

		/**
		 * Mount all cgroup controllers into shared slice
		 *
		 * @return bool
		 */
		public static function mountAll(): bool
		{
			if (Cgroup::version() === 2) {
				$path = FILESYSTEM_SHARED . '/cgroup';
				if (Mount::mounted($path)) {
					return true;
				}

				return Mount::mount($path, 'cgroup2',
					[
						'noexec'    => true,
						'nosuid'    => true,
						'nodev'     => true,
						'rw'        => true,
					]) ?: error("Failed to mount cgroup controller");

			}

			foreach (self::getControllers() as $controller) {
				if ($controller === 'cpu') {
					// special case merged controller
					$controller = 'cpu,cpuacct';
				} else if ($controller === 'cpuacct') {
					continue;
				}
				// Ideally applied against /sys/fs/cgroup with /.socket/cgroup
				// a slave to /sys/fs/cgroup. File descriptor woes in 3.10.x kernels
				// in C7 prevent filesystem from flushing fully on delete requiring
				// a second cgroup mount into /.socket and working exclusively off that layer
				$path = FILESYSTEM_SHARED . "/cgroup/${controller}";
				if (Mount::mounted($path)) {
					continue;
				}

				if (!is_dir($path)) {
					Filesystem::mkdir($path);
				}

				if (!Mount::mount($path, 'cgroup',
					[
						'noexec'    => true,
						'nosuid'    => true,
						'nodev'     => true,
						'rw'        => true,
						$controller => true
					])) {
					warn("Failed to mount cgroup controller `%s'", $controller);

					return false;
				}
			}

			return true;
		}

		/**
		 * Unmount all cgroup controllers from shared slice
		 *
		 * @return bool
		 */
		public static function unmountAll(): bool
		{
			if (self::version() === 2) {
				return true;
			}

			if (null === static::controllerPathPattern()) {
				return true;
			}

			return true;
		}

		/**
		 * Get active controllers filtered, merging same-path controllers
		 *
		 * @return array
		 */
		public static function activeControllers(): array
		{
			if (self::version() === 2) {
				return explode(' ', rtrim(file_get_contents('/sys/fs/cgroup/cgroup.controllers')));
			}

			return array_filter(
				array_merge(array_diff(self::getControllers(), ['cpu', 'cpuacct']), ['cpu,cpuacct']),
				static function ($c) {
					return Mount::mounted(self::CGROUP_HOME . "/${c}");
				}
			);
		}

		/**
		 * Get glob-style pattern for all controllers
		 *
		 * @return string|null
		 */
		public static function controllerPathPattern(): ?string
		{
			if (!($controllers = static::activeControllers())) {
				return null;
			}

			if (\count($controllers) === 1) {
				return implode('', $controllers);
			}

			return '{' . implode(',', str_replace(',', '\\,', $controllers)) . '}';
		}

		public static function version(): ?int
		{
			static $version;
			if (isset($version)) {
				return $version;
			}

			if (!is_dir(self::CGROUP_HOME)) {
				return null;
			}

			$mount = Mount::getFilesystemType(self::CGROUP_HOME);
			if (null === $mount) {
				return null;
			}

			return $version = ($mount === 'cgroup2' ? 2 : 1);
		}
	}