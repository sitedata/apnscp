<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2019
	 */

	declare(strict_types=1);

	namespace Opcenter\System;

	use Opcenter\Dbus\Systemd;

	abstract class GenericSystemdService
	{
		protected const SERVICE = null;
		protected const STATUS_REGEX = null;

		public const SYSTEMD_SERVICE_PATH = '/etc/systemd/system';
		// @var array via systemctl list-units
		private const HEADER_FIELDS = [
			'unit',
			'load',
			'active',
			'sub',
			'description'
		];
		// @var bool process service request asynchronously
		public static $async = false;

		/**
		 * Verify this class has been properly extended
		 */
		private static function check(): void
		{
			if (null === static::SERVICE) {
				fatal('Incomplete implementation');
			}
		}

		/**
		 * Set async mode
		 *
		 * @return GenericSystemdService
		 */
		public function async(): self
		{
			static::$async = true;

			return $this;
		}

		/**
		 * Service has failed/exited abnormally
		 *
		 * @return bool
		 */
		public static function irregular(): bool
		{
			self::check();
			$status = static::state();

			return $status === 'failed' || $status === 'inactive' || $status === null /* DNE */;
		}

		/**
		 * Service has failed
		 *
		 * @return bool
		 */
		public static function failed(): bool
		{
			self::check();

			return static::state() === 'failed';
		}

		/**
		 * Reset service state
		 *
		 * @return bool
		 */
		public static function reset(): bool
		{
			self::check();
			if (!static::exists()) {
				return false;
			}

			(new Systemd)->exec(static::getService(), 'unit')->ResetFailed();
			return true;
		}

		/**
		 * Service is running
		 *
		 * @return bool
		 */
		public static function running(): bool
		{
			return static::state() === 'active';
		}

		/**
		 * Unit's activate state
		 *
		 * May be of form active, activating, failed, reloading, deactivating, or inactive
		 * @return string|null if not exists
		 */
		public static function state(): ?string
		{
			return (new Systemd)->read(static::getService(), 'ActiveState', 'unit');
		}

		/**
		 * Get full service status
		 *
		 * @param string|array $fields optional fields to filter
		 * @return mixed
		 */
		public static function status($fields = [])
		{
			return static::statusWrapper('Service', $fields);
		}

		/**
		 * Service unit information
		 *
		 * @param string|array $fields
		 * @return mixed
		 */
		public static function unitInfo($fields = [])
		{
			return static::statusWrapper('Unit', $fields);
		}

		/**
		 * @param string $object
		 * @param string|array $fields
		 * @return array|mixed|null
		 */
		private static function statusWrapper(string $object = 'Service', $fields = [])
		{
			self::check();
			if (!$fields || (is_array($fields) && \count($fields) > 3)) {
				// perf crossover
				$status = (new Systemd)->readAllProperties(static::getService(), $object);
				if ($fields) {
					$status = array_intersect_key($status, array_flip($fields));
				}

				return $status;
			}

			$vals = [];
			$systemd = (new Systemd);
			foreach ((array)$fields as $field) {
				$vals[$field] = $systemd->read(static::getService(), $field, $object);
			}

			return is_string($fields) ? array_pop($vals) : $vals;
		}

		/**
		 * Get service status from services implementing reporting
		 *
		 * @return array|null
		 */
		public static function getReportedServiceStatus(): ?array
		{
			if (!static::STATUS_REGEX) {
				fatal('Service %s claims reporting but fails to implement regex match', static::class);
			}
			$txt = (new Systemd)->read(static::getService(), 'StatusText') ?? '';
			$res = preg_match(static::STATUS_REGEX, $txt, $matches);

			if (!$res) {
				return [];
			}

			return array_filter($matches, '\is_string', ARRAY_FILTER_USE_KEY);
		}

		/**
		 * Service exists on machine
		 *
		 * @return bool
		 */
		public static function exists(): bool
		{
			self::check();
			return (new Systemd)->exists(static::getService());
		}

		/**
		 * Reload service
		 *
		 * @param string $timespec
		 * @return bool
		 */
		public static function reload(string $timespec = null): bool
		{
			if (static::irregular()) {
				debug("`%s' service marked as failed. Upgraded reload to restart request to clear state", static::getService());
				// @XXX check back later on service states
				return static::run('reload-or-restart', $timespec);
			}

			return static::run('reload', $timespec);
		}

		/**
		 * Restart the service
		 *
		 * @param string|null $timespec
		 * @return bool
		 */
		public static function restart(string $timespec = null): bool
		{
			return static::run('restart', $timespec);
		}

		/**
		 * Run systemd command
		 *
		 * @param string      $command  systemd command to run on service
		 * @param string|null $timespec optional asynchronous delay
		 * @return bool
		 */
		public static function run(string $command, string $timespec = null): bool
		{
			if ($timespec) {
				static::$async = false;
				$proc = new \Util_Process_Schedule($timespec);
				$proc->setID(static::getScheduleKey($command));
				if ($id = $proc->preempted()) {
					return true;
				}
			} else if ($command === 'start' || $command === 'stop' || $command === 'restart' || $command === 'try-restart' || $command === 'reload-or-restart' || $command === 'reload-or-try-restart')
			{
				try {
					$res = (new Systemd)->exec(static::getService(), 'Unit')->{studly_case($command)}('replace');
					if (static::$async) {
						static::$async = false;
						return null !== $res;
					}

					(new Systemd)->jobWait((int)basename($res));
					$running = static::running();
					return $command === 'stop' ? !$running : $running;

				} catch (\DbusExceptionUnknownMethod|\DbusException $e) {
					return error("%(service)s action %(command)s failed: %(msg)s", [
						'service' => static::getService(),
						'command' => $command,
						'msg'     => $e->getMessage()
					]);
				}
			} else if ($command === 'disable' || $command === 'enable') {
				static::$async = false;
				// @todo parse out --now
				try {
					$service = static::getService();
					if (!str_contains($service, '.')) {
						$service .= '.service';
					}
					$args = [
						new \DbusArray(\Dbus::STRING, [
							$service
						]),
						false, /* true = runtime, false = persistent */
					];
					if ($command === 'enable') {
						$args[] = true;
					}
					(new Systemd)->exec(null, 'Manager')->{studly_case($command) . 'UnitFiles'}(...$args);
					return true;
				} catch (\DbusExceptionUnknownMethod|\DbusException $e) {
					return error("%(service)s action %(command)s failed: %(msg)s", [
						'service' => static::getService(),
						'command' => $command,
						'msg'     => $e->getMessage()
					]);
				}
			} else {
				$proc = static::$async ? new \Util_Process_Fork : new \Util_Process;
			}
			$res = $proc->run(
				static::getCommand($command),
				[
					'mute_stdout' => true,
					'mute_stderr' => true,
					'reporterror' => true
				]

			);

			static::$async = false;
			return $res['success'] ?? false;
		}

		/**
		 * Get scheduling key
		 *
		 * @param string $suffix scheduling key marker
		 * @return string
		 */
		protected static function getScheduleKey(string $suffix = 'rld'): string
		{
			self::check();
			// merge events
			if ($suffix === 'reload' || $suffix === 'restart') {
				$suffix = 'rld';
			}
			return static::getService() . ':' . $suffix;
		}

		/**
		 * Translate action into systemd directive
		 *
		 * @param string $mode
		 * @return string
		 */
		protected static function getCommand(string $mode): ?string
		{
			self::check();
			$mode = strtok($mode, ' ');
			$args = strtok("\n");
			switch (strtolower($mode)) {
				case 'start':
				case 'stop':
				case 'reload':
				case 'restart':
				case 'try-restart':
				case 'reload-or-restart':
				case 'reload-or-try-restart':
				case 'isolate':
				case 'kill':
				case 'mask':
				case 'unmask':
				case 'disable':
				case 'enable':
				case 'reenable':
				case 'is-active':
				case 'is-failed':
				case 'status':
				case 'show':
					return "/usr/bin/systemctl $args $mode " . static::getService();
				default:
					fatal("Unknown %s command `%s'", static::getService(), $mode);
			}
			return null;
		}

		/**
		 * Reload systemd daemon
		 *
		 */
		public static function daemonReload(): void
		{
			(new Systemd)->reload();
		}

		/**
		 * Show all services
		 *
		 * @param string      $pattern service pattern to match
		 * @param string|null $state optional state
		 * @return array
		 */
		final public static function listAll(string $pattern = '*', string $state = null): array
		{
			$flag = null;
			if ($state) {
				if (!\in_array($state, ['failed', 'active', 'inactive', 'activating', 'deactivating'], true)) {
					error('Unknown/invalid state requested: %s', $state);
					return [];
				}
				$flag = '--state=';
			}

			$ret = \Util_Process_Safe::exec('systemctl list-units --full --no-legend --plain %s %s%s', [
				$pattern,
				$flag,
				$state
			]);
			$services = [];
			if (!$ret['success']) {
				error($ret['stderr']);
				return $services;
			}
			$lines = explode("\n", $ret['stdout']);
			$header = current($lines);
			$headerFields = static::HEADER_FIELDS;
			$headerLength = \count($headerFields);

			while (false !== ($line = next($lines))) {
				if (!$line) {
					continue;
				}
				$tokens = [];
				$token = strtok($line, ' ');
				for ($i = 1; $i < $headerLength; $i++, $token = strtok(' ')) {
					if (false === $token) {
						break;
					}
					$tokens[] = $token;
				}
				$tokens[] = strtok("\n");
				if (\count($tokens) !== \count($headerFields)) {
					debug('Garbage output detected: %s', $line);
					continue;
				}
				$services[] = array_combine(
					$headerFields,
					$tokens
				);
			}

			return $services;
		}

		public static function listAllNg(string $pattern = '*', string $state = null): array
		{
			// hold for now
			// https://github.com/derickr/pecl-dbus/issues/12
			return array_filter(array_map(static function ($unit) use ($pattern, $state) {
				if ($state && $unit[3] !== $state) {
					return null;
				}
				if ($pattern !== '*' && $pattern && !fnmatch($pattern, $unit[0])) {
					return null;
				}
				return array_combine(
					['unit', 'description', 'load', 'active', 'sub'],
					array_slice($unit, 0, 5)
				);
			}, (new Systemd)->exec(null, 'Manager')->ListUnits()));
		}

		/**
		 * Wait for service activation
		 *
		 * @param int $seconds maximum time
		 * @return bool
		 */
		public static function waitFor(int $seconds = 10): bool
		{
			$sleep = 500000;
			for ($i = 0, $n = ceil($seconds*1000000/$sleep); $i < $n; $i++) {
				if (self::running()) {
					return true;
				}
				usleep($sleep);
			}
			return false;
		}

		/**
		 * Get service name
		 *
		 * @return string
		 */
		protected static function getService(): string
		{
			static::check();

			return static::SERVICE;
		}

		/**
		 * Convert systemd path into service
		 */
		public static function escapePath(string $path): string {
			return str_replace(
				['.', '-'],
				['\\x' . bin2hex('.'), '\\x' . bin2hex('-')],
				$path
			);
		}

		/**
		 * Service is present based on configuration
		 *
		 * @link exists()
		 * Lenient form of exists(), checks by configuration not actual presence
		 *
		 * @return bool
		 */
		public static function present(): bool
		{
			return static::exists();
		}
	}