<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, June 2020
 */
class RecursiveObjectIterator extends RecursiveArrayIterator
{
	public function hasChildren()
	{
		return is_array($this->current());
	}

	public function getChildren()
	{
		return $this->current()->getIterator();
	}
}
