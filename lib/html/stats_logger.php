<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Logging facility handling function calls within esprit
	 * through the GUI
	 *
	 * afiProxy
	 */
	class Stats_Logger
	{
		// mysql instance
		private static $functions = array();

		public static function commit($is_pb = null)
		{
			$db = MySQL::initialize();

			// don't record dashboard functions, these are universal
			return true;
			$is_pb = count($_POST) > 0;
			$fns = self::function_list();
			$app_id = self::getID(\Page_Container::get()->getApplicationID());
			$db->query('INSERT INTO
                page_behavior
                (page_id, func_id, session_id, postback)
                VALUES
                (' . $app_id . ',
                ' . (!isset($fns[0]) ? 'NULL' : "'" . md5(serialize($fns)) . "'") . ",
                '" . session_id() . "',
                " . intval($is_pb) . ');');
			$id = $db->insert_id;
			if (isset($fns[0])) {
				$q = 'INSERT INTO page_functions (behavior_id, function, func_order) VALUES';
				$id = intval($id);
				for ($i = 0, $n = sizeof($fns); $i < $n; $i++) {
					$fns[$i] = $id . ',\'' . $fns[$i] . '\',' . $i;
				}
				$db->query($q . '(' . join('),(', $fns) . ')');
			}
		}

		public static function function_list()
		{
			return self::$functions;
		}

		public static function getID($page = null)
		{
			$db = MySQL::initialize();
			if (is_null($page)) {
				$page = \Page_Container::get()->getApplicationID();
			}
			$q = $db->query("SELECT page_id FROM pages WHERE page_name = '" . $page . "'");
			if ($q->num_rows > 0) {
				return $q->fetch_object()->page_id;
			}

			$q = $db->query("INSERT INTO pages (page_id, page_name)
            	VALUES (NULL, '" . $page . "');");

			return $db->insert_id;
		}

		public static function log_function($fn)
		{
			self::$functions[] = $fn;
		}


	}

	/**
	 * afi proxy handler to facilitate function logging
	 *
	 */
	class afiProxy extends apnscpFunctionInterceptor
	{
		public function __call($function, array $args = array())
		{
			Stats_Logger::log_function($function);

			return parent::__call($function, $args);
		}
	}