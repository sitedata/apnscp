<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2019
 */


if (!function_exists('get_app')) {
	function get_app()
	{
		if (!isset($_GET['app'])) {
			header('HTTP/1.1 403 Forbidden', true, 403);
			exit();
		}

		return $_GET['app'];
	}
}

// application handler
// expects in GET app=appname&fn=function
// optional in POST args
if (!function_exists('dispatch_app')) {
	function dispatch_app()
	{
		if (!isset($_GET['fn'])) {
			return false;
		}
		$app = get_app();
		if (!isset($_GET['fn'])) {
			header('HTTP/1.1 403 Forbidden', true, 403);
			exit();
		}
		$func = $_GET['fn'];
		$args = array();
		if (isset($_POST['args'])) {
			$args = (array)$_POST['args'];
		} else if (isset($_GET['args'])) {
			$args = (array)$_GET['args'];
		}
		unset($_GET['fn']);
		$path = \Page_Container::resolve($app);
		if (!$path) {
			return false;
		}
		include($path);
		include(INCLUDE_PATH . '/lib/html/page_renderer.php');
		include(INCLUDE_PATH . '/lib/html/js_template.php');
		include(INCLUDE_PATH . '/lib/html/stats_logger.php');

		$kernel = Page_Container::kernelFromApp($app);
		$Page = Page_Container::init($kernel);
		$response = call_user_func_array([$Page, $func], array_values($args));
		$success = !Error_Reporter::is_error() && !$response instanceof Exception;

		if (!$success) {
			header('HTTP/1.1 500 Internal Server Error', true, 500);

			// compatibility for use with apnscp.ajaxError()
			return [
				'success' => !Error_Reporter::is_error() && !$response instanceof Exception,
				'errors'  => Error_Reporter::flush_buffer(),
				'return'  => !$response instanceof Exception ? $response : $response->getMessage()
			];
		}

		return $response;
	}
}

if (!function_exists('postback_app')) {
	function postback_app($app, $params)
	{
		$resolved = \Page_Container::resolve($app);
		if (!$resolved) {
			return false;
		}
		include($resolved);

		$Page = \Page::headless();

		return $Page->on_postback($params);
	}
}

if (!function_exists('dispatch_ucard')) {
	function dispatch_ucard($action, $params = array())
	{
		$card = UCard::init();
		if ($action == 'refstats') {
			$card->flush();
			$stats = array(
				'storage'   => $card->getStorage(),
				'bandwidth' => $card->getBandwidth()
			);

			return $stats;
		}

		return false;
	}
}
