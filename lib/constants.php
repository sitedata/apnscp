<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	if (!defined('IS_SOAP')) {
		define('IS_SOAP', 0);
	}
	if (!defined('IS_ISAPI')) {
		define('IS_ISAPI', 0);
	}
	if (!defined('IS_DAV')) {
		define('IS_DAV', 0);
	}
	if (!defined('IS_CLI')) {
		define('IS_CLI', 0);
	}
	if (!defined('NO_AUTH')) {
		define('NO_AUTH', 0);
	}

	// 128 MB is too little
	$mem = ini_get('memory_limit');
	$unit = $mem[-1];
	$mem = (int)$mem;
	if ($unit === 'g' || $unit === 'G') {
		$mem = (int)$mem * 1024;
	}
	if ($mem < 256) {
		ini_set('memory_limit', '256M');
	}
	ini_set('mysqli.reconnect', 'on');

	// likely PHP 5.6

	$key = 'apnscp.c';
	define('CONFIGURATION_KEY', $key);

	/**
	 * Prevent access outside
	 *
	 * @return array
	 */
	$load_constants = static function () use (&$load_constants): array {

		/** host to be used for setting cookies */
		if (!file_exists(INCLUDE_PATH . '/lib/config.php')) {
			echo "FATAL: missing config.php in lib/\n";
			exit(1);
		}
		// NB: unset() retains the function reference, null assignment discards it
		$load_constants = null;
		$constants = include(INCLUDE_PATH . '/lib/config.php');
		// relocated to CFG_DEBUG
		$constants['DEBUG'] = getenv('DEBUG') ?: $constants['CFG_DEBUG'];
		$constants['DEBUG_BACKTRACE_QUALIFIER'] = getenv('VERBOSE') ?: $constants['CFG_VERBOSE'];
		return $constants;
	};

	if (IS_CLI) {
		$constants = $load_constants();
		// @todo breakout to top-level const $k = $v
		// only when overhead is considerable
		foreach ($constants as $k => $v) {
			// using a registry object is marginally faster
			// given the volume of constants
			define($k, $v);
		}
		// DEBUG is determined at runtime
		unset($constants['DEBUG'], $constants['DEBUG_BACKTRACE_QUALIFIER']);
		$cliMarkup = array_map(static function ($sig) {
			return "const SIG${sig} = " . constant("SIG${sig}") . ';';
		}, ['KILL', 'TERM', 'HUP', 'USR1', 'USR2']);

		file_put_contents(storage_path('constants.php'),
			'<?php ' . implode("\n", array_key_map(static function ($k, $v) {
				return "const ${k} = " . var_export($v, true) . ';';
			}, $constants)) . "\n" .
			'if (defined("SIGKILL")) { return; }' . implode("\n", $cliMarkup)
		);

		// Cache_Global is an instance of Cache_Mproxy without a prefix
		// use Cache_Mproxy directly, which does not rely upon constants
		// declared within
		if (!Cache_Mproxy::launch($constants['WS_USER'], $constants['CACHE_SOCKET_PERMS'])) {
			warn('Failed to launch backend cache! %s will be slow', MISC_PANEL_BRAND);
		}

		return;
	}

	if (file_exists($path = storage_path('constants.php'))) {
		include $path;
		define('DEBUG', (bool)(getenv('DEBUG', true) === false ? CFG_DEBUG : getenv('DEBUG', true)));
		define('DEBUG_BACKTRACE_QUALIFIER',
			(int)((false !== $ver = getenv('VERBOSE', true)) ? $ver : CFG_VERBOSE));
	} else {
		// shouldn't happen in production
		$constants = $load_constants();
		if (!$constants['DEBUG']) {
			echo('FATAL: configuration in inconsistent state - set debug=1 in config.ini then restart apnscpd');
			exit(1);
		}
		foreach ($constants as $k => $v) {
			// using a registry object is marginally faster
			// given the volume of constants
			define($k, $v);
		}
	}

	// sanity check
	if (!defined('FILESYSTEM_VIRTBASE')) {
		echo('FATAL: configuration is in inconsistent state - filesytem_virtbase not defined?');
		exit(1);
	}

	/** poof! */
	unset($load_constants);