<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, December 2019
 */

namespace Daphnie\Metrics;

use Daphnie\Contracts\MetricProvider;
use Daphnie\Metric;

class Anonymous extends Metric implements  MetricProvider
{
	private string $attr;
	private string $label;
	private string $type;

	public function __construct(string $attr, string $label, string $type = MetricProvider::TYPE_VALUE)
	{
		$this->attr = $attr;
		$this->label = $label;
		if ($type !== MetricProvider::TYPE_VALUE && $type !== MetricProvider::TYPE_MONOTONIC) {
			fatal("Unknown metric type `%s'", $type);
		}
		$this->type = $type;
	}

	public function metricAsAttribute(): string
	{
		return $this->attr;
	}

	public function getLabel(): string
	{
		return $this->label;
	}

	public function getType(): string
	{
		return $this->type;
	}


}