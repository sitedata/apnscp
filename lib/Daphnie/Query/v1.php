<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2021
 */

namespace Daphnie\Query;

class v1 extends Generic
{
	public function getJobs(): string
	{
		return "SELECT * FROM timescaledb_information.policy_stats 
			where hypertable::varchar = 'metrics'";
	}

	public function hasCompression(): string
	{
		return "SELECT * FROM timescaledb_information.compressed_hypertable_stats WHERE hypertable_name::varchar = 'metrics'";
	}

	public function databaseUsage(): string
	{
		return "SELECT ht.id,
				ht.schema_name AS table_schema,
				ht.table_name,
				t.tableowner AS table_owner,
				ht.num_dimensions,
				( SELECT count(1) AS count
					   FROM _timescaledb_catalog.chunk ch
					  WHERE ch.hypertable_id = ht.id) AS num_chunks,
				bsize.table_bytes,
				bsize.index_bytes,
				bsize.toast_bytes,
				bsize.total_bytes
			   FROM _timescaledb_catalog.hypertable ht
				 LEFT JOIN pg_tables t ON ht.table_name = t.tablename AND ht.schema_name = t.schemaname
				 LEFT JOIN LATERAL hypertable_relation_size(
					CASE
						WHEN has_schema_privilege(ht.schema_name::text, 'USAGE'::text) THEN format('%I.%I'::text, ht.schema_name, ht.table_name)
						ELSE NULL::text
					END::regclass) bsize(table_bytes, index_bytes, toast_bytes, total_bytes) ON true
			WHERE table_name = 'metrics'";
	}

	public function disableArchivalCompression(): string
	{
		return 'ALTER TABLE metrics set (timescaledb.compress=false);';
	}

	public function getChunkCompressionStats(string $chunk): string
	{
		$safe = $this->db->quote($chunk);

		return 'SELECT * FROM timescaledb_information.compressed_chunk_stats ' .
			'WHERE chunk_name::varchar = ' . $safe;
	}

	public function getCompressionStats(): string
	{
		return "SELECT * FROM timescaledb_information.compressed_hypertable_stats WHERE hypertable_name::varchar = 'metrics'";
	}

	public function isCompressed(string $chunk): string
	{
		return 'SELECT 1 FROM timescaledb_information.compressed_chunk_stats ' .
			'WHERE chunk_name::varchar = ' . $this->db->quote($chunk) .
			' AND compression_status::varchar = \'Compressed\'';
	}

	public function getChunkStats(string $chunk = null): string
	{
		$query = "SELECT * FROM chunk_relation_size('metrics')";
		if ($chunk) {
			$query .= " WHERE chunk_table = " . $this->db->quote($chunk);
		}

		return $query;
	}

	public function refreshContinuousAggregate(string $view): string
	{
		return $this->refreshMaterializedView($view);
	}

	/**
	 * Discard data
	 *
	 * Note: $after is used to clear accounting irregularities with clocks
	 *
	 * @param string|null $olderInt oldest interval to preserve
	 * @param string|null $newerInt newest future-dated interval to discard (NOW() + $after)
	 * @return bool
	 */
	public function removeData(?string $olderInt, string $newerInt = null): string
	{
		$window = ['cascade_to_materializations' => 'true'];

		$olderThan = $olderInt ? 'interval ' . $this->db->quote($olderInt) : 'NOW()';

		if ($newerInt) {
			$window['newer_than'] = 'NOW() + interval ' . $this->db->quote($newerInt);
		}

		$args = implode(',', array_key_map(static function ($k, $v) {
			return $k . ' => ' . $v;
		}, $window));

		return "SELECT drop_chunks(" . $olderThan . ", 'metrics', " . $args . ')';
	}
}