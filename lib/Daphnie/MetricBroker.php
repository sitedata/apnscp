<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, November 2019
 */

namespace Daphnie;

	use Daphnie\Contracts\MetricProvider;
	use Daphnie\Metrics\Anonymous;
	use Daphnie\Metrics\Apache;
	use Daphnie\Metrics\Memory;
	use Daphnie\Metrics\Mysql;
	use Daphnie\Metrics\Php;
	use Daphnie\Metrics\Rampart;

	/**
	 * Metrics interactions for DAPHNIE:
	 *
	 * Distributed Analytics and Predictive Hot, Naive Isostatic Economizer
	 */
	class MetricBroker
	{
		/**
		 * @var array preloaded metrics
		 */
		private static array $preloadedGroups = [
			// doubles as metric prefix
			'apache'  => Apache::class,
			'memory'  => Memory::class,
			'mysql'   => Mysql::class,
			'php'     => Php::class,
			'rampart' => Rampart::class
		];

		/**
		 * @var array cached metric maps
		 */
		private static array $metrics = [

		];


		/**
		 * Get preloaded metrics
		 *
		 * Invoked by Collector bootstrap
		 *
		 * @return array
		 */
		public static function getPreload(): array
		{
			return static::$preloadedGroups;
		}

		/**
		 * Lookup metric from registered metrics
		 *
		 * @param string $name
		 * @return null|Metric
		 */
		public static function resolve(int|string $name): ?Metric
		{
			if (is_int($name)) {
				$id = $name;
				$db = \PostgreSQL::pdo();
				$rs = $db->query("SELECT name FROM metric_attributes WHERE attr_id = " . $name);
				if ($rs->rowCount()) {
					$name = $rs->fetch(\PDO::FETCH_NUM)[0];
					static::$metrics[$id] = &static::$metrics[$name];
				}
			}

			if (isset(static::$metrics[$name])) {
				if (\is_string($class = static::$metrics[$name])) {
					static::$metrics[$name] = new $class;
				}
				return static::$metrics[$name];
			} else if (false !== ($pos = strpos($name, '-'))) {
				$group = substr($name, 0, $pos);
				if (isset(static::$preloadedGroups[$group])) {
					// group hasn't been registered yet
					static::register(static::$preloadedGroups[$group], $group);
					return static::$metrics[$name] ?? null;
				}
			}

			// @TODO ugly location. Created for cgroup stat registration, which could still fail
			// if the first stat in a set exceeds field limit
			$db = \PostgreSQL::pdo();
			$rs = $db->query("SELECT label, type FROM metric_attributes WHERE name = " . $db->quote($name));
			if ($rs->rowCount()) {
				return static::$metrics[$name] = (new Anonymous($name, ...$rs->fetch(\PDO::FETCH_NUM)));
			}

			return null;
		}

		/**
		 * Register metric
		 *
		 * Metrics may be registered at boot or as encountered
		 * MetricBroker::register('apache', ApacheMetrics::class)
		 *
		 * @param string|MetricProvider $class
		 * @param string $name
		 */
		public static function register(string|MetricProvider $class, string $name = null): void
		{
			if (\is_string($class)) {
				$class = new $class;
			}

			if (null === $name) {
				$name = $class->metricAsAttribute();
			}

			if (isset(static::$metrics[$name])) {
				debug("Metric `%s' previously registered - ignoring request", $name);
				return;
			}

			if (!$class->isGroup()) {
				static::$metrics[$name] = $class;
			}

			foreach ($class->getGroup() as $metric) {
				$key = "${name}-${metric}";
				if (isset(static::$metrics[$key])) {
					debug("Metric `%s' previously registered - ignoring request", $key);
					continue;
				}
				// @todo weak ref
				static::$metrics[$key] = $class->mutate($metric);
			}
		}
	}