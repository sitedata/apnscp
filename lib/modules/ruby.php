<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	/**
	 * Ruby, RoR features formerly under Web_Module
	 *
	 * @package core
	 */
	class Ruby_Module extends \Module\Support\Anyversion
	{
		const LTS = '2.7.6';

		public function __construct()
		{
			$this->exportedFunctions += [
				'update_remote' => PRIVILEGE_ADMIN
			];

			parent::__construct();
		}

		/**
		 * Execute Ruby within the scope of a version
		 *
		 * @param null|string $version
		 * @param null|string $pwd pwd, defaults to ~
		 * @param string      $command
		 * @param array       $args optional command arguments
		 * @return array process output from pman_run
		 */
		public function do(?string $version, ?string $pwd, string $command, ...$args): array
		{
			if ($version === 'lts') {
				$version = $this->get_lts();
			}

			return parent::do($version, $pwd, $command, ...$args);
		}

		/**
		 * Get configured Ruby LTS
		 *
		 * @return string
		 */
		protected function get_lts(): string
		{
			$prefs = \Preferences::factory($this->getAuthContext());

			return array_get($prefs, 'ruby.lts', static::LTS);
		}

		/**
		 * Update known Ruby versions
		 *
		 * @return bool
		 */
		public function update_remote(): bool
		{
			if (!IS_CLI) {
				return $this->query('ruby_update_remote');
			}
			if (!$root = $this->environmentRoot()) {
				return error("%(what)s is not available on this system", ['what' => 'rbenv']);
			}

			// @TODO move to private repo lest we have an "event-stream" debacle
			$builddir = $root. '/plugins/ruby-build';
			$ret = \Util_Process_Safe::exec('cd %(builddir)s && git pull', ['builddir' => $builddir]);
			if (!$ret['success']) {
				return error(coalesce($ret['stderr'], $ret['stdout']));
			}
			(new \Opcenter\Service\ServiceLayer(null))->dropVirtualCache();

			return true;
		}

		/**
		 * Remove an installed Ruby
		 *
		 * @param string $version
		 * @return bool
		 */
		public function uninstall(string $version): bool
		{
			if ($version === 'lts') {
				$version = $this->get_lts();
			}

			return parent::uninstall($version);
		}

		/**
		 * Assign Ruby version to directory
		 *
		 * @param string $version
		 * @param string $path
		 * @return bool
		 */
		public function make_default(string $version, string $path = '~'): bool
		{
			if ($version === 'lts') {
				$version = $this->get_lts();
			}

			return parent::make_default($version, $path);
		}

		/**
		 * Install Ruby
		 *
		 * @param string $version
		 * @return null|string version installed, null on failure
		 */
		public function install(string $version): ?string
		{
			if ($version === 'lts') {
				$version = $this->get_lts();
			}

			return parent::install($version);
		}

		/**
		 * Ruby version is installed
		 *
		 * @param string $version
		 * @return string|null installed ver
		 */
		public function installed(string $version, string $comparator = '='): ?string
		{
			if ($version === 'lts') {
				return $this->lts_installed() ? $this->get_lts() : null;
			}

			return parent::installed($version, $comparator);
		}

		/**
		 * Latest LTS is installed
		 *
		 * @return bool
		 */
		public function lts_installed(): bool
		{
			$versions = $this->list();

			return \in_array($this->get_lts(), $versions, true);
		}

		/**
		 * Set LTS for account
		 *
		 * @param string $version
		 */
		protected function set_lts(string $version): void
		{
			$prefs = \Preferences::factory($this->getAuthContext());
			$prefs->unlock($this->getApnscpFunctionInterceptor());
			array_set($prefs, 'ruby.lts', $version);
		}
	}