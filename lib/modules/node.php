<?php
	declare(strict_types=1);
	/**
	 *  +------------------------------------------------------------+
	 *  | apnscp                                                     |
	 *  +------------------------------------------------------------+
	 *  | Copyright (c) Apis Networks                                |
	 *  +------------------------------------------------------------+
	 *  | Licensed under Artistic License 2.0                        |
	 *  +------------------------------------------------------------+
	 *  | Author: Matt Saladna (msaladna@apisnetworks.com)           |
	 *  +------------------------------------------------------------+
	 */

	use Opcenter\Versioning;

	/**
	 * Manage and install Node versions
	 *
	 * @package core
	 */
	class Node_Module extends \Module\Support\Anyversion
	{
		const NVM_LOCATION = FILESYSTEM_SHARED . '/node/nvm/nvm-exec';

		/**
		 * Execute Node within the scope of a version
		 *
		 * @param null|string $version
		 * @param string      $command
		 * @param array       $args optional command arguments
		 * @return array process output from pman_run
		 */
		public function do(...$args): array
		{
			$argc = count($args);
			if ($argc === 2) {
				// old format
				warn("API change: node_do uses new signature");
				[$version, $command] = $args;
				$pwd = null;
				$args = $env = [];
			} else {
				if ($argc < 5) {
					$args += array_fill($argc, 5-$argc, []);
				}
				[$version, $pwd, $command, $args, $env] = $args;
			}

			if ($version === 'lts') {
				$version = '--lts';
			} else if ($version) {
				$version = escapeshellarg($version);
			}

			return $this->exec($pwd, "exec --silent {$version} -- {$command}", $args, $env);
		}

		protected function environmentRoot(bool $reset = false): ?string
		{
			return FILESYSTEM_SHARED . '/node/nvm';
		}

		/**
		 * nvm wrapper
		 *
		 * @param null|string $name
		 * @param null|string $command
		 * @param array       $args optional args
		 * @return array
		 */
		protected function exec(?string $pwd, string $command, array $args = [], array $env = []): array
		{
			$env['PATH'] = '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin' . PATH_SEPARATOR . '~/node_modules/.bin';

			return parent::exec($pwd, $command, $args, $env);
		}

		/**
		 * Remove an installed Node
		 *
		 * @param string $version
		 * @return bool
		 */
		public function uninstall(string $version): bool
		{
			if ($version === 'lts') {
				$version = '--lts';
			}
			$ret = $this->exec(null, 'uninstall %s', [$version]);
			if (!$ret['success']) {
				return error('failed to uninstall Node %s: %s',
					$version,
					coalesce($ret['stderr'], $ret['stdout'])
				);
			}

			return true;
		}

		/**
		 * Assign Node version to directory
		 *
		 * Resolves symbolic names to version number
		 *
		 * @param string $version
		 * @param string $path
		 * @return bool
		 */
		public function make_default(string $version, string $path = '~'): bool
		{
			$path .= '/.nvmrc';
			if ($version === 'lts') {
				$version = 'lts/*';
			}

			return $this->file_put_file_contents($path, $this->resolveVersion($version), true);
		}

		/**
		 * Get Node version for a path
		 *
		 * @param string $path
		 * @return string|null
		 */
		public function version_from_path(string $path): string
		{
			$path .= '/.nvmrc';
			if (!$this->file_exists($path)) {
				return 'system';
			}

			return $this->file_get_file_contents($path);
		}

		public function get_default(string $path): ?string
		{
			deprecated_func('Use version_from_path');
			return $this->version_from_path($path);
		}

		/**
		 * Resolve Node alias to version number
		 *
		 * @param string $version
		 * @return null|string
		 */
		protected function resolveVersion(string $version): ?string
		{
			if ($version === 'lts') {
				$version = 'lts/*';
			}
			$ret = $this->exec(null, 'ls %s', [$version]);
			if ($ret['success']) {
				$resolvedVersion = trim(preg_replace('/^\S+\s+|\bv(?=\d)|\s+\*$/', '', $ret['output']));
				// allow setting "12" if 12.15.1 exists
				if (0 === strpos($resolvedVersion, $version)) {
					return $version;
				}

				return $resolvedVersion;
			}

			return null;
		}

		/**
		 * Get installed LTS version for account
		 *
		 * @param string $alias Node release alias (argon, boron, carbon, dubnium, etc)
		 * @return null|string
		 */
		public function lts_version(string $alias = '*'): ?string
		{
			return $this->resolveVersion('lts/' . $alias);
		}

		/**
		 * Install Node
		 *
		 * @param string $version
		 * @return null|string null on error, specific version installed
		 */
		public function install(string $version): ?string
		{
			if ($version === 'lts') {
				$version = '--lts';
			}

			$ret = $this->exec(null, 'install %s', [$version]);
			if (!$ret['success']) {
				return nerror('failed to install Node %s, error: %s',
					$version,
					coalesce($ret['stderr'], $ret['stdout'])
				);
			}

			$resolved = $this->exec(null, 'version %s', [$version]);

			return $resolved['stdout'][0] === 'v' ? substr($resolved['stdout'], 1) : $resolved['stdout'];
		}

		/**
		 * Node version is installed
		 *
		 * @param string $version
		 * @return string|null version satisfied
		 */
		public function installed(string $version, string $comparator = '='): ?string
		{
			if ($version === 'lts') {
				return $this->lts_installed() ? $this->lts_version() : null;
			}
			$nodes = $this->list();

			foreach ($nodes as $alias => $localVersion) {
				if (false !== ($p1 = strpos($localVersion, '.')) &&
					strrpos($localVersion, '.') !== $p1 &&
					Versioning::compare($localVersion, $version, $comparator))
				{
					return $localVersion;
				}
			}
			return null;
		}

		/**
		 * Latest LTS is installed
		 *
		 * @return bool
		 */
		public function lts_installed(): bool
		{
			$versions = $this->list();
			$lts = $versions['lts/*'] ?? null;

			return array_has($versions, $lts);
		}

		/**
		 * List installed Nodes
		 *
		 * @return array
		 */
		public function list(): array
		{
			// 3 = no nodes installed
			$ret = $this->exec(null, 'ls');
			if (!$ret['success']) {
				if ($ret['return'] !== 3) {
					error('failed to query nodes - is nvm installed? error: %s', $ret['error']);
				}

				return [];
			}
			if (preg_match_all(\Regex::NVM_NODES, $ret['output'], $versions, PREG_SET_ORDER)) {
				$nodes = array_combine(array_column($versions, 'alias'), array_column($versions, 'version'));
				if (isset($nodes['->'])) {
					$nodes['active'] = $nodes['->'];
					unset($nodes['->']);
				}

				return $nodes;
			}

			return [];
		}

		public function get_available(): array
		{
			$cache = \Cache_Super_Global::spawn();
			$key = 'node.rem';
			if (false !== ($res = $cache->get($key))) {
				return $res;
			}
			$ret = $this->exec(null, 'ls-remote');
			if (!$ret['success']) {
				error('failed to query remote Node versions: %s', coalesce($ret['stderr'], $ret['stdout']));

				return [];
			}

			if (!preg_match_all('/\s*v(?<version>\S*)\s*(?:\((?:Latest )?LTS: (\S*)\))?/', $ret['stdout'], $versions,
				PREG_SET_ORDER)) {
				warn('failed to discover any Nodes');

				return [];
			}
			$versions = array_column($versions, 'version');
			$cache->set($key, $versions);

			return $versions;
		}
	}