<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Provides rar compression/decompression routines in the file manager
	 *
	 * @package Compression
	 */
	class Rar_Filter extends Archive_Base
	{
		protected static $binary_handler = [
			'read' => 'rar', 'write' => 'rar'
		];
		protected static $mapping_list = [
			'list'        => 'lt',
			'extract_all' => 'x'
		];

		public static function extract_files($mArchive, $mDestination, $mFile = null)
		{
			//print ($mArchive. ' '.$mDestination);
			$proc = self::exec(
				static::$binary_handler['write'] . ' '
				. static::$mapping_list['extract_all'] . ' ' . $mArchive
				. ' ' . $mDestination
			);

			return $proc;
		}

		public static function list_files($mArchive)
		{
			$proc = self::exec(
				static::$binary_handler['read'] . ' '
				. static::$mapping_list['list'] . ' ' . $mArchive
			);
			$proc = explode("\n", trim($proc['output']));
			$files = array();
			foreach ($proc as $line) {

				if (!preg_match('!(.+?)\s+(\d+)\s+(\d+)\s+\d+%\s+([0-9\-]+ \d\d?:\d\d)\s+([A-Z\.]+)\s+([A-F0-9]+)!',
					$line, $matches)
				) {
					continue;
				}
				list ($null, $file_name, $size, $packed_size, $date, $attr, $crc) = $matches;
				$files[] = array(
					'file_name'   => $file_name,
					'file_type'   => $file_name[strlen($file_name) - 1] == '/' ? 'dir' : 'file',
					'can_read'    => true,
					'can_write'   => true,
					'can_execute' => true,
					'size'        => $size,
					'packed_size' => $packed_size,
					'crc'         => $crc,
					'link'        => 0,
					'date'        => strtotime($date)
				);
			}

			return $files;

		}
	}

?>
