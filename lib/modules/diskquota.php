<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, July 2017
	 */

	class Diskquota_Module extends Module_Skeleton implements \Module\Skeleton\Contracts\Hookable
	{
		const DEPENDENCY_MAP = [
			'siteinfo',
		];

		const MIN_STORAGE_AMNESTY = QUOTA_STORAGE_WAIT;
		// time in seconds between amnesty requests
		const AMNESTY_DURATION = QUOTA_STORAGE_DURATION;
		// 24 hours
		const AMNESTY_MULTIPLIER = QUOTA_STORAGE_BOOST;
		const AMNESTY_JOB_MARKER = 'amnesty';

		public $exportedFunctions = [
			'*' => PRIVILEGE_SITE,
			'amnesty' => PRIVILEGE_SITE | PRIVILEGE_ADMIN
		];

		/**
		 * Request a temporary bump to account storage
		 *
		 * @return bool
		 * @see MIN_STORAGE_AMNESTY
		 */
		public function amnesty(string $site = null): bool
		{
			if (posix_getuid() && !IS_CLI) {
				return $this->query('diskquota_amnesty');
			}

			$last = $this->getServiceValue('diskquota', 'amnesty');
			$now = coalesce($_SERVER['REQUEST_TIME'], time());
			if (!$site) {
				$site = $this->site;
			} else if ($site && !($this->permission_level && PRIVILEGE_SITE)) {
				if (!($site = \Auth::get_site_id_from_anything($site))) {
					return error("Unknown site identifier `%s'", $site);
				}

				$site = "site" . $site;
			}

			if (($this->permission_level & PRIVILEGE_SITE) && self::MIN_STORAGE_AMNESTY > ($now - $last)) {
				$aday = self::MIN_STORAGE_AMNESTY / 86400;

				return error('storage amnesty may be requested once every %(period)d days, %(remaining)d days remaining', [
					'period'    => $aday,
					'remaining' => $aday - ceil(($now - $last) / 86400)
				]);
			}

			$ctx = \Auth::context(null, $site);
			$storage = $ctx->getAccount()->conf['diskquota']['quota'];
			$newstorage = $storage * self::AMNESTY_MULTIPLIER;
			$acct = new Util_Account_Editor($ctx->getAccount(), $ctx);
			$acct->setConfig('diskquota', 'quota', $newstorage)->setConfig('diskquota', 'amnesty', $now);
			$ret = $acct->edit();
			if ($ret !== true) {
				Error_Reporter::report(var_export($ret, true));
				return error('failed to set amnesty on account');
			}
			$acct->setConfig('diskquota', 'quota', $storage);
			$cmd = $acct->getCommand();
			$proc = new Util_Process_Schedule('+' . self::AMNESTY_DURATION . ' seconds');
			if (($this->permission_level & PRIVILEGE_ADMIN)) {
				if ($id = $proc->preempted(self::AMNESTY_JOB_MARKER, $ctx)) {
					// @xxx potential unbounded storage growth
					$proc->cancelJob($id);
					// @todo report duplicate request
				}

			}
			$proc->setID(self::AMNESTY_JOB_MARKER, $ctx);
			$ret = $proc->run($cmd);

			// @todo extract to event
			$msg = sprintf("Domain: %s\r\nSite: %d\r\nServer: %s", $this->domain, $this->site_id, SERVER_NAME_SHORT);
			Mail::send(Crm_Module::COPY_ADMIN, 'Amnesty Request', $msg);

			return $ret['success'];
		}

		/**
		 * Account is under amnesty
		 *
		 * @return bool
		 */
		public function amnesty_active(): bool
		{
			$time = $_SERVER['REQUEST_TIME'] ?? time();
			$amnesty = $this->getServiceValue('diskquota', 'amnesty');
			if (!$amnesty) {
				return false;
			}

			return ($time - $amnesty) <= self::AMNESTY_DURATION;
		}

		public function _edit()
		{
			// TODO: Implement _edit() method.
		}

		public function _delete()
		{
			// TODO: Implement _delete() method.
		}

		public function _edit_user(string $userold, string $usernew, array $oldpwd)
		{
			// TODO: Implement _edit_user() method.
		}

		public function _verify_conf(\Opcenter\Service\ConfigurationContext $ctx): bool
		{
			return true;
		}

		public function _create()
		{
			// TODO: Implement _create() method.
		}

		public function _create_user(string $user)
		{
			// TODO: Implement _create_user() method.
		}

		public function _delete_user(string $user)
		{
			// TODO: Implement _delete_user() method.
		}
	}