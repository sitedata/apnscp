<?php declare(strict_types=1);
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, January 2022
 */

namespace CLI;

use Illuminate\Contracts\Support\Arrayable;
use Opcenter\CliParser;
use Opcenter\Process;
use Opcenter\System\Sysctl;
use Symfony\Component\Yaml\Yaml;

class Output
{
	public const STDIN = 0;
	public const STDOUT = 1;
	public const STDERR = 2;
	protected const FORMATS = [
		'auto',
		'json',
		'yaml',
		'print',
		'cli',
		'serialize',
		'var_dump',
		'line'
	];

	protected array $log = [];
	protected int $fd = self::STDOUT;
	protected string $format = 'print';

	public function __construct(string $format = 'auto') {
		$this->setFormat($format);
	}

	/**
	 * Write to file descriptor
	 *
	 * @param int $fd
	 * @return $this
	 */
	public function setDescriptor(int $fd): self
	{
		if ($fd < 1 || $fd >= PHP_FD_SETSIZE) {
			fatal("Invalid descriptor %d", $fd);
		}

		if (!($fp = @fopen("php://fd/$fd", 'wb'))) {
			fatal("Descriptor %d is not writeable", $fd);
		}
		fclose($fp);
		$this->fd = $fd;

		return $this;
	}

	public function writeable(): bool
	{
		$fp = @fopen("php://fd/" . $this->fd, 'c');
		if ($fp) {
			fclose($fp);
		}

		return (bool)$fp;
	}

	/**
	 * Write line or current log to file
	 * @param string $path
	 * @param string|array $lines
	 * @return $this
	 */
	public function writeFile(string $path, $lines = null): self
	{
		if (file_exists($path) && !is_writable($path) || !file_exists($path) && !is_writable(dirname($path))) {
			fatal("Cannot write to %s", $path);
		}

		$fp = fopen($path, 'wb');
		$this->fd = $this->fdFromFile($path);
		$this->echo((array)($lines ?? $this->log));
		fclose($fp);
		return $this;
	}

	/**
	 * Discover FD from file
	 * @param string $file
	 * @return int
	 */
	private function fdFromFile(string $file): int
	{
		$fd = Process::descriptors(getmypid(), static function ($fd, $local) use ($file) {
			return $file === $local;
		});
		if (empty($fd)) {
			fatal("Requested file `%s' missing in descriptor table", $file);
		}

		return key($fd);
	}

	/**
	 * Set output format
	 * @param string $type
	 * @return $this
	 */
	public function setFormat(string $type): self
	{
		if (!in_array($type, static::FORMATS, true)) {
			fatal("Unknown output format `%s'", $type);
		}
		$this->format = $type;

		return $this;
	}

	/**
	 * Write log to file descriptor
	 *
	 * @param $output
	 * @return $this
	 * @throws \JsonException
	 */
	public function echo($output): self
	{
		$rendered = $this->renderFormat($output);
		file_put_contents("php://fd/{$this->fd}", $rendered);

		return $this;
	}

	public function __toString()
	{
		return $this->renderFormat($this->log);
	}

	/**
	 * Read Error_Reporter buffer
	 *
	 * @param array $log
	 * @return $this
	 */
	public function fromLog(array $log): self
	{
		foreach ($log as $item) {
			$str = \ArgumentFormatter::format('(%s) %s', [
				strtoupper(\Error_Reporter::errno2str($item['severity'])),
				$item['message']
			]);
			if (is_debug()) {
				$str .= "\n\t" . str_replace("\n", "\n\t", (string)$item['bt']);
			}
			$this->log[] = $str;
		}

		return $this;
	}

	private function renderFormat($input): string
	{
		if (\is_array($input)) {
			$input = array_map(static function ($e) {
				if (\is_object($e) && $e instanceof Arrayable) {
					return $e->toArray();
				}

				return $e;
			}, $input);
		}

		switch ($this->format) {
			case 'json':
				return json_encode($input, JSON_OBJECT_AS_ARRAY|JSON_THROW_ON_ERROR);
			case 'serialize':
				return serialize($input);
			case 'yaml':
				return Yaml::dump($input, (int)(getenv('YAML_INLINE') ?: 2), 2,
					Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK | Yaml::DUMP_OBJECT_AS_MAP | Yaml::DUMP_OBJECT);
			case 'var_dump':
				if ($this->fd !== Output::STDOUT) {
					fatal('var_dump may only report on STDOUT');
				}
				var_dump($input);
				return '';
			case 'cli':
				return CliParser::collapse($input);
			case 'line':
				return implode(PHP_EOL, (array)$input);
			case 'print':
				return is_array($input) ? print_r($input, true): (string)$input;
			case 'auto':
				$format = is_array($input) ? 'yaml' : 'print';
				$this->as($format)->echo($input);
				return '';
			default:
				fatal('???');
		}
	}

	public function as(string $format): self
	{
		$class = clone $this;

		return $class->fromLog($this->log)->setFormat($format);
	}
}