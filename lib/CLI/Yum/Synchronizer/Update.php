<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace CLI\Yum\Synchronizer;

	use CLI\Yum\Synchronizer;
	use CLI\Yum\Synchronizer\Plugins\Manager;

	/**
	 * Class CLI_Yum_Synchronizer_Update
	 *
	 * Update filesystem template with local copy
	 */
	class Update extends Synchronizer
	{

		private bool $dependencies = false;

		public function __construct(...$args)
		{
			foreach (\func_get_args() as $a) {
				if ($a[0] === '-') {
					switch ($a) {
						case '--soft':
						case '--update':
							break;
						case '-d':
							$this->dependencies = true;
							break;
						default:
							fatal("Unrecognized flag `%s'", $a);
					}
					array_shift($args);
				}
			}

			parent::__construct(...$args);
		}

		public function run()
		{
			$pkg = $this->package;
			if (!Utils::packageInstalled($pkg)) {
				if (Manager::hasPlugin($pkg, 'trigger')) {
					// separate plugin exists, run it
					Manager::run($pkg, 'update');
				}
				return true;
			}
			$service = Utils::getServiceFromPackage($pkg);
			$svcpath = Utils::getServicePath($service);
			$meta = Utils::getMetaFromRPM($pkg);
			$this->version = $meta['version'];
			$this->release = $meta['release'];
			$files = Utils::getFilesFromRPM($pkg);
			$oldfiles = $this->getFilesFromCache($pkg);
			// go in reverse as RPM lists files as tree: dir, dir -> file
			for ($i = \count($oldfiles) - 1; $i >= 0; $i--) {
				$file = $oldfiles[$i];
				$this->removeFile($file, $svcpath);
			}

			foreach ($files as $file) {
				$this->installFile($file, $svcpath);
			}

			if ($this->dependencies) {
				DependencyMap::satisfyRequirements($pkg);
			}
			return $this->postUpdate($pkg, $this->version, $this->release);
		}

		protected function postUpdate($package, $version, $rel = '')
		{
			$db = \PostgreSQL::initialize();
			$q = "UPDATE site_packages SET deleted = 'f', version = '" .
				pg_escape_string($db->getHandler(), $version) . "', release = '" . pg_escape_string($db->getHandler(),
					$rel) . "' WHERE package_name = '" . pg_escape_string($db->getHandler(), $package) . "'";
			$db->query($q);
			SynchronizerCache::get()->addPackage(
				(new Synchronizer\CacheManager\Package(
					$package,
					$this->version,
					$this->release
				))->addFiles(Utils::getFilesFromRPM($package))
			);
			Synchronizer\Plugins\Manager::run($package, 'update');

			return $db->affected_rows() > 0;
		}
	}