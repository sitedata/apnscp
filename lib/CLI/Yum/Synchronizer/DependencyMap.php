<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2018
	 */

	namespace CLI\Yum\Synchronizer;

	class DependencyMap
	{
		protected string $package;
		/**
		 * @var array packages dependeded upon presently installed
		 */
		protected $has = [];
		/**
		 * @var array packages needed to satisfy requirements
		 */
		protected $wants = [];

		public function __construct(string $package)
		{
			$this->package = $package;
		}

		/**
		 * Resolve entire chain of dependencies
		 *
		 * @param array $seen
		 * @param int   $depth explore at most n levels, $depth = 1 analogous to resolve()
		 * @return array
		 */
		public function resolveRecursively(array &$seen = [], $depth = 9999): array
		{
			if (!$depth || !$this->resolve()) {
				return [];
			}

			foreach ($this->getWants() as $want) {
				if (isset($seen[$want])) {
					continue;
				}
				$seen[$want] = 1;
				$map = new DependencyMap($want);
				$map->resolveRecursively($seen, $depth - 1);
				if ($map->satisfied()) {
					continue;
				}
			}

			$this->wants = $seen;

			return $seen;
		}

		/**
		 * Resolve package dependencies
		 *
		 * @return bool
		 */
		public function resolve(): bool
		{
			// @XXX potentially bomb with mixed arch?
			$pkgs = \Util_Process_Safe::exec(['rpm', '-qR', '%s'], $this->package);
			if (!$pkgs['success']) {
				return error("Failed to query package `%s': %s", $this->package, $pkgs['stderr']);
			}
			$deps = [];

			foreach (explode("\n", rtrim($pkgs['stdout'])) as $dep) {
				if (0 === strncmp($dep, 'rpmlib(', 7)) {
					// rpmlib(FEATURE)
					continue;
				}
				$dep = strtok($dep, ' ');
				$ret = \Util_Process_Safe::exec(['rpm', '-q', '--whatprovides', '%(dep)s', '--queryformat=%%{NAME}\n'],
					['dep' => $dep]);
				if (!$ret['success']) {
					warn("Unable to locate dependency `%s'", $dep);
					continue;
				}
				$deppkg = strtok($ret['stdout'], "\n");
				if (isset($deps[$deppkg])) {
					continue;
				}
				$deps[$deppkg] = true;
				if (Utils::packageInstalled($deppkg)) {
					$this->has[$deppkg] = Utils::getServiceFromPackage($deppkg);
				} else {
					$this->wants[$deppkg] = null;
				}
			}

			return true;
		}

		/**
		 * Get requirements for package
		 *
		 * @return array
		 */
		public function getWants(): array
		{
			return array_keys($this->wants);
		}

		/**
		 * Package is satisfied in FST
		 *
		 * @return bool
		 */
		public function satisfied(): bool
		{
			return \count($this->wants) === 0;
		}

		/**
		 * Get all dependencies (present + missing)
		 *
		 * @return array
		 */
		public function getDependencies(): array
		{
			return array_merge(array_keys($this->wants), array_keys($this->has));
		}

		/**
		 * Get dependencies and service location for package
		 *
		 * @return array
		 */
		public function listInstalledDependencies(): array
		{
			return $this->has;
		}

		public static function satisfyRequirements(string $package): bool
		{
			$depmap = new self($package);
			if (!$depmap->resolve()) {
				fatal("Failed to resolve dependencies for `%s'", $package);
			}

			$service = Utils::getServiceFromPackage($package);
			foreach ($depmap->getWants() as $dep) {
				info("Adding dependency `%s' to be installed under `%s'", $dep, $service);
				if (!(new Install($dep, $service))->run()) {
					fatal("Failed to install dependency `%s' in `%s'", $dep, $service);
				}
			}

			return true;

		}
	}