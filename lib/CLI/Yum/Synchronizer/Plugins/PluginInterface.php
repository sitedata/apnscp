<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins;

	interface PluginInterface
	{
		/**
		 * Called after a package installs
		 *
		 * @param string $package
		 * @return bool
		 */
		public function install(string $package): bool;

		/**
		 * Calls after a package is removed
		 *
		 * @param string $package
		 * @return bool
		 */
		public function remove(string $package): bool;

		/**
		 * Called after a package updates
		 *
		 * @param string $package
		 * @return bool
		 */
		public function update(string $package): bool;
	}

