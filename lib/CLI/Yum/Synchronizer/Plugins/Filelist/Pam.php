<?php declare(strict_types=1);
	/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, May 2020
 */

	namespace CLI\Yum\Synchronizer\Plugins\Filelist;

	use CLI\Yum\Synchronizer\Plugins\Filelist;
	use CLI\Yum\Synchronizer\Utils;

	/**
	 * Class Logrotate
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Filelist
	 *
	 * Override filelist
	 *
	 */
	class Pam extends Filelist
	{
		const DISCRETIONARY_FILES = [
			'/etc/pam.d/fingerprint-auth',
			'/etc/pam.d/password-auth',
			'/etc/pam.d/postlogin',
			'/etc/pam.d/smartcard-auth',
			'/etc/pam.d/system-auth'
		];

		public function appendFiles(array $files): array
		{
			foreach (static::DISCRETIONARY_FILES as $f) {
				if (!is_link($f)) {
					continue;
				}
				$referent = readlink($f);
				if (false !== strpos($referent, '/etc/authselect/')) {
					$files[] = $referent;
				}
			}
			return parent::appendFiles($files);
		}
	}