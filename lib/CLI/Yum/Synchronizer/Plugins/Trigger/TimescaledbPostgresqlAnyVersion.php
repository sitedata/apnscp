<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, September 2018
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use CLI\Yum\Synchronizer\Utils;
	use Opcenter\Apnscp;
	use Opcenter\Database\PostgreSQL;
	use Opcenter\System\Memory;

	/**
	 * Class TimescaledbPostgresqlAnyVersion
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class TimescaledbPostgresqlAnyVersion extends Trigger
	{
		use \NamespaceUtilitiesTrait;

		public function update(string $package): bool
		{
			$this->optimize($package);
			info('Updating %s', POSTGRESQL_DATABASE);
			$db = \PostgreSQL::pdo(true);
			$version = Utils::getMetaFromRPM($package)['version'];
			$db->exec('ALTER EXTENSION timescaledb UPDATE TO ' . $db->quote($version));
			// @XXX need better way to look for idle cycle in backend or graceful reload
			Apnscp::restart('10 minutes');
			return true;
		}

		/**
		 * Optimize PostgreSQL database for telemetry
		 */
		private function optimize(string $name) {
			if (!TELEMETRY_AUTOTUNE || !file_exists('/usr/bin/timescaledb-tune')) {
				return;
			}

			info('Optimizing database');
			$version = PostgreSQL::version();
			$maj = floor($version / 10000);
			$min = floor(($version % 1000) / 100);
			if ($maj >= 10) {
				$verPath = $maj;
			} else {
				$verPath = $maj . '.' . $min;
			}
			$amount = TELEMETRY_MEMORY_CONSUMPTION;
			if ($amount <= 0) {
				$amount = (int)min(Memory::stats()['memtotal']*3/1024, 64);
			}
			$native = (int)$amount == $amount ? 'M' : null;
			$memMax = \Formatter::changeBytes($amount, 'M', $native);

			$args = [
				(string)$verPath,
				$memMax
			];

			$cmd = 'timescaledb-tune --pg-config=/usr/pgsql-%s/bin/pg_config -memory %sMB -max-conns 40 -yes';

			// @XXX waiting on compatibility from TimescaleDB release
			$version = array_get(Utils::getMetaFromRPM('timescaledb-tools'), 'version', '0.0');
			if (version_compare($version, '0.8.0', '>=')) {
				$cmd .= ' -wal-disk-size %s';
				$args[] = TELEMETRY_MAX_WAL ? ( \Formatter::changeBytes(TELEMETRY_MAX_WAL, 'MB', $native) . 'MB' ) : '128MB';
			}

			$ret = \Util_Process::exec($cmd, $args);
			if (!$ret['success']) {
				warn('Failed to optimize PostgreSQL: %s', coalesce($ret['stderr'], $ret['stdout']));
			}
		}


	}