<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, October 2017
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\AlternativesTrigger;

	/**
	 * Class Nss
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Nss extends AlternativesTrigger
	{
		protected $alternative;

		public function __construct()
		{
			$this->alternatives = [
				[
					'name'     => 'libnssckbi.so.' . php_uname('m'),
					'src'      => '/usr/lib64/libnssckbi.so',
					'dest'     => '/usr/lib64/nss/libnssckbi.so',
					'priority' => 10
				]
			];
		}
	}