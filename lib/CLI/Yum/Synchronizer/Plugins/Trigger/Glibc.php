<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2021
	 */

	namespace CLI\Yum\Synchronizer\Plugins\Trigger;

	use CLI\Yum\Synchronizer\Plugins\Trigger;
	use Opcenter\Ftp\Vsftpd;

	/**
	 * Class TimescaledbPostgresqlAnyVersion
	 *
	 * @package CLI\Yum\Synchronizer\Plugins\Trigger
	 *
	 * @todo    support other packages
	 *
	 */
	class Glibc extends Trigger
	{
		use \NamespaceUtilitiesTrait;

		public function update(string $package): bool
		{
			// verify librt is updated in the process image
			Vsftpd::restart();

			return true;
		}
	}