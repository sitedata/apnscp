<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, February 2022
	 */

	namespace CLI\Transfer;

	use Opcenter\Account\Enumerate;
	use Opcenter\Provisioning\ConfigurationWriter;

	class CliParser implements \ArrayAccess {

		const SHORT_OPTS = "s:t:c:o:fph";
		const LONG_OPTS = [
			'force',
			'help',
			'pull',
			'override:',
			'stage:',
			'template:',
			'server:',
			'components',
			'no-create',
			'no-suspend',
			'no-notify',
			'do:'
		];

		const DEFAULT_OPTIONS = [
			'stage'    => null,
			'do'       => [],
			'sites'    => [],
			'pull'     => false,
			'log'      => '/dev/stdout',
			'server'   => null,
			'template' => null,
			'force'    => false,
			'create'   => true,
			'suspend'  => true,
			'notify'   => true,
			'override' => []
		];

		protected array $options;

		public function __construct(array $options) {
			$this->options = array_merge(static::DEFAULT_OPTIONS, array_intersect_key($options, self::DEFAULT_OPTIONS));
		}

		public static function parse(): array
		{
			$options = static::DEFAULT_OPTIONS;
			$optind = null;
			$opts = getopt(static::SHORT_OPTS, static::LONG_OPTS, $optind);
			$argv = $_SERVER['argv'];
			foreach ($opts as $o => $a) {
				switch ($o) {
					case 'h':
					case 'help':
						static::usage();
						break;
					case 'p':
					case 'pull':
						$options['pull'] = true;
						break;
					case 'components':
						static::displayComponents();
						break;
					case 'do':
						// TOFIX
						if (!in_array($a, static::stageComponents(), false)) {
							fatal("Unknown migration component `%s'", $a);
						}
						$options['stage'][] = $a;
						break;
					case 'o':
					case 'override':
						deprecated("Use -c option instead of %s", $o);
					case 'c':
						foreach (array_map('\Opcenter\CliParser::parseServiceConfiguration', (array)$a) as $opt) {
							$key = key($opt);
							$config = array_pop($opt);
							if (!isset($override[$key])) {
								$override[$key] = [];
							}
							$override[$key] = array_replace($override[$key], $config);
						}
						$options['override'] = $override;
						break;
					case 'template':
					case 't':
						$templates = array_combine(
							Notify::TEMPLATES,
							array_replace(Notify::TEMPLATES, array_values((array)\Util_Conf::inferType($a))
						));
						foreach ($templates as &$t) {
							if ($t[0] !== '/' && 0 !== strpos($t, "migration/")) {
								$t = "migrations/$t";
							}

							if (null === (new ConfigurationWriter($t, null))->getTemplatePath()) {
								fatal("Template `%s' does not exist", $t);
							}
						}

						unset($t);
						$options['template'] = $templates;
						break;
					case 's':
					case 'server':
						if (!inet_pton($a) && !preg_match(\Regex::DOMAIN, $a)) {
							warn("Fully-qualifying hostname by appending `%s'", \Util_Process::exec("dnsdomainname")['output']);
						}
						$options['server'] = $a;
						break;
					case 'stage':
						$stage = \Util_Conf::inferType($a);
						if (!is_int($stage) || $stage < 0 || $stage > 2) {
							fatal("Invalid stage `%s'", $stage);
						}
						$options['stage'] = $stage;
						break;
					case 'no-create':
						$options['create'] = false;
						break;
					case 'no-suspend':
						$options['suspend'] = false;
						break;
					case 'no-notify':
						$options['notify'] = false;
						break;
					case 'f':
					case 'force':
						$options['force'] = true;
						break;
					case '--':
						break 2;
					default:
						fatal("Unknown option `%s'", $o);

				}
			}

			if (array_get($options, 'all')) {
				$sites = Enumerate::active();
			} else {
				$sites = self::translateSites(\array_slice($_SERVER['argv'], $optind));
			}

			return [
				'options' => new static($options),
				'sites'   => $sites
			];
		}

		/**
		 * Convert identifiers into sites
		 *
		 * @param array $markers
		 * @return array
		 */
		protected static function translateSites(array $markers): array
		{
			return array_flatten(array_map(static function ($d) {
				if ('site' === ($site = 'site' . \Auth::get_site_id_from_anything($d))) {
					// not found
					$sites = array_map(static function ($d) {
						return 'site' . $d;
					}, (array)\Auth::get_site_id_from_invoice($d));

					if (!$sites) {
						warn("No sites found matching `%s'", $d);

						return [];
					}

					return array_unique((array)array_first($sites, static function ($site) {
							return (new \Opcenter\SiteConfiguration($site))->
							getServiceValue('billing', 'invoice', false);
						}) + append_config($sites));
				}

				return $site;
			}, $markers));
		}

		public static function usage()
		{
			echo "usage: " . basename($_SERVER['argv'][0]) . " [-s server] <domain>" . "\n" . "\n";
			echo " -f, --force\t\t Force stage 1 -> 2 migration ahead of 24 hour sanity check" . "\n" .
				" -s, --server\t\t Set target server (applicable in stage 0 only)" . "\n" .
				" -t, --template\t\t Specify e-mail template for stage 1 notice. Absolute or relative within templates/migration/" . "\n" .
				" --no-notify\t\t Disable notification" . "\n" .
				"     --stage\t\t Force creation stage (0, 1, 2)" . "\n" .
				"     --do\t\t Perform a single stage of migration \",\" delimits optional arguments. May be specified multiple times" . "\n" .
				"     --log\t\t Tee output a log file" . "\n" .
				"     --all\t\t Select all eligible domains on server" . "\n" .
				"     --components\t List migration stage components" . "\n" .
				" -o  --override\t\t Alter configuration on new server (first stage only)" . "\n" .
				" -p  --pull\t\t Target server is source, this server is destination." . "\n" .
				"     --no-create\t Skip account creation on target server" . "\n" .
				"     --no-suspend\t Disable automatic suspension following migration" . "\n";
			exit(1);
		}

		public static function displayComponents(): void
		{
			echo "Available components to migrate:\n";
			foreach (self::stageComponents() as $stage) {
				echo "\t", $stage, "\n";
			}
			exit(1);
		}

		/**
		 * List migration stage components
		 *
		 * @return array
		 */
		private static function stageComponents(): array
		{
			$stages = [];
			$rfxn = new \ReflectionClass(\CLI_Transfer::class);
			foreach ($rfxn->getMethods() as $method) {
				if (0 !== strpos($method->getName(), "_sync_")) {
					continue;
				}
				$stages[] = substr($method->getName(), 6);
			}

			return $stages;
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return array_key_exists($offset, static::DEFAULT_OPTIONS);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return $this->options[$offset];
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			$this->options[$offset] = $value;
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			$this->options[$offset] = static::DEFAULT_OPTIONS[$offset];
		}


	}
