<?php

	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */
	class CLI_Transfer_Util
	{
		// YYYYMMDDHH difference, include different hours
		const MIN_MIGRATION = 99;

		/**
		 * Account may migrate to next stage
		 *
		 * @param int $record
		 * @return bool
		 */
		public static function canMigrate(int $record): bool
		{
			return self::createMigrationRecord() - $record >= self::MIN_MIGRATION;
		}

		/**
		 * Create migration record
		 *
		 * @return int
		 */
		public static function createMigrationRecord(): int
		{
			return date('YmdH');
		}
	}