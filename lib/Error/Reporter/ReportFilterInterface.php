<?php

	namespace Error_Reporter;

	interface ReportFilterInterface extends FilterInterface
	{
		/**
		 * @param $errno
		 * @param $errstr
		 * @param $errfile
		 * @param $errline
		 * @param $errcontext
		 * @return mixed
		 */
		public function filter($errno, $errstr, $errfile, $errline, $errcontext);
	}