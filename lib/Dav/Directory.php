<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace Dav;

	use Sabre\DAV as Provider;

	class Directory extends Provider\Collection implements Provider\IQuota, Provider\IMoveTarget
	{
		use \apnscpFunctionInterceptorTrait;
		use \FilesystemPathTrait;
		use StatCacheTrait;

		protected $myPath;

		public function __construct($myPath)
		{
			$this->myPath = rtrim($myPath, '/');
		}

		public function getChildren()
		{
			$statarr = $this->file_get_directory_contents($this->myPath);
			if (false === $statarr) {
				throw new Provider\Exception\Forbidden('Access denied');
			}
			$dirents = array_column($statarr, 'filename');
			$this->lastCache = [
				'dir'   => $this->myPath,
				'cache' => array_combine($dirents, $statarr)
			];

			return array_map([$this, 'getChild'], $dirents);
		}

		public function delete()
		{
			return $this->file_delete($this->myPath, true);
		}

		public function setName($newName)
		{
			if ($newName[0] != '/') {
				$newName = \dirname($this->myPath) . '/' . $newName;
			}

			return $this->file_rename($this->myPath, $newName);
		}

		public function getLastModified()
		{
			$stat = $this->file_stat($this->myPath);

			return $stat['mtime'];
		}

		public function getChild($name)
		{
			$path = $this->myPath . '/' . $name;
			$stat = $this->cacheCheck($this->myPath, $name);
			if (!$stat) {
				$stat = $this->file_stat($path);
				if (!$stat) {
					throw new Provider\Exception\NotFound('The file with name: ' . $name . ' could not be found');
				}
			}

			// support symlinks...
			if ($stat['file_type'] === 'dir' || $stat['link'] === 2) {
				return new Directory($path);

			} else if ($stat['file_type'] === 'file' || $stat['link'] === 1) {
				return new File($path);
			}
			throw new Provider\Exception\Forbidden('File type cannot be accessed');
		}

		/**
		 * Check if named file exists in directory
		 *
		 * @param string $name named file
		 * @return bool file exists
		 */
		public function childExists($name)
		{
			return $this->file_exists($this->myPath . '/' . $name);
		}

		public function getName()
		{
			return basename($this->myPath);
		}

		/**
		 * Moves a node into this collection.
		 *
		 * It is up to the implementors to:
		 *   1. Create the new resource.
		 *   2. Remove the old resource.
		 *   3. Transfer any properties or other data.
		 *
		 * Generally you should make very sure that your collection can easily move
		 * the move.
		 *
		 * If you don't, just return false, which will trigger sabre/dav to handle
		 * the move itself. If you return true from this function, the assumption
		 * is that the move was successful.
		 *
		 * @param string         $targetName New local file/collection name.
		 * @param string         $sourcePath Full path to source node
		 * @param Provider\INode $sourceNode Source node itself
		 * @return bool
		 */
		public function moveInto($targetName, $sourcePath, Provider\INode $sourceNode)
		{
			// We only support FSExt\Directory or FSExt\File objects, so
			// anything else we want to quickly reject.
			if (!$sourceNode instanceof Provider && !$sourceNode instanceof File) {
				return false;
			}

			// PHP allows us to access protected properties from other objects, as
			// long as they are defined in a class that has a shared inheritence
			// with the current class.
			return $this->file_rename($sourceNode->getName(), $this->myPath . '/' . $targetName);
		}

		public function createFile($name, $data = null)
		{
			if ($name == '.' || $name == '..') {
				throw new Provider\Exception\Forbidden('Permission denied to . and ..');
			}
			$filepath = $this->myPath . '/' . $name;
			if (!$this->file_touch($filepath)) {
				throw new Provider\Exception\Forbidden('Permission denied to create file (filename ' . $name . ')');
			}
			$pointer = $this->file_expose($filepath, 'write');
			$pointer = fopen($this->domain_fs_path() . $pointer, 'w');
			if (!$pointer) {
				throw new Provider\Exception\ServiceUnavailable('Failed to open stream');
			}
			stream_copy_to_stream($data, $pointer);
			fclose($pointer);

			return (new File($this->myPath . '/' . $name))->getETag();
		}

		public function createDirectory($name)
		{
			$newPath = $this->myPath . '/' . $name;
			if (!$this->file_create_directory($newPath)) {
				throw new Provider\Exception\Forbidden('Permission denied to create directory');
			}
		}

		public function getQuotaInfo()
		{
			$quota = $this->user_get_quota();

			return [
				$quota['qused'],
				$quota['qhard'] - $quota['qused']
			];
		}

		public function getETag()
		{
			$hash = $this->file_etag($this->myPath);
			if (!$hash) {
				return $hash;
			}

			return '"' . $hash . '"';
		}
	}