<?php

	/**
	 * Util Process extension
	 * escape untrusted arguments
	 *
	 * MIT License
	 *
	 * @author  Matt Saladna <matt@apisnetworks.com>
	 * @license http://opensource.org/licenses/MIT
	 * @version $Rev: 1786 $ $Date: 2015-05-28 00:15:38 -0400 (Thu, 28 May 2015) $
	 */
	class Util_Process_Safe extends Util_Process
	{
		protected function synthesizeCommand(string $cmd, array $args = []): string
		{
			if (is_array($this->getCommand(true))) {
				// escapeshellarg only has an effect on shell, direct execve is unnecessary
				return parent::synthesizeCommand($cmd, $args);
			}
			return parent::synthesizeCommand($cmd, $this->deepEscape($args));
		}

		protected function deepEscape(array $args, int $depth = 0): array
		{
			foreach ($args as $key => $arg) {
				// only strings can carry nasty payloads
				// objects could with a __toString() method
				// if a module were stupid enough to implement such a calamity
				if (is_array($arg)) {
					$args[$key] = $this->deepEscape($args[$key], $depth + 1);
				} else if (is_string($arg) ||
					is_object($arg) && method_exists($arg, '__toString'))
				{
					$args[$key] = escapeshellarg($arg);
				}
			}

			return $args;
		}
	}