<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	/**
	 * Resource limit management
	 */
	class Util_Ulimit
	{
		/**
		 * @link http://php.net/manual/en/posix.constants.setrlimit.php
		 * @var array limit-flag mapping
		 */
		static private $_constMap = array(
			'core'       => '-c',
			'data'       => '-d',
			'nice'       => '-e',
			'fsize'      => '-f',
			'sigpending' => '-i',
			'memlock'    => '-l',
			'rss'        => '-m', // useless
			'nofile'     => '-n',
			'msgqueue'   => '-q',
			'rtprio'     => '-r',
			'stack'      => '-s',
			'cpu'        => '-t',
			'nproc'      => '-u',
			'as'         => '-v'
		);

		static private $_translationMap = array(
			'nofile' => 'openfiles',
			'as'     => 'virtualmem',
			'nproc'  => 'maxproc',
			'fsize'  => 'filesize',
		);

		/**
		 * Set a ulimit
		 *
		 * @param             $limit
		 * @param             $value
		 * @param string|null $type
		 * @return bool|void
		 */
		public static function set(string $limit, $value, string $type = null)
		{
			if ($value !== 'unlimited' && !is_int($value) && !isset(self::$_constMap[$limit])) {
				return error("invalid value `%s'", $value);
			}
			if (function_exists('posix_setrlimit')) {
				$constant = self::_limit2Const($limit);
				if ($value === 'unlimited') {
					// multi-platform compatibility :(
					$value = constant('POSIX_RLIMIT_INFINITY');
				}
				$soft = $type === 'soft' || is_null($type) ? $value : self::get($limit, 'soft');
				$hard = $type === 'hard' || is_null($type) ? $value : self::get($limit, 'hard');

				return posix_setrlimit(constant($constant), $soft, $hard);
			}
			$flag = self::_limit2Flag($limit);
			if ($type == 'hard') {
				warn('prlimit does not distinguish between hard/soft');
				$flag = '-H ' . $flag;
			} else if ($type == 'soft') {
				warn('prlimit does not distinguish between hard/soft');
				$flag = '-S ' . $flag;
			}
			/**
			 * ulimit won't work. It operates in a subshell that
			 * terminates upon execution resetting the rlimit. prlimit
			 * is the only viable option... besides gdb
			 */
			$ret = Util_Process::exec(
				'prlimit %s%s --pid %s', $flag, $value, getmypid()
			);

			return $ret['success'];
		}

		private static function _limit2Const($limit)
		{
			if (!isset(self::$_constMap[$limit])) {
				return error("unknown limit `%s'", $limit);
			}

			return strtoupper('POSIX_RLIMIT_' . $limit);
		}

		/**
		 * Get a ulimit value
		 *
		 * @param        $limit
		 * @param string $type
		 * @return bool|void
		 */
		public static function get($limit, $type = 'soft')
		{
			if (!isset(self::$_constMap[$limit])) {
				return error("unknown limit `%s'", $limit);
			} else if ($type != 'hard' && $type != 'soft') {
				return error("unknown limit specifier `%s'", $type);
			}
			if (function_exists('posix_getrlimit')) {
				$limits = posix_getrlimit();
				$key = $limit;
				if (isset(self::$_translationMap[$limit])) {
					$key = self::$_translationMap[$limit];
				}
				$key = $type . ' ' . $key;
				if (!isset($limits[$key])) {
					// ???
					warn("unknown limit `%s' requested", $key);

					return null;
				}

				return $limits[$key] === 'unlimited' ? POSIX_RLIMIT_INFINITY : (int)$limits[$key];
			}

			// old school!
			$flag = self::$_constMap[$limit];
			if ($type === 'hard') {
				$flag = '-H ' . $flag;
			}
			$ret = Util_Process::exec('ulimit %s', $flag);
			if (!$ret['success']) {
				return error("failed to get limit `%s': %s", $limit, $ret['error']);
			}
			$limit = trim($ret['output']);

			return $limit === 'unlimited' ? POSIX_RLIMIT_INFINITY : (int)$limit;
		}

		private static function _limit2Flag($limit)
		{
			if (!isset(self::$_constMap[$limit])) {
				return error("unknown limit `%s'", $limit);
			}

			return self::$_constMap[$limit];
		}
	}