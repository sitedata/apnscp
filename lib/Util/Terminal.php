<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, June 2017
	 */

	class Util_Terminal
	{
		/**
		 * Prompt for password with confirmation
		 *
		 * @return string
		 */
		public static function password_interactive()
		{
			$fp = fopen('php://stdin', 'r');
			if (!$fp) {
				fatal("can't open stdin for reading password! is terminal interactive?");
			}
			do {
				echo 'Enter a password: ';
				static::hide_input();
				$passwd = stream_get_line($fp, 1024, PHP_EOL);
				static::unhide_input();
				echo "\nVerify password: ";
				static::hide_input();
				$passwd2 = stream_get_line($fp, 1024, PHP_EOL);
				static::unhide_input();
				if ($passwd === $passwd2) {
					break;
				}
				echo "\033[0m";
				echo "\nPasswords do not match\n";
			} while (true);
			echo "\n";
			fclose($fp);

			return $passwd;
		}

		public static function hide_input()
		{
			return system('stty -echo');
		}

		public static function unhide_input()
		{
			return system('stty echo');
		}
	}