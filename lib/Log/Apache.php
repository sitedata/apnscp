<?php declare(strict_types=1);
/*
 * 	Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * 	Unauthorized copying of this file, via any medium, is
 * 	strictly prohibited without consent. Any dissemination of
 * 	material herein is prohibited.
 *
 * 	For licensing inquiries email <licensing@apisnetworks.com>
 *
 * 	Written by Matt Saladna <matt@apisnetworks.com>, November 2023
 */

namespace Log;

use Opcenter\Contracts\Virtualizable;
use Opcenter\Contracts\VirtualizedContextable;

class Apache implements Virtualizable, VirtualizedContextable
{
	use \ContextableTrait;
	const LOCATION = '/var/log/httpd';

	private string $root;

	public function __construct(string $root = '/')
	{
		$this->root = $root;
	}

	public static function bindTo(string $root = '/')
	{
		return new static($root);
	}

	public function path(string $log = ''): string
	{
		return ltrim($this->root . "/" . self::LOCATION . "/{$log}", '/');
	}


}