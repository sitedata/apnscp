<?php

	use Opcenter\Map;

	require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class ParentingTest extends TestFramework
	{
		public function testSubordination() {
			/*********
			 * site 4
			 *   \/
			 * site 3
			 *   \/
			 * site 2
			 *   \/
			 * site 1
			 */
			$handle = $this->mockHierarchy();
			$this->assertTrue($handle->check(), 'Empty map works');
			$handle['site2'] = 'site1';
			$this->assertTrue($handle->check(), 'site2 subordinate to site1');
			$handle['site3'] = 'site2';
			$this->assertTrue($handle->check(), 'site3 subordinate to site2');
			$handle['site4'] = 'site3';
			$this->assertTrue($handle->check(), 'site4 subordinate to site3');
			try {
				unset($handle['site3']);
				$this->fail('Removing site3 orphans site4');
			} catch (\Opcenter\Reseller\Exceptions\OrphanedNode $e) { }

			$this->assertCount(3, $handle->getDependents('site1', -1));
			$this->assertCount(2, $handle->getDependents('site1', 2));
			$handle['site5'] = 'site1';
			$this->assertCount(3, $handle->getDependents('site1', 2));
			try {
				unset($handle);
			} catch (\Opcenter\Reseller\Exceptions\HierarchyCycleException $e) { }
		}

		public function testMove() {
			$handle = $this->mockHierarchy();
			$handle['site2'] = 'site1';
			$this->assertCount(1, $handle->getDependents('site1'));
			$handle['site2'] = 'site3';
			$this->assertTrue($handle->check());
			$this->assertCount(0, $handle->getDependents('site1'));
			$this->assertCount(1, $handle->getDependents('site3'));
		}

		public function testMoveComplex() {
			$handle = $this->mockHierarchy();
			$handle['site2'] = 'site1';
			$handle['site3'] = 'site2';
			$handle['site4'] = 'site1';
			$handle['site5'] = 'site2';
			$handle['site6'] = 'site3';
			$this->assertCount(5, $handle->getDependents('site1', -1));
			$this->assertCount(3, $handle->getDependents('site2', -1));
			// promote to parent
			$handle['site3'] = null;
			$this->assertTrue($handle->check());
			$this->assertCount(1, $handle->getDependents('site3', -1));
			$this->assertCount(1, $handle->getDependents('site2', -1));

		}

		protected function mockHierarchy(): \Opcenter\Reseller\Hierarchy
		{
			return new class extends \Opcenter\Reseller\Hierarchy
			{
				private $file;

				public function __construct()
				{
					parent::__construct($this->file = tempnam(TEMP_DIR, 'maptest'));
				}

				public function __destruct() {
					if (file_exists($this->file)) {
						unlink($this->file);
					}
				}

				public function getMap(string $flags = 'cd'): \Opcenter\Map
				{
					return Map::load($this->file, $flags);
				}
			};
		}
	}