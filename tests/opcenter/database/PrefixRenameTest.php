<?php
/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, October 2020
 */

require_once dirname(__DIR__, 2) . '/TestFramework.php';

	class PrefixRenameTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testMySQLPrefix()
		{
			$this->prefix('mysql', 'MySQL');
		}

		public function testPostgreSQLPrefix()
		{
			$this->prefix('pgsql', 'PostgreSQL');
		}

		private function prefix(string $module, string $class) {
			$acct = \Opcenter\Account\Ephemeral::create([
				"${module}.enabled" => 1,
				"${module}.dbaseprefix" => null
			]);
			$class = "\Opcenter\Database\\${class}";
			$oldPrefix = $acct->getContext()->getAccount()->cur[$module]['dbaseprefix'];

			$this->assertTrue($acct->getApnscpFunctionInterceptor()->{$module . '_create_database'}("foo"), 'Created database');
			$this->assertTrue($class::databaseExists("${oldPrefix}foo"));
			$this->assertTrue($acct->getApnscpFunctionInterceptor()->{$module . '_database_exists'}("foo"));
			if ($module === 'pgsql') {
				$conn = PostgreSQL::stub();
				$query = 'CREATE TABLE "a-c" (id serial);';
			} else {
				$conn = \MySQL::stub();
				$query = 'CREATE TABLE `' . $oldPrefix . 'foo`.`a-c` (id int(4) auto_increment primary key);';
			}
			$conn->connect(
				'localhost',
				$acct->getContext()->username,
				$acct->getApnscpFunctionInterceptor()->{$module . '_get_password'}(),
				$oldPrefix . "foo"
			);
			$this->assertNotFalse($conn->query($query), "Create $module DB");
			$newPrefix = \Opcenter\Auth\Password::generate(10, 'a-z0-9') . '_';
			$conn = null;
			if ($module === 'pgsql') {
				// pg_connect uses the last connection when parameter omitted,
				// closing connection breaks implied connection
				\PostgreSQL::initialize()->close();
			}
			$this->assertTrue(\Util_Account_Editor::instantiateContexted($acct->getContext())->setConfig([
				"${module}.dbaseprefix" => $newPrefix
			])->edit());

			$this->assertFalse(\Opcenter\Map::load($class::PREFIX_MAP)->exists($oldPrefix), "$module DB unmapped");
			$this->assertTrue(\Opcenter\Map::load($class::PREFIX_MAP)->exists($newPrefix), "$module DB mapped");
			// check if db exists
			$this->assertTrue($acct->getApnscpFunctionInterceptor()->{$module . '_database_exists'}("foo"), "$module DB exists in API");
			$this->assertFalse($class::databaseExists("${oldPrefix}foo"), "$module DB removed");
			$this->assertTrue($class::databaseExists("${newPrefix}foo"));
		}
	}

