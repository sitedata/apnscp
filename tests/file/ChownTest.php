<?php
require_once dirname(__FILE__) . '/../TestFramework.php';

class ChownTest extends TestFramework
{
	protected $sourceDir;
	protected $auth;
	private string $user;

	public function setUp(): void
	{
		parent::setUp();
		$this->auth = TestHelpers::create(array_get(Definitions::get(), 'auth.site.domain'));
		$this->site = $this->auth->site;
		$afi = apnscpFunctionInterceptor::factory($this->auth);
		$this->setApnscpFunctionInterceptor($afi);

		$this->sourceDir = $this->user_get_home() . '/unit-test-' . microtime(true);
		$this->assertTrue($this->file_create_directory($this->sourceDir));
		$this->user = \Opcenter\Auth\Password::generate(8, 'a-z');
		$this->assertTrue($this->user_add($this->user, \Opcenter\Auth\Password::generate()), 'User created');
	}

	public function tearDown(): void {
		if ($this->user_exists($this->user)) {
			$this->user_delete($this->user);
		}
		$this->file_delete($this->sourceDir, true);
		parent::tearDown();
	}

	public function testChownSubUser()
	{
		$this->sourceDir = $this->user_get_home($this->user) . '/unit-test.' . random_int(0, PHP_INT_MAX);
		$this->assertTrue($this->file_create_directory($this->sourceDir));
		$this->assertTrue($this->file_chmod($this->sourceDir, 711));
		$this->assertTrue($this->file_chown($this->sourceDir, $this->user));
		$userAfi = \apnscpFunctionInterceptor::factory(\Auth::context($this->user, $this->auth->site));
		$this->assertInstanceOf(\apnscpFunctionInterceptor::class, $userAfi);
		$this->assertSame($userAfi->common_whoami(), $this->user);
		$this->assertEquals(
			0,
			$userAfi->pman_run('echo -n "foo bar baz" > ' . $this->sourceDir . '/test-file')['return'],
			'DACs reflected in filesystem'
		);
		$this->assertSame('foo bar baz', $userAfi->file_get_file_contents($this->sourceDir . '/test-file'));


	}
}

