<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, August 2019
 */

	require_once dirname(__FILE__) . '/../TestFramework.php';

	class ProcessTest extends TestFramework
	{
		public function testPriority()
		{
			$basePrio = pcntl_getpriority();
			do {
				$prio = random_int(-20, 19);
			} while ($prio !== $basePrio && $prio === 0);
			$proc = new \Util_Process();
			$proc->addCallback(function ($cmd, \Util_Process $self) use ($basePrio) {
				$pid = posix_getpid();
				$stat = \Opcenter\Process::stat($pid);
				$this->assertSame($basePrio, $stat['nice']);
			}, 'open');
			$proc->setPriority($prio);

			$proc->addCallback(function($cmd, \Util_Process $self) use ($prio) {
				$pid = $self->getProcessStatus()['pid'];
				$path = '/proc/' . $pid . '/stat';
				$this->assertFileExists($path, 'Process created in table');
				// process space isn't updated yet with new process name yet
				$stat = \Opcenter\Process::stat($pid);
				$this->assertSame($prio, $stat['nice']);
			}, 'exec');
			$proc->run('true');
		}

		public function testSigchldExitCodes() {
			if (!CFG_DEBUG) {
				return $this->markTestSkipped('DEBUG not enabled');
			}

			$this->assertEquals(12, $this->test_sigchld_exit(12, true), 'Backend sigchld');
			$this->assertEquals(
				$this->test_sigchld_exit(29, true),
				$this->test_sigchld_exit(29, false),
				'sigchld consistency check'
			);
		}

		public function testArgumentFormatting()
		{
			$oldEx = \Error_Reporter::exception_upgrade(\Error_Reporter::E_WARNING);
			defer($_, static fn() => \Error_Reporter::exception_upgrade($oldEx));
			$this->assertEquals('Foo Bar' . "\n",
				array_get(\Util_Process::exec('/bin/echo %s', 'Foo Bar'), 'stdout'),
				'Non-placement formatting'
			);

			$this->assertEquals('Foo' . "\n",
				array_get(\Util_Process::exec('/bin/echo %s %s', [null, 'Foo']), 'stdout'),
				'Non-placement null formatting'
			);

			$this->assertSame(
				\Util_Process::exec(['/bin/echo', '%(foo)s', '%(bar)s'], ['foo' => null, 'bar' => 'Foo']),
				\Util_Process::exec(['/bin/echo', '%s', '%s'], [null, 'Foo']),
				'Null formatting'
			);

			$this->assertSame(
				\Util_Process::exec('/bin/echo %s %s', null, 'Foo'),
				\Util_Process::exec(['/bin/echo', '%(foo)s', '%(bar)s'], ['foo' => null, 'bar' => 'Foo']),
				'Null formatting'
			);

			$this->assertSame(
				\Util_Process::exec('/bin/echo %s %s', null, 'Foo'),
				\Util_Process::exec(['/bin/echo', '%s', '%s'], [null, 'Foo']),
				'Null formatting'
			);

			$this->assertEquals('Foo Bar Bar Baz' . "\n",
				array_get(\Util_Process::exec('/bin/echo %1$s %2$s', 'Foo Bar', 'Bar Baz'), 'stdout'),
				'Placement formatting'
			);

			$this->assertEquals('Foo Bar Bar Baz' . "\n",
				array_get(\Util_Process::exec('/bin/echo %(foo)s %(bar)s', ['foo' => 'Foo Bar', 'bar' => 'Bar Baz']), 'stdout'),
				'Symbolic placement formatting'
			);

			$this->assertEquals('Foo Bar Bar Baz' . "\n",
				array_get(\Util_Process::exec(
					['/bin/echo', '%(foo)s', '%(bar)s'],
					['foo' => 'Foo Bar', 'bar' => 'Bar Baz']
				), 'stdout'),
				'Shell bypass symbolic placement formatting'
			);

			$this->assertTrue(array_get(\Util_Process::exec(
				'/bin/false %(foo)s %(bar)s',
				['foo' => 'Foo Bar', 'bar' => 'Bar Baz'],
				[0, 1]
			), 'success'),
				'Additional argument formatting'
			);

			$this->assertTrue(array_get(\Util_Process::exec(
					['/bin/false', '%(foo)s', '%(bar)s'],
					['foo' => 'Foo Bar', 'bar' => 'Bar Baz'],
					[0,1]
				), 'success'),
				'Shell bypass additional argument formatting'
			);

			$ret = \Util_Process::exec(
				'/bin/echo %2$s %1$s && /bin/false', 'Bar', 'Foo',
				[1]
			);

			$this->assertTrue(array_get($ret, 'success'));
			$this->assertEquals('Foo Bar' . "\n", array_get($ret, 'stdout'), 'Variadic additional argument formatting');

			$ret = \Util_Process::exec(
				'/bin/echo %s %s && /bin/false', 'Bar', 'Foo',
				[1]
			);

			$this->assertTrue(array_get($ret, 'success'));
			$this->assertEquals('Bar Foo' . "\n", array_get($ret, 'stdout'), 'Variadic non-placement additional argument formatting');

			$this->assertSame(
				\Util_Process_Safe::exec(['/bin/echo', '%s', '%s'], "Bar'Baz", 'Foo'),
				\Util_Process_Safe::exec('/bin/echo %s %s', "Bar'Baz", 'Foo'),
				'Safe formatting'
			);
		}
	}



