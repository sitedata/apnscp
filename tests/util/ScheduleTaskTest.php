<?php
require_once dirname(__FILE__) . '/../TestFramework.php';

class ScheduleTaskTest extends TestFramework
{
	private $_temp;

	/**
	 * Prepares the environment before running a test.
	 */
	protected function setUp(): void
	{
		parent::setUp();
		$this->_temp = sprintf("test%07d", mt_rand(0, 9999999));
		if (file_exists($this->_temp)) {
			unlink($this->_temp);
		}
		touch($this->_temp);
	}

	/**
	 * Cleans up the environment after running a test.
	 */
	protected function tearDown(): void
	{
		if (file_exists($this->_temp))
			unlink($this->_temp);
		parent::tearDown();

	}

	public function testScheduleTime ()
	{
		$load = $this->common_get_load();
		if (array_shift($load) > NPROC*4) {
			return $this->markTestSkipped("Load average exceeds atd limit " . NPROC*4);
		}
		$proc = new Util_Process_Schedule("2 seconds");
		$contents = file_get_contents($this->_temp);
		$this->assertEquals($contents,"");
		$proc->run("echo -n foo > %s", $this->_temp);
		sleep(5);
		$contentsnew = file_get_contents($this->_temp);
		$this->assertEquals($contentsnew,"foo");
	}

	public function testScheduleTime2 ()
	{
		$load = $this->common_get_load();
		if (array_shift($load) > NPROC * 4) {
			return $this->markTestSkipped("Load average exceeds atd limit " . NPROC * 4);
		}

		$d = new DateTime();
		$dold = clone($d);
		$d->modify("+1 second");
		$this->assertNotSame($d, $dold);
		$proc = new Util_Process_Schedule($d);
		$str = "foozy!";
		$contents = file_get_contents($this->_temp);
		$this->assertEquals($contents,"");
		$proc->run("echo -n '%s' > %s", $str, $this->_temp);
		sleep(5);
		$contentsnew = file_get_contents($this->_temp);
		$this->assertEquals($contentsnew,$str);

	}

}


