<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class NextcloudTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function testVersionFetch()
		{
			$afi = \apnscpFunctionInterceptor::init();
			$versions = $afi->nextcloud_get_versions();
			$this->assertNotEmpty($versions, 'Version check succeeded');
			$this->assertContains('19.0.4', $versions, '19.0.4 in version index');
			$this->assertGreaterThan(array_search('19.0.4', $versions), array_search('19.0.13', $versions),
				'Versions are ordered');
		}

		public function testInstallUpgrade() {
			$account = \Opcenter\Account\Ephemeral::create(['cgroup.enabled' => 0]);
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			$versions = $afi->nextcloud_get_versions();
			$version = '24.0.6';
			$maximal = \Opcenter\Versioning::maxVersion($versions, \Opcenter\Versioning::asMinor($version));
			$this->assertNotEquals($version, $maximal, 'Version update can succeed');

			// Laravel always installs maximal minor...
			$this->assertTrue(
				$afi->nextcloud_install(
					$domain,
					'',
					[
						'version' => $version,
						'notify'  => false,
						'ssl'     => false,
						'email'   => null,
						'verlock' => 'minor'
					]
				)
			);
			$this->assertEquals($version, $afi->nextcloud_get_version($domain));

			$this->assertTrue(
				$afi->nextcloud_update_all($domain, ''),
				'Nextcloud update succeeded'
			);

			$this->assertEquals($maximal, $afi->nextcloud_get_version($domain), 'Correct Nextcloud version is installed');
		}
	}

