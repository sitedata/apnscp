<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class ProviderModuleTest extends TestFramework
	{
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;
		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testProviderSelection()
		{
			$acct1 = \Opcenter\Account\Ephemeral::create([
				'dns' => [
					'provider' => 'null'
				]
			]);
			$dns = \Dns_Module::instantiateContexted($acct1->getContext());
			$this->assertEquals(Opcenter\Dns\Providers\Null\Module::class, get_class($dns->_proxy()));
			$acct1->destroy();
		}

		public function testNonExistentProvider() {

			$found = false;
			try {
				\Opcenter\Account\Ephemeral::create(
					[
						'dns' => [
							'provider' => 'doESNoTExISt'
						]
					]
				);
			} catch (\apnscpException $ex) {
				$found = false !== strpos($ex->getMessage(), 'doESNoTExISt');
				$this->assertTrue($found, 'Fail on loading non-existent provider');
			} finally {
				if (!$found) {
					$this->fail('Non-existent provider did not raise error');
				}
			}

		}

		public function testDisabledServiceDefault() {
			$oldex = \Error_Reporter::exception_upgrade(Error_Reporter::E_FATAL);
			try {
				$this->assertInstanceOf(\Opcenter\Account\Ephemeral::class,
					\Opcenter\Account\Ephemeral::create(
						[
							'dns' => [
								'enabled' => 0
							],
							'mail' => [
								'enabled' => 0
							]
						]
				), 'Account created');
			} catch (\apnscpException $ex) {
				$this->fail('Fail on setting service,enabled=0');
			} finally {
				\Error_Reporter::exception_upgrade($oldex);
			}

		}
	}

