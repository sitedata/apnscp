<?php

	namespace account;

	use Opcenter\Account\Edit;
	use Opcenter\System\Cgroup\Controller;
	use Opcenter\System\Cgroup\Group;
	use TestFramework;

	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class ReconfigUpdate extends TestFramework
	{

		protected ?\Opcenter\Account\Ephemeral $account;
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function setUp(): void
		{
			$this->account = \Opcenter\Account\Ephemeral::create([
				'cgroup.enabled' => true,
				'cgroup.proclimit' => 69
			]);
		}

		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testReconfigFlag()
		{
			$controller = Controller::make(new Group($this->account->getContext()->site), 'pids');
			$this->assertEquals(69, $controller->createAttribute('max', null)->read());
			(new Edit($this->account->getContext()->site, ['cgroup' => ['proclimit' => 420]]))->setOption('reconfig', true)->exec();
			$this->assertEquals(420, $controller->createAttribute('max', null)->read());

		}
	}