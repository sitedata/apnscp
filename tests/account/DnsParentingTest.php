<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	class DnsParentingTest extends TestFramework
	{
	    /**
		 * A basic test example.
		 *
		 * @return void
		 */
		public function testParent()
		{
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$this->swapHandler($afi);
			$domain = $account->getContext()->domain;
			$this->assertFalse($afi->dns_parent_test($domain), 'Self is not parented');
			$this->assertTrue($afi->dns_parent_test("foo.$domain"), 'Subdomain is parented');
			$this->assertFalse($afi->dns_parent_test("$domain.au"), 'TLD is not parented');
			$this->assertSame($domain, $afi->dns_get_parent_test("foo.$domain"), 'Subdomain is parented');
		}

		public function testParentAddition() {
			$account = \Opcenter\Account\Ephemeral::create();
			$afi = $account->getApnscpFunctionInterceptor();
			$domain = $account->getContext()->domain;
			$this->swapHandler($afi);
			$this->assertTrue($afi->aliases_add_domain("$domain.au.test", '/var/www/test'));
			$this->assertFalse($afi->dns_parent_test("$domain.au.test"));
			// domain not attached yet
			$this->assertFalse($afi->dns_parent_test("foo.$domain.au.test"));
			$this->assertTrue($afi->aliases_synchronize_changes());

			$this->assertFalse($afi->dns_parent_test("$domain.au.test"));
			$this->assertTrue($afi->dns_parent_test("foo.$domain.au.test"));
			return;

		}

		private function swapHandler(\apnscpFunctionInterceptor $afi): void
		{
			$afi->swap('dns',
				new class extends Dns_Module {
					public function parent_test(string $domain)
					{
						return $this->parented($domain);
					}

					public function get_parent_test(string $domain)
					{
						return $this->getParent($domain);
					}
				}
			);
		}
	}
