<?php
	require_once dirname(__DIR__, 1) . '/TestFramework.php';

	/**
	 * Class Duplicate
	 *
	 * Test duplicate account handling
	 *
	 */
	class RenamesTest extends TestFramework
	{

		protected ?\Opcenter\Account\Ephemeral $account;
		const EXCEPTION_LEVEL = \Error_Reporter::E_ERROR;

		public function setUp(): void
		{
			$this->account = \Opcenter\Account\Ephemeral::create([
				'aliases.max' => null,
				'apache.webuser' => 'apache'
			]);
		}

		/**
		 * @covers \Opcenter\Account\Create
		 */
		public function testUsernameChange()
		{
			if (!AUTH_ALLOW_USERNAME_CHANGE) {
				$this->markTestSkipped('Username changes disabled');
			}

			$newUser = \Opcenter\Auth\Password::generate(16, 'a-z');

			$ui = \Module\Support\Webapps\App\UIPanel::instantiateContexted($this->account->getContext());
			$ui->get($this->account->getContext()->domain);
			$ui = null;

			$this->account->getApnscpFunctionInterceptor()->auth_change_username($newUser);
			$this->assertEquals($newUser, $this->account->getApnscpFunctionInterceptor()->common_whoami());

			$ui = \Module\Support\Webapps\App\UIPanel::instantiateContexted($this->account->getContext());
			$ui->get($this->account->getContext()->domain);
			$ui = null;

		}

		public function testDomainChange()
		{
			if (!AUTH_ALLOW_DOMAIN_CHANGE) {
				$this->markTestSkipped('Domain changes disabled');
			}

			$newDomain = \Opcenter\Account\Ephemeral::random('domain');
			$this->account->getApnscpFunctionInterceptor()->auth_change_domain($newDomain);
			$this->assertEquals($newDomain,
				$this->account->getApnscpFunctionInterceptor()->common_get_service_value('siteinfo', 'domain'));
			$this->assertStringContainsString($newDomain, $this->account->getApnscpFunctionInterceptor()->common_get_web_server_name());
		}

		public function testPrefixChange()
		{
			if (!AUTH_ALLOW_DATABASE_CHANGE) {
				$this->markTestSkipped('Prefix changes disabled');
			}

			$newPrefix = \Opcenter\Auth\Password::generate(14, 'a-z') . '_';
			$this->account->getApnscpFunctionInterceptor()->sql_change_prefix($newPrefix);
			$this->assertEquals($newPrefix,
				$this->account->getApnscpFunctionInterceptor()->mysql_get_prefix());
		}

		public function testConcurrentChange()
		{
			$this->markTestSkipped('@todo');
			$testDomain = \Opcenter\Account\Ephemeral::random('domain');
			$testUser = \Opcenter\Auth\Password::generate(16, 'a-z');
			$testDbPrefix = \Opcenter\Auth\Password::generate(16, 'a-z') . '_';


			\var_dump("IN", $this->account->getContext()->username, "OUT", $testUser);
			(new \Opcenter\Account\Edit($this->account->getContext()->site, [
				'siteinfo' => [
					'domain'    => $testDomain,
					'admin_user' => $testUser
				],
				'mysql' => [
					'dbaseprefix' => $testDbPrefix
				]
			]))->exec();

			// update reference
			$this->assertEquals($testUser, $this->account->getApnscpFunctionInterceptor()->common_whoami());
			$this->assertEquals($testDomain,
				$this->account->getApnscpFunctionInterceptor()->common_get_service_value('siteinfo', 'domain'));
			$this->assertStringContainsString($testDomain,
				$this->account->getApnscpFunctionInterceptor()->common_get_web_server_name());
			$this->assertEquals($testDbPrefix,
				$this->account->getApnscpFunctionInterceptor()->mysql_get_prefix());

		}

	}