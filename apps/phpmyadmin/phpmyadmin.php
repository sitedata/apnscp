<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\phpmyadmin;

	use Error_Reporter;
	use Opcenter\Crypto\NaiveCrypt;
	use Opcenter\Net\Ip4;
	use Page_Container;

	class Page extends Page_Container
	{
		public $failed = true;
		private $phpMyAdminURL;
		private $mysqlUsername;
		private $mysqlPassword;

		// database to redirect
		private $database;

		public function __construct()
		{
			parent::__construct();
			$this->add_css('phpmyadmin.css');
			Error_Reporter::suppress_php_error('mysqli_connect');
			\Page_Renderer::display_on_error();
			$this->mysqlUsername = $this->get_mysql_username();
			$this->mysqlPassword = $this->get_mysql_password();
			if (!$this->validatePassword()) {
				$this->mysqlPassword = null;
				error("Unable to login to phpMyAdmin, please update your password.");
			}

			if (is_null($this->mysqlPassword)) {
				return;
			}

			if (isset($_GET['db']) && preg_match(\Regex::SQL_DATABASE, $_GET['db'])) {
				$this->database = $_GET['db'];
			}

			$this->setup_phpmyadmin_session();
		}

		private function validatePassword(): bool
		{
			if (sizeof($_POST) > 1) {
				// handled elsewhere
				return true;
			}
			if ($this->check_mysql_login($this->mysqlUsername, $this->mysqlPassword)) {
				return true;
			}

			$password = $this->getAccountPassword();
			if (!$password || $this->mysqlPassword === $password) {
				return false;
			}

			if (!$this->check_mysql_login($this->mysqlUsername, $password)) {
				return false;
			}
			// cPanel style import, account password is same as DB
			// for safety let's never prompt user to store this in .my.cnf
			// a compromised PHP-FPM pool running as that user would be a disaster
			$this->mysqlPassword = $password;

			return true;
		}

		private function getAccountPassword()
		{
			if (empty($_SESSION['password']) || !($_SESSION['password'] instanceof NaiveCrypt)) {
				return (string)($_SESSION['password'] ?? '');
			}

			return $_SESSION['password']->get();
		}

		public function get_mysql_username()
		{
			$user = $this->sql_get_mysql_option('user', 'client');
			if (!$user) {
				$user = $_SESSION['username'];
				$this->sql_set_mysql_option('user', $user, 'client');
			}

			return $user;
		}

		public function set_mysql_username($user)
		{
			$olduser = $this->sql_get_mysql_option('user', 'client');
			if ($olduser != $user) {
				warn("changed default mysql user from `%s' to `%s'",
					$olduser, $user);
			}
			$this->mysqlUsername = $user;

			return $this->sql_set_mysql_option('user', $user, 'client');
		}

		public function get_mysql_password()
		{
			return $this->sql_get_mysql_option('password', 'client');
		}

		public function set_mysql_password($pass)
		{
			return $this->sql_set_mysql_option('password', $pass, 'client');
		}

		public function check_mysql_login($username, $password)
		{
			try {
				$db = mysqli_connect("localhost", $username, $password);
				return $db && mysqli_close($db);
			} catch (\mysqli_sql_exception $e) {
				return false;
			}

		}

		public function setup_phpmyadmin_session()
		{
			if (!$this->mysqlUsername) {
				$this->mysqlUsername = $_SESSION['username'];
			}
			$curl = new \CurlLayer();
			$curl->set_form_method('GET');
			$fn = tmpfile();
			$curl->change_option(CURLOPT_COOKIEFILE, $fn);
			$curl->change_option(CURLOPT_COOKIESESSION, 1);
			$curl->change_option(CURLOPT_FOLLOWLOCATION, true);
			$curl->change_option(CURLOPT_HEADER, true);
			$routed = Ip4::enabled() ? '127.0.0.1' : '[::1]';
			$curl->change_option(CURLOPT_RESOLVE, [SERVER_NAME . ':443:' . $routed, SERVER_NAME . ':80:' . $routed]);

			// roll self-signed certificate into check
			$curl->trustServerCertificate();

			$data = $curl->try_request(PHPMYADMIN_LOCATION);
			if (false === ($pos = strpos($data, "\r\n\r\n"))) {
				if (curl_errno($curl->ch) & (CURLE_SSL_CACERT|CURLE_SSL_PEER_CERTIFICATE)) {
					return error("SSL is not configured correctly yet on this server. See https://docs.apiscp.com/admin/SSL - error message: %s",
						curl_error($curl->ch));
				}
				return error("Internal subrequest failed. Check server setup. Error: %s", curl_error($curl->ch));
			}
			do {
				[$header, $body] = [substr($data, 0, $pos), substr($data, $pos + 4)];
				$data = $body;
				$pos = strpos($data, "\r\n\r\n");
			} while (0 === strpos($body, 'HTTP/'));

			$postArgs = array_merge($this->parseInput($data), [
				'pma_username' => $this->mysqlUsername,
				'pma_password' => $this->mysqlPassword
			]);
			if (preg_match('/\bphpMyAdmin=([^;]+);/', $header, $match)) {
				$cookie = trim($match[1]);
			}
			$curl->set_form_method('POST');
			$curl->change_option(CURLOPT_FOLLOWLOCATION, 1);
			$curl->change_option(CURLOPT_AUTOREFERER, 1);
			$curl->toggle_body(false);
			$post = implode('&', array_map(function ($v, $k) {
				return urlencode($k) . '=' . urlencode($v);
			}, $postArgs, array_keys($postArgs)));
			$curl->change_option(CURLOPT_POSTFIELDS, $post);
			$curl->change_option(CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			$data = $curl->try_request(rtrim(PHPMYADMIN_LOCATION, '/') . '/');

			$iv = $ivname = $token = $username = $passwd = $cookie = null;
			foreach (explode("\n", $data) as $line) {
				if (preg_match('/\b(pma_(?:mcrypt_iv|iv-1)|pmaAuth-1(?:_https?))=([^;]+);/', $line, $match)) {
					$iv = $match[2];
					$ivname = $match[1];

				} else if (preg_match('/\bpmaUser-1(?:_https?)=([^;]+);/', $line, $match)) {
					$username = $match[1];
				} else if (preg_match('/\bpmaPass-1(?:_https?)=([^;]+);?/', $line, $match)) {
					$passwd = trim($match[1]);
				} else if (preg_match('/\bphpMyAdmin(?:_https?)=([^;]+);/', $line, $match)) {
					$cookie = trim($match[1]);
				} else if (preg_match('/Location:.+token=([^&\s]+)|name="token" value="([^"]+)"/', $line, $match)) {
					$token = $match[1] ?: $match[2];
				}
			}

			if ($iv && $username && ($passwd || 0 === strpos($ivname, 'pmaAuth-1')) && $cookie) {
				$lang = "en";
				$this->phpMyAdminURL = PHPMYADMIN_LOCATION . '/dummyset.php?' . $ivname . '=' . $iv . '&u=' . $username . '&pwd=' . $passwd . '&c=' . $cookie .
					'&https=' . ($ivname === 'pmaAuth-1_https' ? '1' : '0'). '&t=' . $token . '&return=' .
					urlencode("index.php?db=" . $this->database . "&lang=$lang&convcharset=utf-8&collation_connection=utf8_general_ci&token=" . $token . "&phpMyAdmin=" . $cookie);
				$this->failed = false;
				\Util_HTTP::forwardNoProxy();
				header('Location: ' . $this->phpMyAdminURL, true, 302);
			}

		}

		protected function parseInput(string $input): array
		{
			$dom = new \DOMDocument();
			@$dom->loadHTML($input);
			$post = [];
			foreach ($dom->getElementsByTagName('input') as $tag) {
				$name = $tag->getAttribute('name');
				if (!$name) {
					continue;
				}
				$post[$name] = $tag->getAttribute('value');
			}

			return $post;
		}

		public function on_postback($params)
		{
			if (isset($params['username']) && $params['username']) {
				$this->set_mysql_username($params['username']);
			}
			Error_Reporter::suppress_php_error('mysqli_connect');
			if (!empty($params['reset-password'])) {
				$myuser = $this->get_mysql_username();
				foreach ($this->mysql_list_users() as $user => $hosts) {
					if ($user !== $myuser) {
						continue;
					}
					foreach (array_keys($hosts) as $host) {
						$this->mysql_edit_user($user, $host, ['password' => $params['password']]);
					}
					break;
				}
			}

			if (empty($params['password'])) {
				return;
			}
			try {
				mysqli_connect('localhost', $this->mysqlUsername, $params['password']);
				$this->set_mysql_password($params['password']);
				$this->mysqlPassword = $params['password'];
			} catch (\mysqli_sql_exception $e) {
				$this->mysqlPassword = null;
				return error("MySQL password submitted does not match record in system for login " . $this->mysqlUsername);
			}

			$this->setup_phpmyadmin_session();
		}

		public function phpMyAdminURL()
		{
			return $this->phpMyAdminURL;
		}

		public function password_exists()
		{
			return !is_null($this->mysqlPassword);
		}
	}
