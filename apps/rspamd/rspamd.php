<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\rspamd;

	use Opcenter\Crypto\NaiveCrypt;
	use Opcenter\Mail\Services\Rspamd;
	use Page_Container;

	class Page extends Page_Container
	{
		/**
		 * @var string|null
		 */
		protected $password;

		public function __construct()
		{
			parent::__construct();
			\Page_Renderer::show_full_help();
			$this->add_css('#ui-app { padding: 0 !important;};', 'internal');
			$this->password = $this->getPassword();
			if (!$this->password) {
				error("rspamd is not configured properly. Please restart %s and try again", PANEL_BRAND);
			}
		}

		public function _layout()
		{
			parent::_layout();
			if ($this->password) {
				$this->add_javascript('RSPAMD_PASSWORD = ' . json_encode($this->password) . ';', 'internal', false, true);
				$this->add_javascript('rspamd.js', 'external', false, true);
			}
		}

		public function getPassword(): ?string
		{
			return \Preferences::get(Rspamd::ADMIN_PREFKEY);
		}
	}