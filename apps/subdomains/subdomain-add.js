$(document).ready(function () {
	$('#submitBtn').ajaxWait();
	$('#browse_path').click(
		function () {
			var path = $('#subdomain_fs').filter(':not(.hinted)').val() || '/var/www';
			apnscp.explorer({
				selected: path, selectField: '#subdomain_fs', onSelect: function(file) {
					$('#selected_dir').text(file);
					$('#subdomain_fs').triggerHandler('change');
				}
			});
		});

	$('select.multiselect').multiSelect({
		noneSelected: "select domain(s)",
		selectAllText: "Global subdomain",
		oneOrMoreSelected: "% domains bound"
	});

	var originalValue = '';
	$('#subdomain_fs').focus(function () {
		originalValue = $(this).val();
	}).change(function(e) {
		if (originalValue !== $(this).val()) {
			$('#subdomain').unbind('change.suggest');
		}
	});

	$('#subdomain').bind('change.suggest', function () {
		var token = $.trim($(this).val()).replace(/^www\./i, "");
		if (!token) {
			return;
		}
		var suggested = '/var/www/' + token;
		var $selectedDomains = $(':input[name="domains[]"]:checked');
		if ($selectedDomains.length === 1) {
			suggested += '.' + $selectedDomains.val();
		}
		$('#subdomain_fs').val(suggested);
	});

	$('#fallthrough').change(function () {
		if ($(this).prop('checked')) {
			$('#subdomain').val("").prop('disabled', true);
		} else {
			$('#subdomain').prop('disabled', false).focus();
		}
		return true;
	});

	$('[data-toggle=tooltip]').tooltip();

	$('#addForm').submit(function () {
		// disabling input inhibits posting the form control...
		/*var $el = $('<input type="hidden" value="1" />').attr('name', $('#submitBtn').attr('name'));
		$('i', '#submitBtn').removeClass('ui-action-add').addClass('ui-ajax-indicator ui-ajax-loading').end().prop('disabled', true).append($el);
		return true;*/
	});

});
