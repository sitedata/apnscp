<button class="ml-auto ui-action-label ui-action ui-action-toolbox toolbox btn btn-secondary dropdown-toggle"
        aria-haspopup="true" aria-expanded="false" type="button" data-toggle="dropdown">
	Toolbox
</button>
<div class="dropdown-menu dropdown-menu-right" id="toolbox">
	@if (\count($Page->list_domains()) > 1)
		<button name="clone" class="btn btn-block dropdown-item" type="button" id="cloneSubdomains">
			<i class="ui-action ui-action-copy"></i>
			Clone Subdomains
		</button>
	@endif
</div>