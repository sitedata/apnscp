<h3>Scheduled Tasks</h3>
<div class="tasks">
	@includeWhen(!$jobs, 'partials.jobs.none-active')
	<div id="activeTasks" class="collapse @if ($status) show @endif" aria-describedby="">
		@includeWhen($jobs, 'partials.jobs.active')
	</div>
</div>
<hr/>

@include('partials.jobs.add')
