<h3>Billing Information</h3>
<?php
    if ($Page->get_payment_method() == 'credit'):

$country = $Page->get_country();
$state = $Page->get_state();
$renewal = $Page->renewalUri();
$hash = $Page->renewalHash();
if (!$hash):

?>

<form class="row" method="POST" action="/apps/changebilling.php">
	<p class="col-12">
		<span class="optional">*</span><span class="text-muted">denotes optional information.</span>
	</p>

	<fieldset class="form-group col-md-5">
		<label for="fname" id="fname">First Name</label>
		<input type="text" class="form-control" name="fname" value="<?=$Page->get_first_name()?>"/>
	</fieldset>

	<fieldset class="form-group offset-md-2 col-md-5">
		<label for="lname" id="lname">Last Name</label>
		<input type="text" class="form-control" name="lname" value="<?=$Page->get_last_name()?>"/>
	</fieldset>

	<fieldset class="form-group col-12">
		<label for="company" id="company">Company Name <span class="optional">*</span></label>
		<input type="text" class="form-control" name="company" value="<?=$Page->get_company()?>"/>
	</fieldset>

	<fieldset class="form-group col-12">
		<label for="address1" id="address1">Street Address</label>
		<input class="form-control" type="text" name="address1" value="<?=$Page->get_address(1)?>" maxlength="32"
		       size="32"/>
		<input class="form-control" type="text" placeholder="Line 2" name="address2" value="<?=$Page->get_address(2)?>"
		       maxlength="32" size="32"/>
	</fieldset>


	<fieldset class="form-group col-12">
		<label for="city" id="city">City</label>
		<input class="form-control" type="text" name="city" value="<?=$Page->get_city()?>"/>
	</fieldset>

	<fieldset class="form-group col-12">
		<label for="state" id="state">State/Territory/or Province</label>

		<select name="state1" class="form-control custom-select">
			<option value="">Select State/ Territory/ Province</option>
			<option value="AL"
			<?php if ($state == "AL") { print("SELECTED "); } ?>>Alabama</option>
			<option value="AK"
			<?php if ($state == "AK") { print("SELECTED "); } ?>>Alaska</option>
			<option value="AS"
			<?php if ($state == "AS") { print("SELECTED "); } ?>>American Samoa</option>

			<option value="AZ"
			<?php if ($state == "AZ") { print("SELECTED "); } ?>>Arizona</option>
			<option value="AR"
			<?php if ($state == "AR") { print("SELECTED "); } ?>>Arkansas</option>
			<option value="AA"
			<?php if ($state == "AA") { print("SELECTED "); } ?>>Armed Forces America</option>
			<option value="AE"
			<?php if ($state == "AE") { print("SELECTED "); } ?>>Armed Forces Other Areas</option>
			<option value="AP"
			<?php if ($state == "AP") { print("SELECTED "); } ?>>Armed Forces Pacific</option>
			<option value="CA"
			<?php if ($state == "CA") { print("SELECTED "); } ?>>California</option>

			<option value="CO"
			<?php if ($state == "CO") { print("SELECTED "); } ?>>Colorado</option>
			<option value="CT"
			<?php if ($state == "CT") { print("SELECTED "); } ?>>Connecticut</option>
			<option value="DE"
			<?php if ($state == "DE") { print("SELECTED "); } ?>>Delaware</option>
			<option value="DC"
			<?php if ($state == "DC") { print("SELECTED "); } ?>>District of Columbia</option>
			<option value="FM"
			<?php if ($state == "FM") { print("SELECTED "); } ?>>Federated States of Micronesia</option>
			<option value="FL"
			<?php if ($state == "FL") { print("SELECTED "); } ?>>Florida</option>

			<option value="GA"
			<?php if ($state == "GA") { print("SELECTED "); } ?>>Georgia</option>
			<option value="GU"
			<?php if ($state == "GU") { print("SELECTED "); } ?>>Guam</option>
			<option value="HI"
			<?php if ($state == "HI") { print("SELECTED "); } ?>>Hawaii</option>
			<option value="ID"
			<?php if ($state == "ID") { print("SELECTED "); } ?>>Idaho</option>
			<option value="IL"
			<?php if ($state == "IL") { print("SELECTED "); } ?>>Illinois</option>
			<option value="IN"
			<?php if ($state == "IN") { print("SELECTED "); } ?>>Indiana</option>

			<option value="IA"
			<?php if ($state == "IA") { print("SELECTED "); } ?>>Iowa</option>
			<option value="KS"
			<?php if ($state == "KS") { print("SELECTED "); } ?>>Kansas</option>
			<option value="KY"
			<?php if ($state == "KY") { print("SELECTED "); } ?>>Kentucky</option>
			<option value="LA"
			<?php if ($state == "LA") { print("SELECTED "); } ?>>Louisiana</option>
			<option value="ME"
			<?php if ($state == "ME") { print("SELECTED "); } ?>>Maine</option>
			<option value="MH"
			<?php if ($state == "MH") { print("SELECTED "); } ?>>Marshall Islands</option>

			<option value="MD"
			<?php if ($state == "MD") { print("SELECTED "); } ?>>Maryland</option>
			<option value="MA"
			<?php if ($state == "MA") { print("SELECTED "); } ?>>Massachusetts</option>
			<option value="MI"
			<?php if ($state == "MI") { print("SELECTED "); } ?>>Michigan</option>
			<option value="MN"
			<?php if ($state == "MN") { print("SELECTED "); } ?>>Minnesota</option>
			<option value="MS"
			<?php if ($state == "MS") { print("SELECTED "); } ?>>Mississippi</option>
			<option value="MO"
			<?php if ($state == "MO") { print("SELECTED "); } ?>>Missouri</option>

			<option value="MT"
			<?php if ($state == "MT") { print("SELECTED "); } ?>>Montana</option>
			<option value="NE"
			<?php if ($state == "NE") { print("SELECTED "); } ?>>Nebraska</option>
			<option value="NV"
			<?php if ($state == "NV") { print("SELECTED "); } ?>>Nevada</option>
			<option value="NH"
			<?php if ($state == "NH") { print("SELECTED "); } ?>>New Hampshire</option>
			<option value="NJ"
			<?php if ($state == "NJ") { print("SELECTED "); } ?>>New Jersey</option>
			<option value="NM"
			<?php if ($state == "NM") { print("SELECTED "); } ?>>New Mexico</option>

			<option value="NY"
			<?php if ($state == "NY") { print("SELECTED "); } ?>>New York</option>
			<option value="NC"
			<?php if ($state == "NC") { print("SELECTED "); } ?>>North Carolina</option>
			<option value="ND"
			<?php if ($state == "ND") { print("SELECTED "); } ?>>North Dakota</option>
			<option value="MP"
			<?php if ($state == "MP") { print("SELECTED "); } ?>>Northern Mariana Islands</option>
			<option value="OH"
			<?php if ($state == "OH") { print("SELECTED "); } ?>>Ohio</option>
			<option value="OK"
			<?php if ($state == "OK") { print("SELECTED "); } ?>>Oklahoma</option>

			<option value="OR"
			<?php if ($state == "OR") { print("SELECTED "); } ?>>Oregon</option>
			<option value="PW"
			<?php if ($state == "PW") { print("SELECTED "); } ?>>Palau</option>
			<option value="PA"
			<?php if ($state == "PA") { print("SELECTED "); } ?>>Pennsylvania</option>
			<option value="PR"
			<?php if ($state == "PR") { print("SELECTED "); } ?>>Puerto Rico</option>
			<option value="RI"
			<?php if ($state == "RI") { print("SELECTED "); } ?>>Rhode Island</option>
			<option value="SC"
			<?php if ($state == "SC") { print("SELECTED "); } ?>>South Carolina</option>

			<option value="SD"
			<?php if ($state == "SD") { print("SELECTED "); } ?>>South Dakota</option>
			<option value="TN"
			<?php if ($state == "TN") { print("SELECTED "); } ?>>Tennessee</option>
			<option value="TX"
			<?php if ($state == "TX") { print("SELECTED "); } ?>>Texas</option>
			<option value="VI"
			<?php if ($state == "VI") { print("SELECTED "); } ?>>U.S. Virgin Islands</option>
			<option value="UT"
			<?php if ($state == "UT") { print("SELECTED "); } ?>>Utah</option>
			<option value="VT"
			<?php if ($state == "VT") { print("SELECTED "); } ?>>Vermont</option>

			<option value="VA"
			<?php if ($state == "VA") { print("SELECTED "); } ?>>Virginia</option>
			<option value="WA"
			<?php if ($state == "WA") { print("SELECTED "); } ?>>Washington</option>
			<option value="WV"
			<?php if ($state == "WV") { print("SELECTED "); } ?>>West Virginia</option>
			<option value="WI"
			<?php if ($state == "WI") { print("SELECTED "); } ?>>Wisconsin</option>
			<option value="WY"
			<?php if ($state == "WY") { print("SELECTED "); } ?>>Wyoming</option>
		</select>

		<input class="form-control" type="text" placeholder="State (non-US)" name="state2"
		       value="<?=$Page->get_country() != 'us' ? $state : '' ?>"/>
	</fieldset>

	<fieldset class="form-group  col-4">
		<label for="zip" id="zip">Zip/Postal Code</label><br/>
		<input class="form-control" type="text" id="zip" name="zip" value="<?=$Page->get_zip()?>"/>
	</fieldset>

	<fieldset class="form-group  col-12">
		<label for="country" id="country">Country</label>

		<select class="form-control custom-select" name="country" id="country">
			<option value=""
			<?php if (!$country) { print("SELECTED "); } ?>>Select a Country</option>
			<option value="af"
			<?php if ($country == "af") { print("SELECTED "); } ?>>Afghanistan</option>

			<option value="al"
			<?php if ($country == "al") { print("SELECTED "); } ?>>Albania</option>
			<option value="dz"
			<?php if ($country == "dz") { print("SELECTED "); } ?>>Algeria</option>
			<option value="as"
			<?php if ($country == "as") { print("SELECTED "); } ?>>American Samoa</option>
			<option value="ad"
			<?php if ($country == "ad") { print("SELECTED "); } ?>>Andorra</option>
			<option value="ao"
			<?php if ($country == "ao") { print("SELECTED "); } ?>>Angola</option>
			<option value="ai"
			<?php if ($country == "ai") { print("SELECTED "); } ?>>Anguilla</option>

			<option value="aq"
			<?php if ($country == "aq") { print("SELECTED "); } ?>>Antarctica</option>
			<option value="ag"
			<?php if ($country == "ag") { print("SELECTED "); } ?>>Antigua and Barbuda</option>
			<option value="ar"
			<?php if ($country == "ar") { print("SELECTED "); } ?>>Argentina</option>
			<option value="am"
			<?php if ($country == "am") { print("SELECTED "); } ?>>Armenia</option>
			<option value="aw"
			<?php if ($country == "aw") { print("SELECTED "); } ?>>Aruba</option>
			<option value="ac"
			<?php if ($country == "ac") { print("SELECTED "); } ?>>Ascension Island</option>

			<option value="au"
			<?php if ($country == "au") { print("SELECTED "); } ?>>Australia</option>
			<option value="at"
			<?php if ($country == "at") { print("SELECTED "); } ?>>Austria</option>
			<option value="az"
			<?php if ($country == "az") { print("SELECTED "); } ?>>Azerbaijan</option>
			<option value="bs"
			<?php if ($country == "bs") { print("SELECTED "); } ?>>Bahamas</option>
			<option value="bh"
			<?php if ($country == "bh") { print("SELECTED "); } ?>>Bahrain</option>
			<option value="bd"
			<?php if ($country == "bd") { print("SELECTED "); } ?>>Bangladesh</option>

			<option value="bb"
			<?php if ($country == "bb") { print("SELECTED "); } ?>>Barbados</option>
			<option value="by"
			<?php if ($country == "by") { print("SELECTED "); } ?>>Belarus</option>
			<option value="be"
			<?php if ($country == "be") { print("SELECTED "); } ?>>Belgium</option>
			<option value="bz"
			<?php if ($country == "bz") { print("SELECTED "); } ?>>Belize</option>
			<option value="bj"
			<?php if ($country == "bj") { print("SELECTED "); } ?>>Benin</option>
			<option value="bm"
			<?php if ($country == "bm") { print("SELECTED "); } ?>>Bermuda</option>

			<option value="bt"
			<?php if ($country == "bt") { print("SELECTED "); } ?>>Bhutan</option>
			<option value="bo"
			<?php if ($country == "bo") { print("SELECTED "); } ?>>Bolivia</option>
			<option value="ba"
			<?php if ($country == "ba") { print("SELECTED "); } ?>>Bosnia and Herzegovina</option>
			<option value="bw"
			<?php if ($country == "bw") { print("SELECTED "); } ?>>Botswana</option>
			<option value="bv"
			<?php if ($country == "bv") { print("SELECTED "); } ?>>Bouvet Island</option>
			<option value="br"
			<?php if ($country == "br") { print("SELECTED "); } ?>>Brazil</option>

			<option value="io"
			<?php if ($country == "io") { print("SELECTED "); } ?>>British Indian Ocean Territory</option>
			<option value="bn"
			<?php if ($country == "bn") { print("SELECTED "); } ?>>Brunei Darussalam</option>
			<option value="bg"
			<?php if ($country == "bg") { print("SELECTED "); } ?>>Bulgaria</option>
			<option value="bf"
			<?php if ($country == "bf") { print("SELECTED "); } ?>>Burkina Faso</option>
			<option value="bi"
			<?php if ($country == "bi") { print("SELECTED "); } ?>>Burundi</option>
			<option value="kh"
			<?php if ($country == "kh") { print("SELECTED "); } ?>>Cambodia</option>

			<option value="cm"
			<?php if ($country == "cm") { print("SELECTED "); } ?>>Cameroon</option>
			<option value="ca"
			<?php if ($country == "ca") { print("SELECTED "); } ?>>Canada</option>
			<option value="cv"
			<?php if ($country == "cv") { print("SELECTED "); } ?>>Cape Verde Islands</option>
			<option value="ky"
			<?php if ($country == "ky") { print("SELECTED "); } ?>>Cayman Islands</option>
			<option value="cf"
			<?php if ($country == "cf") { print("SELECTED "); } ?>>Central African Republic</option>
			<option value="td"
			<?php if ($country == "td") { print("SELECTED "); } ?>>Chad</option>

			<option value="cl"
			<?php if ($country == "cl") { print("SELECTED "); } ?>>Chile</option>
			<option value="cn"
			<?php if ($country == "cn") { print("SELECTED "); } ?>>China</option>
			<option value="cx"
			<?php if ($country == "cx") { print("SELECTED "); } ?>>Christmas Island</option>
			<option value="cc"
			<?php if ($country == "cc") { print("SELECTED "); } ?>>Cocos (Keeling) Islands</option>
			<option value="co"
			<?php if ($country == "co") { print("SELECTED "); } ?>>Colombia</option>
			<option value="km"
			<?php if ($country == "km") { print("SELECTED "); } ?>>Comoros</option>

			<option value="cd"
			<?php if ($country == "cd") { print("SELECTED "); } ?>>Congo, Democratic Republic of</option>
			<option value="cg"
			<?php if ($country == "cg") { print("SELECTED "); } ?>>Congo, Republic of</option>
			<option value="ck"
			<?php if ($country == "ck") { print("SELECTED "); } ?>>Cook Islands</option>
			<option value="cr"
			<?php if ($country == "cr") { print("SELECTED "); } ?>>Costa Rica</option>
			<option value="ci"
			<?php if ($country == "ci") { print("SELECTED "); } ?>>Cote d'Ivoire</option>
			<option value="hr"
			<?php if ($country == "hr") { print("SELECTED "); } ?>>Croatia/Hrvatska</option>

			<option value="cu"
			<?php if ($country == "cu") { print("SELECTED "); } ?>>Cuba</option>
			<option value="cy"
			<?php if ($country == "cy") { print("SELECTED "); } ?>>Cyprus</option>
			<option value="cz"
			<?php if ($country == "cz") { print("SELECTED "); } ?>>Czech Republic</option>
			<option value="dk"
			<?php if ($country == "dk") { print("SELECTED "); } ?>>Denmark</option>
			<option value="dj"
			<?php if ($country == "dj") { print("SELECTED "); } ?>>Djibouti</option>
			<option value="dm"
			<?php if ($country == "dm") { print("SELECTED "); } ?>>Dominica</option>

			<option value="do"
			<?php if ($country == "do") { print("SELECTED "); } ?>>Dominican Republic</option>
			<option value="tp"
			<?php if ($country == "tp") { print("SELECTED "); } ?>>East Timor</option>
			<option value="ec"
			<?php if ($country == "ec") { print("SELECTED "); } ?>>Ecuador</option>
			<option value="eg"
			<?php if ($country == "eg") { print("SELECTED "); } ?>>Egypt</option>
			<option value="sv"
			<?php if ($country == "sv") { print("SELECTED "); } ?>>El Salvador</option>
			<option value="gq"
			<?php if ($country == "gq") { print("SELECTED "); } ?>>Equatorial Guinea</option>

			<option value="er"
			<?php if ($country == "er") { print("SELECTED "); } ?>>Eritrea</option>
			<option value="ee"
			<?php if ($country == "ee") { print("SELECTED "); } ?>>Estonia</option>
			<option value="et"
			<?php if ($country == "et") { print("SELECTED "); } ?>>Ethiopia</option>
			<option value="fk"
			<?php if ($country == "fk") { print("SELECTED "); } ?>>Falkland Islands</option>
			<option value="fo"
			<?php if ($country == "fo") { print("SELECTED "); } ?>>Faroe Islands</option>
			<option value="fj"
			<?php if ($country == "fj") { print("SELECTED "); } ?>>Fiji</option>

			<option value="fi"
			<?php if ($country == "fi") { print("SELECTED "); } ?>>Finland</option>
			<option value="fr"
			<?php if ($country == "fr") { print("SELECTED "); } ?>>France</option>
			<option value="gf"
			<?php if ($country == "gf") { print("SELECTED "); } ?>>French Guiana</option>
			<option value="pf"
			<?php if ($country == "pf") { print("SELECTED "); } ?>>French Polynesia</option>
			<option value="tf"
			<?php if ($country == "tf") { print("SELECTED "); } ?>>French Southern Territories</option>
			<option value="ga"
			<?php if ($country == "ga") { print("SELECTED "); } ?>>Gabon</option>

			<option value="gm"
			<?php if ($country == "gm") { print("SELECTED "); } ?>>Gambia</option>
			<option value="ge"
			<?php if ($country == "ge") { print("SELECTED "); } ?>>Georgia</option>
			<option value="de"
			<?php if ($country == "de") { print("SELECTED "); } ?>>Germany</option>
			<option value="gh"
			<?php if ($country == "gh") { print("SELECTED "); } ?>>Ghana</option>
			<option value="gi"
			<?php if ($country == "gi") { print("SELECTED "); } ?>>Gibraltar</option>
			<option value="gr"
			<?php if ($country == "gr") { print("SELECTED "); } ?>>Greece</option>

			<option value="gl"
			<?php if ($country == "gl") { print("SELECTED "); } ?>>Greenland</option>
			<option value="gd"
			<?php if ($country == "gd") { print("SELECTED "); } ?>>Grenada</option>
			<option value="gp"
			<?php if ($country == "gp") { print("SELECTED "); } ?>>Guadeloupe</option>
			<option value="gu"
			<?php if ($country == "gu") { print("SELECTED "); } ?>>Guam</option>
			<option value="gt"
			<?php if ($country == "gt") { print("SELECTED "); } ?>>Guatemala</option>
			<option value="gg"
			<?php if ($country == "gg") { print("SELECTED "); } ?>>Guernsey</option>

			<option value="gn"
			<?php if ($country == "gn") { print("SELECTED "); } ?>>Guinea</option>
			<option value="gw"
			<?php if ($country == "gw") { print("SELECTED "); } ?>>Guinea-Bissau</option>
			<option value="gy"
			<?php if ($country == "gy") { print("SELECTED "); } ?>>Guyana</option>
			<option value="ht"
			<?php if ($country == "ht") { print("SELECTED "); } ?>>Haiti</option>
			<option value="hm"
			<?php if ($country == "hm") { print("SELECTED "); } ?>>Heard and McDonald Islands</option>
			<option value="hn"
			<?php if ($country == "hn") { print("SELECTED "); } ?>>Honduras</option>

			<option value="hk"
			<?php if ($country == "hk") { print("SELECTED "); } ?>>Hong Kong</option>
			<option value="hu"
			<?php if ($country == "hu") { print("SELECTED "); } ?>>Hungary</option>
			<option value="is"
			<?php if ($country == "is") { print("SELECTED "); } ?>>Iceland</option>
			<option value="in"
			<?php if ($country == "in") { print("SELECTED "); } ?>>India</option>
			<option value="id"
			<?php if ($country == "id") { print("SELECTED "); } ?>>Indonesia</option>
			<option value="ir"
			<?php if ($country == "ir") { print("SELECTED "); } ?>>Iran</option>

			<option value="iq"
			<?php if ($country == "iq") { print("SELECTED "); } ?>>Iraq</option>
			<option value="ie"
			<?php if ($country == "ie") { print("SELECTED "); } ?>>Ireland</option>
			<option value="im"
			<?php if ($country == "im") { print("SELECTED "); } ?>>Isle of Man</option>
			<option value="il"
			<?php if ($country == "il") { print("SELECTED "); } ?>>Israel</option>
			<option value="it"
			<?php if ($country == "it") { print("SELECTED "); } ?>>Italy</option>
			<option value="jm"
			<?php if ($country == "jm") { print("SELECTED "); } ?>>Jamaica</option>

			<option value="jp"
			<?php if ($country == "jp") { print("SELECTED "); } ?>>Japan</option>
			<option value="je"
			<?php if ($country == "je") { print("SELECTED "); } ?>>Jersey</option>
			<option value="jo"
			<?php if ($country == "jo") { print("SELECTED "); } ?>>Jordan</option>
			<option value="kz"
			<?php if ($country == "kz") { print("SELECTED "); } ?>>Kazakhstan</option>
			<option value="ke"
			<?php if ($country == "ke") { print("SELECTED "); } ?>>Kenya</option>
			<option value="ki"
			<?php if ($country == "ki") { print("SELECTED "); } ?>>Kiribati</option>

			<option value="kp"
			<?php if ($country == "kp") { print("SELECTED "); } ?>>Korea, Democratic People's Republic of</option>
			<option value="kr"
			<?php if ($country == "kr") { print("SELECTED "); } ?>>Korea, Republic of</option>
			<option value="kw"
			<?php if ($country == "kw") { print("SELECTED "); } ?>>Kuwait</option>
			<option value="kg"
			<?php if ($country == "kg") { print("SELECTED "); } ?>>Kyrgyzstan</option>
			<option value="la"
			<?php if ($country == "la") { print("SELECTED "); } ?>>Laos</option>
			<option value="lv"
			<?php if ($country == "lv") { print("SELECTED "); } ?>>Latvia</option>

			<option value="lb"
			<?php if ($country == "lb") { print("SELECTED "); } ?>>Lebanon</option>
			<option value="ls"
			<?php if ($country == "ls") { print("SELECTED "); } ?>>Lesotho</option>
			<option value="lr"
			<?php if ($country == "lr") { print("SELECTED "); } ?>>Liberia</option>
			<option value="ly"
			<?php if ($country == "ly") { print("SELECTED "); } ?>>Libya</option>
			<option value="li"
			<?php if ($country == "li") { print("SELECTED "); } ?>>Liechtenstein</option>
			<option value="lt"
			<?php if ($country == "lt") { print("SELECTED "); } ?>>Lithuania</option>

			<option value="lu"
			<?php if ($country == "lu") { print("SELECTED "); } ?>>Luxembourg</option>
			<option value="mo"
			<?php if ($country == "mo") { print("SELECTED "); } ?>>Macau</option>
			<option value="mk"
			<?php if ($country == "mk") { print("SELECTED "); } ?>>Macedonia, Former Yugoslav Republic</option>
			<option value="mg"
			<?php if ($country == "mg") { print("SELECTED "); } ?>>Madagascar</option>
			<option value="mw"
			<?php if ($country == "mw") { print("SELECTED "); } ?>>Malawi</option>
			<option value="my"
			<?php if ($country == "my") { print("SELECTED "); } ?>>Malaysia</option>

			<option value="mv"
			<?php if ($country == "mv") { print("SELECTED "); } ?>>Maldives</option>
			<option value="ml"
			<?php if ($country == "ml") { print("SELECTED "); } ?>>Mali</option>
			<option value="mt"
			<?php if ($country == "mt") { print("SELECTED "); } ?>>Malta</option>
			<option value="mh"
			<?php if ($country == "mh") { print("SELECTED "); } ?>>Marshall Islands</option>
			<option value="mq"
			<?php if ($country == "mq") { print("SELECTED "); } ?>>Martinique</option>
			<option value="mr"
			<?php if ($country == "mr") { print("SELECTED "); } ?>>Mauritania</option>

			<option value="mu"
			<?php if ($country == "mu") { print("SELECTED "); } ?>>Mauritius</option>
			<option value="yt"
			<?php if ($country == "yt") { print("SELECTED "); } ?>>Mayotte Island</option>
			<option value="mx"
			<?php if ($country == "mx") { print("SELECTED "); } ?>>Mexico</option>
			<option value="fm"
			<?php if ($country == "fm") { print("SELECTED "); } ?>>Micronesia</option>
			<option value="md"
			<?php if ($country == "md") { print("SELECTED "); } ?>>Moldova</option>
			<option value="mc"
			<?php if ($country == "mc") { print("SELECTED "); } ?>>Monaco</option>

			<option value="mn"
			<?php if ($country == "mn") { print("SELECTED "); } ?>>Mongolia</option>
			<option value="me"
			<?php if ($country == "me") { print("SELECTED "); } ?>>Montenegro</option>
			<option value="ms"
			<?php if ($country == "ms") { print("SELECTED "); } ?>>Montserrat</option>
			<option value="ma"
			<?php if ($country == "ma") { print("SELECTED "); } ?>>Morocco</option>
			<option value="mz"
			<?php if ($country == "mz") { print("SELECTED "); } ?>>Mozambique</option>
			<option value="mm"
			<?php if ($country == "mm") { print("SELECTED "); } ?>>Myanmar</option>

			<option value="na"
			<?php if ($country == "na") { print("SELECTED "); } ?>>Namibia</option>
			<option value="nr"
			<?php if ($country == "nr") { print("SELECTED "); } ?>>Nauru</option>
			<option value="np"
			<?php if ($country == "np") { print("SELECTED "); } ?>>Nepal</option>
			<option value="nl"
			<?php if ($country == "nl") { print("SELECTED "); } ?>>Netherlands</option>
			<option value="an"
			<?php if ($country == "an") { print("SELECTED "); } ?>>Netherlands Antilles</option>
			<option value="nc"
			<?php if ($country == "nc") { print("SELECTED "); } ?>>New Caledonia</option>

			<option value="nz"
			<?php if ($country == "nz") { print("SELECTED "); } ?>>New Zealand</option>
			<option value="ni"
			<?php if ($country == "ni") { print("SELECTED "); } ?>>Nicaragua</option>
			<option value="ne"
			<?php if ($country == "ne") { print("SELECTED "); } ?>>Niger</option>
			<option value="ng"
			<?php if ($country == "ng") { print("SELECTED "); } ?>>Nigeria</option>
			<option value="nu"
			<?php if ($country == "nu") { print("SELECTED "); } ?>>Niue</option>
			<option value="nf"
			<?php if ($country == "nf") { print("SELECTED "); } ?>>Norfolk Island</option>

			<option value="mp"
			<?php if ($country == "mp") { print("SELECTED "); } ?>>Northern Mariana Islands</option>
			<option value="no"
			<?php if ($country == "no") { print("SELECTED "); } ?>>Norway</option>
			<option value="om"
			<?php if ($country == "om") { print("SELECTED "); } ?>>Oman</option>
			<option value="pk"
			<?php if ($country == "pk") { print("SELECTED "); } ?>>Pakistan</option>
			<option value="pw"
			<?php if ($country == "pw") { print("SELECTED "); } ?>>Palau</option>
			<option value="ps"
			<?php if ($country == "ps") { print("SELECTED "); } ?>>Palestinian Territories</option>

			<option value="pa"
			<?php if ($country == "pa") { print("SELECTED "); } ?>>Panama</option>
			<option value="pg"
			<?php if ($country == "pg") { print("SELECTED "); } ?>>Papua New Guinea</option>
			<option value="py"
			<?php if ($country == "py") { print("SELECTED "); } ?>>Paraguay</option>
			<option value="pe"
			<?php if ($country == "pe") { print("SELECTED "); } ?>>Peru</option>
			<option value="ph"
			<?php if ($country == "ph") { print("SELECTED "); } ?>>Philippines</option>
			<option value="pn"
			<?php if ($country == "pn") { print("SELECTED "); } ?>>Pitcairn Island</option>

			<option value="pl"
			<?php if ($country == "pl") { print("SELECTED "); } ?>>Poland</option>
			<option value="pt"
			<?php if ($country == "pt") { print("SELECTED "); } ?>>Portugal</option>
			<option value="pr"
			<?php if ($country == "pr") { print("SELECTED "); } ?>>Puerto Rico</option>
			<option value="qa"
			<?php if ($country == "qa") { print("SELECTED "); } ?>>Qatar</option>
			<option value="re"
			<?php if ($country == "re") { print("SELECTED "); } ?>>Reunion Island</option>
			<option value="ro"
			<?php if ($country == "ro") { print("SELECTED "); } ?>>Romania</option>

			<option value="ru"
			<?php if ($country == "ru") { print("SELECTED "); } ?>>Russian Federation</option>
			<option value="rw"
			<?php if ($country == "rw") { print("SELECTED "); } ?>>Rwanda</option>
			<option value="sh"
			<?php if ($country == "sh") { print("SELECTED "); } ?>>Saint Helena</option>
			<option value="kn"
			<?php if ($country == "kn") { print("SELECTED "); } ?>>Saint Kitts and Nevis</option>
			<option value="lc"
			<?php if ($country == "lc") { print("SELECTED "); } ?>>Saint Lucia</option>
			<option value="pm"
			<?php if ($country == "pm") { print("SELECTED "); } ?>>Saint Pierre and Miquelon</option>

			<option value="vc"
			<?php if ($country == "vc") { print("SELECTED "); } ?>>Saint Vincent and the Grenadines</option>
			<option value="sm"
			<?php if ($country == "sm") { print("SELECTED "); } ?>>San Marino</option>
			<option value="st"
			<?php if ($country == "st") { print("SELECTED "); } ?>>Sao Tome and Principe</option>
			<option value="sa"
			<?php if ($country == "sa") { print("SELECTED "); } ?>>Saudi Arabia</option>
			<option value="sn"
			<?php if ($country == "sn") { print("SELECTED "); } ?>>Senegal</option>
			<option value="rs"
			<?php if ($country == "rs") { print("SELECTED "); } ?>>Serbia</option>

			<option value="sc"
			<?php if ($country == "sc") { print("SELECTED "); } ?>>Seychelles</option>
			<option value="sl"
			<?php if ($country == "sl") { print("SELECTED "); } ?>>Sierra Leone</option>
			<option value="sg"
			<?php if ($country == "sg") { print("SELECTED "); } ?>>Singapore</option>
			<option value="sk"
			<?php if ($country == "sk") { print("SELECTED "); } ?>>Slovak Republic</option>
			<option value="si"
			<?php if ($country == "si") { print("SELECTED "); } ?>>Slovenia</option>
			<option value="sb"
			<?php if ($country == "sb") { print("SELECTED "); } ?>>Solomon Islands</option>

			<option value="so"
			<?php if ($country == "so") { print("SELECTED "); } ?>>Somalia</option>
			<option value="za"
			<?php if ($country == "za") { print("SELECTED "); } ?>>South Africa</option>
			<option value="gs"
			<?php if ($country == "gs") { print("SELECTED "); } ?>>South Georgia and South Sandwich Islands</option>
			<option value="es"
			<?php if ($country == "es") { print("SELECTED "); } ?>>Spain</option>
			<option value="lk"
			<?php if ($country == "lk") { print("SELECTED "); } ?>>Sri Lanka</option>
			<option value="sd"
			<?php if ($country == "sd") { print("SELECTED "); } ?>>Sudan</option>

			<option value="sr"
			<?php if ($country == "sr") { print("SELECTED "); } ?>>Suriname</option>
			<option value="sj"
			<?php if ($country == "sj") { print("SELECTED "); } ?>>Svalbard and Jan Mayen Islands</option>
			<option value="sz"
			<?php if ($country == "sz") { print("SELECTED "); } ?>>Swaziland</option>
			<option value="se"
			<?php if ($country == "se") { print("SELECTED "); } ?>>Sweden</option>
			<option value="ch"
			<?php if ($country == "ch") { print("SELECTED "); } ?>>Switzerland</option>
			<option value="sy"
			<?php if ($country == "sy") { print("SELECTED "); } ?>>Syria</option>

			<option value="tw"
			<?php if ($country == "tw") { print("SELECTED "); } ?>>Taiwan</option>
			<option value="tj"
			<?php if ($country == "tj") { print("SELECTED "); } ?>>Tajikistan</option>
			<option value="tz"
			<?php if ($country == "tz") { print("SELECTED "); } ?>>Tanzania</option>
			<option value="th"
			<?php if ($country == "th") { print("SELECTED "); } ?>>Thailand</option>
			<option value="tl"
			<?php if ($country == "tl") { print("SELECTED "); } ?>>Timor-Leste</option>
			<option value="tg"
			<?php if ($country == "tg") { print("SELECTED "); } ?>>Togo</option>

			<option value="tk"
			<?php if ($country == "tk") { print("SELECTED "); } ?>>Tokelau</option>
			<option value="to"
			<?php if ($country == "to") { print("SELECTED "); } ?>>Tonga Islands</option>
			<option value="tt"
			<?php if ($country == "tt") { print("SELECTED "); } ?>>Trinidad and Tobago</option>
			<option value="tn"
			<?php if ($country == "tn") { print("SELECTED "); } ?>>Tunisia</option>
			<option value="tr"
			<?php if ($country == "tr") { print("SELECTED "); } ?>>Turkey</option>
			<option value="tm"
			<?php if ($country == "tm") { print("SELECTED "); } ?>>Turkmenistan</option>

			<option value="tc"
			<?php if ($country == "tc") { print("SELECTED "); } ?>>Turks and Caicos Islands</option>
			<option value="tv"
			<?php if ($country == "tv") { print("SELECTED "); } ?>>Tuvalu</option>
			<option value="ug"
			<?php if ($country == "ug") { print("SELECTED "); } ?>>Uganda</option>
			<option value="ua"
			<?php if ($country == "ua") { print("SELECTED "); } ?>>Ukraine</option>
			<option value="ae"
			<?php if ($country == "ae") { print("SELECTED "); } ?>>United Arab Emirates</option>
			<option value="uk"
			<?php if ($country == "uk") { print("SELECTED "); } ?>>United Kingdom</option>

			<option value="us"
			<?php if ($country == "us") { print("SELECTED "); } ?>>United States</option>
			<option value="uy"
			<?php if ($country == "uy") { print("SELECTED "); } ?>>Uruguay</option>
			<option value="um"
			<?php if ($country == "um") { print("SELECTED "); } ?>>US Minor Outlying Islands</option>
			<option value="uz"
			<?php if ($country == "uz") { print("SELECTED "); } ?>>Uzbekistan</option>
			<option value="vu"
			<?php if ($country == "vu") { print("SELECTED "); } ?>>Vanuatu</option>
			<option value="va"
			<?php if ($country == "va") { print("SELECTED "); } ?>>Vatican City</option>

			<option value="ve"
			<?php if ($country == "ve") { print("SELECTED "); } ?>>Venezuela</option>
			<option value="vn"
			<?php if ($country == "vn") { print("SELECTED "); } ?>>Vietnam</option>
			<option value="vg"
			<?php if ($country == "vg") { print("SELECTED "); } ?>>Virgin Islands (British)</option>
			<option value="vi"
			<?php if ($country == "vi") { print("SELECTED "); } ?>>Virgin Islands (USA)</option>
			<option value="wf"
			<?php if ($country == "wf") { print("SELECTED "); } ?>>Wallis and Futuna Islands</option>
			<option value="eh"
			<?php if ($country == "eh") { print("SELECTED "); } ?>>Western Sahara</option>

			<option value="ws"
			<?php if ($country == "ws") { print("SELECTED "); } ?>>Western Samoa</option>
			<option value="ye"
			<?php if ($country == "ye") { print("SELECTED "); } ?>>Yemen</option>
			<option value="zm"
			<?php if ($country == "zm") { print("SELECTED "); } ?>>Zambia</option>
			<option value="zw"
			<?php if ($country == "zw") { print("SELECTED "); } ?>>Zimbabwe</option>
		</select>
	</fieldset>

	<fieldset class="form-group  col-4">
		<label for="phone" id="phone">Phone Number<sup>&dagger;</sup></label>
		<input class="form-control" type="text" name="phone" value="<?=$Page->get_phone()?>" id="phone"/>
	</fieldset>

	<fieldset class="form-group  col-12">
		<label for="email" id="email">E-mail Address<sup>&Dagger;</sup></label>
		<input class="form-control" type="text" name="email" size="30" value="<?=$Page->get_email()?>" id="email"/>
	</fieldset>

	<p class="clearfix"></p>

	<h4 class="col-12">
		<i class="fa fa-credit-card"></i>
		Card Information
	</h4>
	<fieldset id="card-panel" class="col-12 mb-1 py-2">
		<fieldset class="checkbox form-group">
			<label id="label-cc_delete" rel="cc_delete" data-toggle="tooltip"
			       title="Status changes will take 24 hours to update within the control panel." for="cc_delete">
				<input type="checkbox" name="cc_delete" id="cc_delete" value=""/>
				Delete credit card/halt recurring billing
			</label>
		</fieldset>

		<fieldset class="form-group">
			<label for="cc_type" id="cc_type">Card Type</label>

			<select class="form-control custom-select" name="cc_type" id="cc_type">
				<option value="">Please Choose</option>
				<option
				<?=$Page->get_card_type() == 'visa' ? 'SELECTED' : ''?> value="visa">Visa</option>
				<option
				<?=$Page->get_card_type() == 'mc' ? 'SELECTED' : ''?> value="mc">MasterCard</option>
				<option
				<?=$Page->get_card_type() == 'amex' ? 'SELECTED' : ''?> value="amex">American Express</option>
				<option
				<?=$Page->get_card_type() == 'discover' ? 'SELECTED' : ''?> value="discover">Discover</option>
			</select>
		</fieldset>

		<fieldset class="form-group">
			<label for="cc_number">Credit Card Number</label>
			<input type="text" class="form-control" autocomplete="off" maxlength="16" size="22" name="cc_number"
			       value="<?=$Page->get_card_number()?>" id="cc_number"/>
		</fieldset>

		<div class="form-group row">
			<div class="col-md-3 col-6">
				<label>
					Exp Month
					<select class="form-control custom-select" name="exp_month">
						<option
						<?=$Page->get_card_exp('month') === 01 ? 'SELECTED' : ''?> value="01">1</option>
						<option
						<?=$Page->get_card_exp('month') === 02 ? 'SELECTED' : ''?> value="02">2</option>
						<option
						<?=$Page->get_card_exp('month') === 03 ? 'SELECTED' : ''?> value="03">3</option>
						<option
						<?=$Page->get_card_exp('month') === 04 ? 'SELECTED' : ''?> value="04">4</option>
						<option
						<?=$Page->get_card_exp('month') === 05 ? 'SELECTED' : ''?> value="05">5</option>
						<option
						<?=$Page->get_card_exp('month') === 06 ? 'SELECTED' : ''?> value="06">6</option>
						<option
						<?=$Page->get_card_exp('month') === 07 ? 'SELECTED' : ''?> value="07">7</option>
						<option
						<?=$Page->get_card_exp('month') === 8 ? 'SELECTED' : ''?> value="08">8</option>
						<option
						<?=$Page->get_card_exp('month') === 9 ? 'SELECTED' : ''?> value="09">9</option>
						<option
						<?=$Page->get_card_exp('month') === 10 ? 'SELECTED' : ''?> value="10">10</option>
						<option
						<?=$Page->get_card_exp('month') === 11 ? 'SELECTED' : ''?> value="11">11</option>
						<option
						<?=$Page->get_card_exp('month') === 12 ? 'SELECTED' : ''?> value="12">12</option>
					</select>
				</label>
			</div>

			<div class="col-md-3 col-6">
				<label>
					Exp Year
					<select class="form-control custom-select" name="exp_year">
						<?php for ($i = date('y') ; $i < date('y')+10; $i++) {
                                printf('<option %svalue="%s">%s</option>',
						($Page->get_card_exp('year') == $i ? 'SELECTED ' : ''),
						str_pad($i,2,0,STR_PAD_LEFT),
						str_pad($i,2,0,STR_PAD_LEFT));
						}
						?>
					</select>
				</label>
			</div>

			<div class="col-12">
				<label for="cvv2">
					CSC/CVV2 Number
					<input type="text" class="form-control" name="cvv2" size="4" value="" maxlength="4" id="cvv2"/>
				</label>
			</div>

			<input type="hidden" name="old_exp" value="<?=$Page->get_card_exp()?>"/>
		</div>
	</fieldset>

	<div class="col-md-3">
		<input type="hidden" name="info_hash" value="<?=$Page->get_hash()?>"/>
		<button type="submit" class="btn btn-primary save px-5 mt-3" value="Submit" name="submit">
			Submit
		</button>
	</div>

	<div class="col-md-3">
		<button class="btn btn-secondary warn mt-3" type="reset" value="Reset">
			Reset
		</button>
	</div>

	<p class="col-12 text-muted mt-1">
		&dagger;: We may use this phone number for credit card verification.<br/>
		&Dagger;: Transaction receipts are sent to this address.
	</p>
</form>
<?php
	    	else:
	    	?>
<div class="alert alert-danger">
	<p>
		The subscription on this account has lapsed. Please access our billing portal to renew service.
		<br/><a class="ui-action ui-action-visit-site" href="<?= $renewal ?>">Access
			Billing Portal</a>
	</p>
</div>
<?php
    endif;
    else:
        ?>
<div align="center" class="alert alert-info">
	<p>
		Last payment method is not direct credit card. Please visit <a href="https://www.paypal.com"
		                                                               class="ui-action ui-action-visit-site ui-action-label">PayPal</a>
		to modify payment
		information attached to your hosting.
		<br/><br/>
		<strong>Switching to credit card</strong>? See our KB article,
		<a class="ui-action ui-action-label ui-action-kb"
		   href="<?=MISC_KB_BASE?>/billing/switching-to-from-paypal-credit-card/">Switching to/from PayPal/Credit
			Card</a>
	</p>
</div>
<div class="clear">&nbsp;</div>

<?php
    endif;
?>
