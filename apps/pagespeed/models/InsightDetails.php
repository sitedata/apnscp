<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, March 2020
 */

namespace apps\pagespeed\models;

use ArgumentError;
use Illuminate\Contracts\Support\Jsonable;

class InsightDetails implements Jsonable {

	protected $time;
	protected $numStylesheets;
	protected $numScripts;
	protected $numRequests;
	protected $throughput;
	protected $rtt;
	protected $size;
	protected $docSize;
	protected $ttfb;
	protected $speedIndex;
	protected $firstPaint;
	protected $interactive;

	public function __construct(array $details)
	{
		if (empty($details)) {
			throw new ArgumentError("Invalid insights");
		}
		$this->time = $details['totalTaskTime'];
		$this->size = $details['totalByteWeight'];
		$this->docSize = $details['mainDocumentTransferSize'];
		$this->rtt  = $details['rtt'];
		$this->throughput = $details['throughput'];
		$this->numRequests = $details['numRequests'];
		$this->numScripts = $details['numScripts'];
		$this->numStylesheets = $details['numStylesheets'];
		$this->ttfb = $details['ttfb'];
		$this->firstPaint = $details['firstPaint'];
		$this->speedIndex = $details['speedIndex'];
		$this->interactive = $details['interactive'];
	}

	/**
	 * @return mixed
	 */
	public function getTime(): float
	{
		return $this->time;
	}

	/**
	 * @return mixed
	 */
	public function getNumStylesheets(): int
	{
		return $this->numStylesheets;
	}

	/**
	 * @return mixed
	 */
	public function getNumScripts(): int
	{
		return $this->numScripts;
	}

	/**
	 * @return mixed
	 */
	public function getNumRequests(): int
	{
		return $this->numRequests;
	}

	/**
	 * @return mixed
	 */
	public function getThroughput(): float
	{
		return $this->throughput;
	}

	/**
	 * @return mixed
	 */
	public function getRtt(): float
	{
		return $this->rtt;
	}

	/**
	 * @return mixed
	 */
	public function getSize(): int
	{
		return $this->size;
	}

	/**
	 * @return mixed
	 */
	public function getDocSize(): int
	{
		return $this->docSize;
	}

	/**
	 * @return mixed
	 */
	public function getTtfb(): float
	{
		return $this->ttfb;
	}

	/**
	 * @return mixed
	 */
	public function getFirstPaint(): float
	{
		return $this->firstPaint;
	}

	/**
	 * @return mixed
	 */
	public function getSpeedIndex(): int
	{
		return $this->speedIndex;
	}

	/**
	 * @return mixed
	 */
	public function getInteractive(): float
	{
		return $this->interactive;
	}



	public function __toString()
	{
		return json_encode($this);
	}

	public function toJson($options = 0)
	{
		return $this->__toString();
	}


}