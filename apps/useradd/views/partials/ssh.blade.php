<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-group-label">
		<i class="fa fa-terminal"></i>
		Terminal
	</h5>

	<div class="col-12">
		<label class="pl-0 form-inline custom-checkbox custom-control mt-1 align-items-center d-flex">
			<input type="checkbox" class="custom-control-input" name="ssh_enable" id="ssh_enable"
			       value="1" {{ $Page->get_option('ssh_enable', 'checkbox') }} />
			<span class="custom-control-indicator"></span>
			Enable
		</label>
		<label class="pl-0 form-inline custom-checkbox custom-control align-items-center d-flex">
			<input type="checkbox" class="custom-control-input" name="crontab_enable" id="crontab_enable"
			       value="1" {{ $Page->get_option('crontab_enable', 'checkbox') }} />
			<span class="custom-control-indicator"></span>
			Task scheduling (crontab)
		</label>
	</div>
	<div class="col-12">
		<label class="form-control-label">
			Login shell
			<select name="shell" class="custom-select">
				@php
					$activeShell = $Page->get_option('shell', 'text');
				@endphp
				@foreach (\cmd('user_get_shells') as $shell)
					<option value="{{ $shell }}" @if ($activeShell == $shell) selected="SELECTED" @endif>
						{{ $shell }}
					</option>
				@endforeach
			</select>
		</label>
	</div>
</fieldset>