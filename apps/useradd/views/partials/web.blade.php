<hr/>
<fieldset class="form-group my-3 row">
	<h5 class="col-12 form-control-label">
		<i class="fa fa-desktop"></i>
		Subdomain
	</h5>
	<div class="col-sm-6">
		<div class="d-flex align-items-center">
			<label class="mb-0 pl-0 form-inline custom-checkbox custom-control align-items-center d-inline-flex">
				<input type="checkbox" class="custom-control-input" name="subdomain_enable" id="subdomain_enable"
				       value="1" {{ $Page->get_option('subdomain_enable', 'checkbox') }} />
				<span class="custom-control-indicator"></span>
				Enable
			</label>
			<button type="button" name="Advanced" id="subdomain_advanced" value="Advanced" class="btn btn-secondary"
			        data-toggle="collapse"
			        data-target="#subdomainOptions" aria-expanded="false" aria-controls="subdomainOptions">
				<span class="ui-action-advanced ui-action"></span>
				Advanced
			</button>
		</div>
		<div id="subdomainOptions" class="collapse mt-2">
			Create subdomain on following domains: <br/>
			<select name="subdomain_domains[]" class="custom-select form-control" id="subdomain_domains" multiple="1">
				@foreach (\Util_Conf::all_domains() as $domain)
					<option value="{{ $domain }}" {{ $Page->get_option('subdomain_domains', 'option', $domain) }}
					>{{ $domain }}</option>
				@endforeach
			</select>
		</div>
	</div>
</fieldset>