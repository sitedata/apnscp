<h3>Edit Databases</h3>
<form method="POST" action="{{ \HTML_Kit::page_url_params() }}">
	<table class="table" id="database-list">
		<thead>
		<tr>
			<th class="left">
				Actions
			</th>
			<th>
				Name
			</th>
			<th>
				Owner
			</th>
			<th>
				Size
			</th>
		</tr>
		</thead>
		@foreach($Page->getDatabases() as $dbname)
			@include('partials.db', ['dbowner' => \cmd('pgsql_get_owner', $dbname), 'users' => $users])
		@endforeach
	</table>
</form>

<h3>Edit Users</h3>

@foreach($users as $user => $connlimit)
	@include('partials.user', compact('user', 'connlimit'))
@endforeach

@include('partials.modal')