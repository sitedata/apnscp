<!-- main object data goes here... -->
<form method="post" action="/apps/logrotate.php?mode={{ $Page->get_edit_mode() }}">

	<!-- begin advanced edit mode -->
	@if ($Page->get_edit_mode() == 'advanced')

		<h4>Default Configuration</h4>
		<p class="text-muted note">
			Also available under <code>/etc/logrotate.conf</code>
		</p>
		<div class="row mb-1">
			<div class="col-12">
	                <textarea class="form-control" rows="15" name="default_logrotate"
	                    >{{ $Page->get_default_logrotate() }}
	                </textarea>
			</div>
		</div>

		<h3>Log Profiles</h3>
		<div class="row">
			<div class="col-3">
				Profile Name
			</div>
			<div class="col-3">
				Log Path
			</div>
			<div class="col-3">
				Configuration
			</div>
		</div>
	@else
		@php
			$default_options = $Page->get_default_options();
		@endphp
		<div class="row">
			<div class="col-4">
				<fieldset class="form-group">
					<label class="d-block">Retention policy</label>
					<select name="rotation_type" id="rotationSelection" class="form-control custom-select w-100">
						<option value="periodic"
								@if ($default_options['rotation_type'] == 'periodic') SELECTED @endif
						        data-target="#periodicGroup">Periodic
						</option>
						<option value="size"
								@if ($default_options['rotation_type'] == 'size') SELECTED @endif
						        data-target="#sizeGroup">Size
						</option>
					</select>
				</fieldset>
			</div>
			<div class="col-4">
				<fieldset class="form-group">
					<div class="rotation-param collapse @if ($default_options['rotation_type'] == 'size') show @endif"
					     id="sizeGroup">
						<label class="d-block">Max size</label>
						<div class="input-group">
							<input class="form-control" type="number" min="0" size="5" value="{{ $default_options['size'] }}"
							       name="size" id="rotation_size"/>
							<span class="input-group-addon">MB</span>
						</div>
					</div>
					<div class="rotation-param collapse @if ($default_options['rotation_type'] == 'periodic') show @endif"
					     id="periodicGroup">
						<label class="d-block">Frequency</label>
						<select class="custom-select w-100" name="periodic"
						        id="rotation_period">
							@foreach (['daily', 'weekly', 'monthly'] as $frequency)
								<option value="{{ $frequency }}" @if ($default_options['frequency'] === $frequency) SELECTED @endif
								>{{ _(ucwords($frequency)) }}</option>
							@endforeach
						</select>
					</div>
				</fieldset>
			</div>
			<div class="col-4">
				<fieldset class="form-group">
					<label for="hold_logs">
						Maximum files per log
					</label>
					<input type="number" class="form-control" name="hold_logs" size="2" id="hold_logs"
					       value="{{ $default_options['hold_logs'] }}"/>
				</fieldset>
			</div>
			<div class="col-12 clearfix">
				<div class="btn-group">
					<button type="submit" value="Save" class="btn btn-primary" name="Save">
						Save
					</button>
					<button type="button" data-toggle="collapse" data-target="#advanced-container"
					        aria-controls="advanced-container" aria-expanded="false" value="Advanced" id="advanced"
					        class="btn btn-secondary btn-group-addon ui-action-advanced secondary" name="Advanced">Advanced
					</button>
				</div>
			</div>


			<div id="advanced-container" class="mt-1 col-12 collapse">
				<fieldset class="form-group mb-0">
					<label class="custom-checkbox custom-control mt-1 mb-0 align-items-center d-flex">
						<input type="checkbox" class="custom-control-input" id="email-toggle" name="email"
						       value="1" @if (!empty($default_options['mail'])) checked="CHECKED" @endif
						       data-target="#email-container" data-toggle="collapseCheckbox"
						       aria-expanded="{{ !empty($default_options['mail']) ? 'true' : 'false' }}"/>
						<span class="custom-control-indicator"></span>
						Send a copy of the most recent logfile
					</label>

					<div id="email-container" class="collapse mb-1 @if (!empty($default_options['mail'])) show @endif">
						<label for="email">Email address</label>
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
							<input class="form-control" type="text" name="email_address" size="40" id="email"
							       value="{{ $default_options['mail'] }}"/>
						</div>
					</div>
				</fieldset>

				<fieldset class="form-group">
					<label class="custom-checkbox custom-control my-1 align-items-center d-flex">
						<input type="checkbox" class="custom-control-input" id="compress-toggle" name="compress"
						       value="1" @if ($default_options['compress']) CHECKED @endif data-target="#delay-container"
						       data-toggle="collapseCheckbox"
						       aria-expanded="{{ $default_options['compress'] ? 'true' : 'false' }}"/>
						<span class="custom-control-indicator"></span>
						Compress logs after rotation
					</label>

					<div id="delay-container" class="collapse @if ($default_options['compress']) show @endif">
						<label class="custom-checkbox custom-control mt-0 align-items-center d-flex">
							<input type="checkbox" class="custom-control-input" name="defer" value="1"
							       id="defer_compression" @if ($default_options['compress']) CHECKED @endif />
							<span class="custom-control-indicator"></span>
							Delay compression 24 hours
						</label>
					</div>

					<div id="delay-container">
						<label class="custom-checkbox custom-control mb-1 align-items-center d-flex">
							<input type="checkbox" class="custom-control-input" name="dateext" value="1"
							       id="dateext" @isset($default_options['dateext']) CHECKED @endif />
							<span class="custom-control-indicator"></span>
							Use date as file suffix
						</label>
					</div>
				</fieldset>
			</div>
		</div>

	@endif
</form>