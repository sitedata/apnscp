@if (!$Page->hasSSO())
	<p class="alert-info note w-100">
		Single sign-on is not available. Mail access requires manual login. Username
		is {{ \UCard::get()->getUser() . '@' . \UCard::get()->getDomain() }}
	</p>
@endif
<form method="get">
	@each('partials.app', $Page->webmails(), 'app')
</form>