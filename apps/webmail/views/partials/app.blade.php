<div class="panel webmail-container webmail-{{ $app['id'] }}">
	<button id="{{ $app['id'] }}-logo" class="hidden-sm-down webmail-logo webmail-link border-0"
	        name="app" value="{{ $app['id'] }}"></button>
	<h4>{{ $app['name'] }}</h4>
	<button class="ui-action ui-action-label ui-action-visit-site webmail-link btn btn-primary"
	        name="app" value="{{ $app['id'] }}" @if (\Page_Renderer::externalOpener()) rel="external"
	        formtarget="{{ \Page_Renderer::externalOpenerTarget()  }}" @endif>
		Access {{ $app['name'] }}
	</button>
	<p class="url my-1">
		<i class="fa fa-cloud"></i>
		{{ $app['alias'] }}
	</p>
	@if (\UCard::get()->hasPrivilege('site'))
		<button class="btn btn-secondary ui-action ui-action-label edit-url" id="change-{{ $app['id'] }}"><span
					class="ui-action ui-action-edit"></span>Change Shortcut
		</button>
	@endif
	<a class="lightbox btn btn-outline-secondary ui-action ui-action-label ui-action-view-picture"
	   title="{{ $app['name'] }}"
	   href="/images/apps/webmail/{{ $app['id'] }}-screencap.png">
		View Screenshot
	</a>
</div>