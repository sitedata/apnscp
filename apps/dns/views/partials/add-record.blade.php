<h3>Add DNS Record</h3>

<div class="row">
	<div class="col-12 col-sm-8 col-md-4 col-lg-4 col-xl-4">
		<fieldset class="form-group">
			<label>Hostname </label>
			<label for="subdomain" class="hinted">subdomain</label>
			<div class="input-group">
				<input type="text" class="form-control" id="subdomain" name="add_name" value=""/>
				<span class="input-group-addon d-md-none">.<?=$Page->getDomain();?></span>
			</div>
		</fieldset>
	</div>

	<div class="col-12 col-md-3 col-lg-3 col-xl-4">
		<div class="row">
			<div class="col-12 col-sm-8 col-md-6 col-lg-6 col-xl-5">
				<fieldset class="form-group">
					<label>RR</label>
					<select name="add_rr" class="form-control custom-select" id="rr">
						@foreach ($Page->getSupportedRRs() as $rr):
						<option value="{{ $rr }}" {{ !$loop->first  or 'SELECTED' }}>{{ $rr }}</option>
						@endforeach
					</select>
				</fieldset>
			</div>

			<div class="col-12 col-sm-8 col-md-2 col-lg-3 col-xl-2 hidden-lg-down">
				<fieldset class="form-group">
					<label>Class</label>
					<div class="form-control-static">IN</div>
				</fieldset>
			</div>

			<div class="col-12 col-sm-8 col-md-6 col-lg-6 col-xl-5">
				<fieldset class="form-group">
					<label>TTL</label>
					<input type="text" name="add_ttl" class="form-control"
					       value="<?php print \Dns_Module::DNS_TTL; ?>" size="6"/>
				</fieldset>
			</div>
		</div>
	</div>


	<div class="col-12 col-sm-8 col-md-3 col-lg-3 col-xl-3">
		<fieldset class="form-group">
			<label>Parameter</label>
			<label for="parameter" class="hinted" id="label-parameter"></label>
			<input type="text" class="form-control" name="add_parameter" id="parameter"/>
		</fieldset>
	</div>

	<div class="col-12 col-sm-8 col-md-2 col-lg-2 col-xl-1">
		<fieldset class="forum-group">
			<label class="hidden-sm-down">&nbsp;</label>
			<button type="submit" name="Add_Zone" class="form-control primary add" value="Add">
				Add
			</button>
		</fieldset>
	</div>

</div>