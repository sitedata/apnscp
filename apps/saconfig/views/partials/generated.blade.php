<div class="" id="postback">
	<div class="text text-success lead">
		Below is your generated configuration. We've <b>automatically added</b> these settings for you.
	</div>
	@if (\Opcenter\Mail\Services\Spamassassin::present())
		<div class="mb-3">
			<div class="d-flex align-items-center flex-row mb-1">
				<h4 class="mb-0">{{ \cmd('user_get_home') }}/.spamassassin/user_prefs</h4>
				<button type="button" data-placement="left"
				        class="btn btn-outline-secondary ui-action ui-action-copy ui-action-label ml-auto"
			            data-clipboard-target="#sacfg" data-target="recipe">
					Copy
				</button>
			</div>
			<textarea rows="10" cols="72" name="code" id="sacfg"
			          class="form-control text-monospace clipboard" WRAP="OFF"
			>{{ $saconfig }}
				</textarea>

		</div>
	@endif
	<div class="mb-3">
		<div class="d-flex align-items-center flex-row mb-1">
			<h4 class="mb-0">{{ \cmd('user_get_home') }}/.mailfilter</h4>
			<button type="button" data-placement="left"
			        class="btn btn-outline-secondary ui-action ui-action-copy ui-action-label ml-auto"
			        data-clipboard-target="#recipe" data-target="recipe">
				Copy
			</button>
		</div>

		<textarea rows="10" cols="72" name="recipe" id="recipe"
		          class="form-control text-monospace clipboard" WRAP="OFF"
		>{{ $recipe }}
				</textarea>
	</div>
	<hr/>
</div>