<div id="dev" class="tab-pane" role="tabpanel" aria-labelledby="">
	<div class="mb-3">
		<h4>Flush OverlayFS Cache</h4>
		<p>Stale file handle error messages in Terminal? File ghosting? Dump the OverlayFS cache to force a refresh
			of your filesystem.</p>
		<fieldset>
			<button type="submit" name="purgecache" value="1" class="btn btn-primary">
				Purge Cache
			</button>
		</fieldset>
	</div>

	<div class="mb-3">
		<h4>Code Page Conversion</h4>
		<p>
			Code page conversion automatically converts special characters, such as “ and ” to
			their approximate representations (&quot;) when the detected file is US-ASCII in
			<b>Files</b> &gt; <b>File Manager</b>. Leaving this disabled will automatically
			upconvert a file to UTF-8 encoding if such characters are detected. This is
			helpful if you edit a PHP file, for example, on an Apple product.
		</p>
		<label class="custom-control custom-checkbox mb-0">
			<input type="hidden" name="pref{{\HTML_Kit::prefify(\apps\filemanager\Page::TRANSLIT_KEY) }}" value="0"/>
			<input type="checkbox" class="custom-control-input"
			       name="pref{{\HTML_Kit::prefify(\apps\filemanager\Page::TRANSLIT_KEY) }}" value="1"
			       @if (\Preferences::get(\apps\filemanager\Page::TRANSLIT_KEY)) checked="CHECKED" @endif />
			<span class="custom-control-indicator"></span>
			Implicitly convert characters to closest US-ASCII match.
		</label>
	</div>
</div>