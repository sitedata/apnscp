<?php declare(strict_types=1);
/**
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, July 2020
 */

namespace apps\mailboxroutes\models;

use Template\Searchlet;

class Search extends Searchlet
{
	public function specs(): array
	{
		return [
			'user'    => 'User',
			'domain'  => 'Domain',
			'mailbox' => 'Destination',
			'type'    => 'Type'
		];
	}

	public function formAction(): ?string
	{
		return \HTML_Kit::page_url_params(['mode' => 'edit']);
	}

	public function operations(): array
	{
		return [];
	}

	public function filterClass(): string
	{
		return 'filter-mail';
	}


}

