<table class="table table-bordered table-striped table-responsive">
	@if (!posix_getuid())
	<tr>
		<th>Valid</th>
		<td>
			{{ $license->verify() ? 'YES' : 'NO' }}
		</td>
	</tr>
	@endif
	<tr>
		<th>Common Name</th>
		<td>
			{{ $license->getCommonName() }}
		</td>
	</tr>
	<tr>
		<th>Serial</th>
		<td>
			{{ $license->getSerial() }}
		</td>
	</tr>
	<tr>
		<th>Fingerprint</th>
		<td>
			{{ implode(':', str_split(strtoupper(openssl_x509_fingerprint($license, $license->getLicense()['signatureTypeLN'])), 2)) }}
		</td>
	</tr>
	<tr>
		<th>Expiration date</th>
		<td>
			{{ (new DateTime())->setTimestamp(array_get($license->getLicense(), 'validTo_time_t', time()))->format('r')  }}
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<h4 class="mb-0">Features</h4>
		</td>
	</tr>
	<tr>
		<th>Revoked</th>
		<td>
			{{ $license->revoked() ? 'YES' : 'NO' }}
		</td>
	</tr>
	<tr>
		<th>Domain limit</th>
		<td>
			{{ $license->hasDomainCountRestriction() ? 'YES' : 'NO' }}
			@if ($license->hasDomainCountRestriction())
				&ndash; {{ sprintf("%d/%d", \count(\Opcenter\Map::load(\Opcenter\Map::DOMAIN_MAP)->fetchAll()), $license->getDomainLimit()) }}
			@endif
		</td>
	</tr>
	<tr>
		<th>Lifetime</th>
		<td>
			{{ $license->isLifetime() ? 'YES' : 'NO' }}
		</td>
	</tr>
	<tr>
		<th>Trial license</th>
		<td>
			{{ $license->isTrial() ? 'YES' : 'NO' }}
		</td>
	</tr>
	<tr>
		<th>Loopback restrictions</th>
		<td>
			{{ $license->isLocal() ? 'YES' : 'NO' }}
		</td>
	</tr>
	<tr>
		<th>Network restrictions</th>
		<td>
			{{ $license->hasNetRestriction() ? 'YES' : 'NO' }}
			@if ($license->hasNetRestriction())
				<ul class="mt-1 mb-0 list-unstyled">
					@foreach ($license->getAuthorizedNetworks() as $range)
						<li/>- {{ $range[0] }}@if ($range[0] !== $range[1]) &ndash; {{ $range[1] }}@endif
					@endforeach
				</ul>
			@endif
		</td>
	</tr>
	<tr>
		<th>Development-only</th>
		<td>
			{{ $license->isDevelopment() ? 'YES' : 'NO' }}
		</td>
	</tr>
	<tr>
		<th>DNS-only</th>
		<td>
			{{ $license->isDnsOnly() ? 'YES' : 'NO' }}
		</td>
	</tr>
</table>