@php
	$bandwidth = $Page->getBandwidthData() ;
	$stats = $Page->getStats($bandwidth);
	$bwquota = $Page->getBandwidthQuota();
	$color = 'listodd';
@endphp

@includeWhen(count($bandwidth) < \apps\bandwidthstat\Page::MINIMUM_CYCLES, 'partials.insufficient-data')
@includeWhen(count($bandwidth) >= \apps\bandwidthstat\Page::MINIMUM_CYCLES, 'bandwidth-display')