<div class="alert alert-info text-lg missing-data">
	<i class="fa fa-calendar fa-2x"></i>
	Oops, there isn't enough data on this account yet!
	Come back next month after we have collected some data for you.
</div>