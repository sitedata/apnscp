var prev_lang = null;
$(document).ready(function () {
	if (!$('input.lang:checked').val()) {
		$('#packman_form table tbody').hide();
	}
	$('input.lang').click(function () {
		$('#packman_form table tbody').show();
		populate_installed_modules();
	});
	$('#remote_pane').focus(function () {
		deselect_select("#local_pane");
		return false;
	});
	$('#local_pane').focus(function () {
		deselect_select("#remote_pane");
		return false;
	});
	$('#packman_form').submit(function () {
		install_module($('#remote_pane').val(), get_selected_language());
		return false;
	});
});

function deselect_select(n) {
	$(n).get(0).selectedIndex = -1;
}

function populate_installed_modules() {
	// don't call ajax_end() since we
	// cascade to populate_remote_modules
	ajax_start();
	$.getJSON(ajax_url + '?fn=get_installed_modules', {
		s: session.id,
		type: get_selected_language()
	}, function (data) {
		add_modules('local_pane', data);
		populate_remote_modules("alphabet");
	});
}

function add_modules(pane, data) {
	var el = $('#' + pane);
	el.empty();
	for (pkg_idx in data)
		for (pkg in data[pkg_idx]) {
			el.append($('<option />').text(pkg + ' (' + data[pkg_idx][pkg] + ')').val(pkg));
		}
}

function populate_remote_modules(filter_type) {
	ajax_start();
	$.getJSON(ajax_url + '?fn=get_modules', {
			s: session.id,
			type: get_selected_language(),
			filter: $('#alphabet').val(),
			mode: 'alphabet'
		}, function (data) {
			add_modules('remote_pane', data);
			ajax_end();
		}
	);
}

function install_module(name) {
	$.ajax_unbuffered({
		url: ajax_url + '?fn=install_module',
		data: {
			s: session.id,
			name: name,
			language: get_selected_language()
		},
		output: $('#output_status_container'),
		success: function (data, success) {
			if (!data) return;
			$('#local_pane').append($('<option />').val(name).text($('#remote_pane :selected').text()));
			$('#local_pane').get(0).scrollTop += 1000;
			return true;
		}
	});

}

function get_selected_language() {
	return $('input[name=repository]:checked').val();
}

function load_doc(name) {
	ajax_start();
	$.post(ajax_url + '?fn=load_doc',
		{
			s: session.id,
			language: get_selected_language(),
			module: $('#remote_pane').val()
		}, function (desc) {
			$('#pkg-desc').text(desc.desc);
			$('#pkg-url').attr('href', desc.url).text(desc.url);
			ajax_end();
		}, "json");
}

function ajax_start() {
	$('#status').html('Loading...');
	$('#loading_img').addClass('ui-spinner-small');
}

function ajax_end() {
	$('#status').html('&nbsp;');
	$('#loading_img').removeClass('ui-spinner-small');
}
