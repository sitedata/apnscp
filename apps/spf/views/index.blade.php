
<form method="POST" action="{{ HTML_Kit::page_url_params(['domain' => $Page->getDomain()]) }}" id="spfForm">
	<h4 class="form-inline">
		SPF Record Setup for
		<select name="domain" class="ml-1 form-control custom-select form-control-inline "
		        OnChange="document.location='/apps/spf?domain=' + this.value;">
			@foreach($Page->list_domains() as $domain)
				<option @if (isset($_GET['domain']) && $_GET['domain'] == $domain) SELECTED @endif
					name="{{ $domain }}">{{ $domain }}</option>
			@endforeach
		</select>
	</h4>
	@if (!$ipaddress = $Page->getIPAddress())
		<p class='alert alert-warning'>
			No DNS records found for hostname <b>{{ $Page->getDomain() }}</b>.
			Record cannot be automatically added.
		</p>
    @endif
	<INPUT TYPE="HIDDEN" NAME="mydomain" VALUE="{{ $Page->getDomain() }}">
	<table class="table table-responsive table-striped" id="SPF-Wizard">
		@if (!empty($spfExists = $Page->getSpf()))
		<TR>
			<TD CLASS="warning" COLSPAN="3">
				<div class="alert alert-warning">
					<strong>Warning:</strong> A SPF record already exists for your domain.  Clicking &quot;Save Record&quot; will
					overwrite this record:
					<code class="prior-spf">
						{{ $spfExists }}
					</code>
				</div>
			</TD>
		</TR>
		@endif
		<TR>
			<TD CLASS="cell1" COLSPAN=1>
				The following IP addresses were detected for {{ $Page->getDomain() }}.
				<ul class="list-unstyled">
					@foreach ($ipaddress as $addr)
						<li class="">{{ $addr }}</li>
					@endforeach
				</ul>

				Is mail sent by any of these IPs?
			</TD>
			<TD CLASS="cell1" ALIGN=CENTER>
				[a]
			</TD>

			<TD CLASS="cell1" WIDTH="30%">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-secondary active">
						<INPUT TYPE="radio" class="input" NAME="a" VALUE="yes" CHECKED />
						Yes
					</label>

					<label class="btn btn-secondary">
						<INPUT TYPE="radio" class="input" NAME="a" VALUE="no" />
						No
					</label>
				</div>
			</TD>
		</TR>
		<TR>
			<TD CLASS="cell1" COLSPAN=1>
				@php
					$mxrecords = $Page->getMXRecords();
				@endphp
				This wizard detected the following MX servers for {{ $Page->getDomain() }}.
				<table class="table-secondary">
					<thead>
						<th>Priority</th>
						<th>Target</th>
					</thead>
					@foreach ($mxrecords as $record)
						@php
							$record = explode(' ', $record['parameter'], 2);
						@endphp
						<tr>
							<td>{{ $record[0] }}</td>
							<td>{{ $record[1] }}</td>
						</tr>
					@endforeach
				</table>

				Do any of these hostnames also send email for the domain?
				<em>A single machine may go by more than one hostname. All of them are shown.</em>
			</TD>
			<TD CLASS="cell1" ALIGN=CENTER>
				[mx]
			</TD>
			<TD CLASS="cell1" WIDTH="30%">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-secondary active">
						<INPUT TYPE="radio" class="input" NAME="mx" VALUE="yes" CHECKED />
						Yes
					</label>

					<label class="btn btn-secondary">
						<INPUT TYPE="radio" class="input" NAME="mx" VALUE="no" />
						No
					</label>
				</div>
			</TD>
		</TR>
		<TR>
			<TD CLASS="cell1" COLSPAN=1>
				Do you want to just approve any host whose name ends in
				<em>{{ $Page->getDomain() }}</em>? (Expensive, unreliable and not recommended)
			</TD>
			<TD CLASS="cell1" ALIGN=CENTER>
				[ptr]
			</TD>
			<TD CLASS="cell1" WIDTH="30%">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-secondary">
						<INPUT TYPE="radio" class="input" NAME="ptr" VALUE="yes" />
						Yes
					</label>

					<label class="btn btn-secondary active">
						<INPUT TYPE="radio" class="input" NAME="ptr" VALUE="no" CHECKED />
						No
					</label>
				</div>
			</TD>
		</TR>

		<TR>
			<TD CLASS="cell1" ROWSPAN=2 VALIGN=TOP>
				<P>Do any other servers send mail from <em>{{ $Page->getDomain() }}</em>? <br/>
					You can describe them by giving "arguments" to the <B>a:</B>, <B>mx:</B>, <B>ip4:</B>,
					and <B>ptr:</B> mechanisms.
					To keep the wizard short we left out <B>ptr:</B> but it works the same way.
				</P>
			</TD>
			<TD CLASS="cell1" ALIGN=CENTER>
				[a:]
			</TD>
			<TD WIDTH="30%">
				<TEXTAREA NAME="a_colon" class="form-control input" ROWS="3" placeholder="regular hostnames"></TEXTAREA>
			</TD>
		</TR>
		<TR>
			<TD>
				[mx:]
			</TD>
			<TD WIDTH="30%">
				<TEXTAREA class="form-control input" NAME="mx_colon" ROWS="3" placeholder="MX server"></TEXTAREA>
			</TD>
		</TR>
		<TR>
			<TD CLASS="cell1" ALIGN=RIGHT>
				<i>IP networks can be entered using CIDR notation such as 192.0.2.0/24</i>
			</TD>
			<TD CLASS="cell1" ALIGN=CENTER>
				[ip4:]
			</TD>
			<TD CLASS="cell1" ALIGN=CENTER WIDTH="30%">
				<TEXTAREA class="form-control input" NAME="ip4_colon" ROWS="3" placeholder="IP addresses"></TEXTAREA>
			</TD>
		</TR>
		<!-- </TR> -->
		<TR>
			<TD CLASS="cell1" COLSPAN=1>
				Could mail from <em>{{ $Page->getDomain() }}</em> originate through
				servers belonging to some other domain?
				<BR>If you send mail through your ISP's servers, and the ISP has
				published an SPF record, name the ISP here.
			</TD>
			<TD>
				[include:]
			</TD>
			<TD WIDTH="30%">
				<INPUT TYPE="text" NAME="include" class="form-control input" VALUE="" placeholder="example.com" />
			</TD>
		</TR>
		<TR>
			<TD CLASS="cell1" COLSPAN=1>
				Do the above lines describe <strong>all the hosts</strong>
				that send mail from <em>{{ $Page->getDomain() }}</em>?
			</TD>
			<TD CLASS="cell1">
				[-all]
			</TD>
			<TD CLASS="cell1" WIDTH="30%">
				<div class="btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-secondary active">
						<INPUT TYPE="radio" class="input" NAME="all" VALUE="yes" CHECKED />
						Yes
					</label>

					<label class="btn btn-secondary">
						<INPUT TYPE="radio" class="input" NAME="all" VALUE="maybe" />
						Maybe
					</label>

					<label class="btn btn-secondary">
						<INPUT TYPE="radio" class="input" NAME="all" VALUE="no" />
						No
					</label>
				</div>
			</TD>
		</TR>
		<!-- record-so-far -->
		<TR>
			<TD CLASS="cell1" COLSPAN=2>
				<h5>Proposed SPF Record</h5>
				<div class="input-group">
					<span class="input-group-addon">{{ $Page->getDomain() }}. IN TXT</span>
					<input type="text" NAME="record_so_far" class="form-control" id="spf_rec"/>
					<button class="input-group-append input-group-addon ui-action ui-action-copy ui-action-d-compact" type="button"
					        data-toggle="tooltip" title="Copy to Clipboard"
					        id="clipboardCopySpfCommand" data-clipboard-target="#spf_rec">
					</button>
				</div>
			</TD>
			<TD CLASS="cell1 align-bottom">
				 @if ($ipaddress)
					<INPUT TYPE=HIDDEN NAME="use_built_from_args" VALUE="1">
					<INPUT TYPE=SUBMIT class="btn btn-primary" NAME="Save" VALUE="Save Record">
				@endif
			</TD>
		</TR>
	</table>
</form>