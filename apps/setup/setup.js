/*
 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
 *
 * Unauthorized copying of this file, via any medium, is
 * strictly prohibited without consent. Any dissemination of
 * material herein is prohibited.
 *
 * For licensing inquiries email <licensing@apisnetworks.com>
 *
 * Written by Matt Saladna <matt@apisnetworks.com>, February 2021
 */
$(window).on('load', function () {
	activateCopyCommand(jQuery);
});

$('document').ready(function() {
	$('#domain').on('change', function () {
		$(this).closest('form').submit();
		return true;
	})
})

function activateCopyCommand($) {
	var COPY_SUCCESS_MSG = 'copied!';
	clipboard = new Clipboard('#ui-app .ui-action-copy');
	$('#ui-app [data-toggle="tooltip"]').tooltip();

	clipboard.on('success', function (e) {
		var originalTitle = $(e.trigger).attr('data-original-title') || 'copy';
		$(e.trigger).attr('data-original-title', COPY_SUCCESS_MSG).tooltip('show').on('hidden.bs.tooltip', function (e) {
			$(e.currentTarget).attr('data-original-title', originalTitle);
		});
		e.clearSelection();
	});

}