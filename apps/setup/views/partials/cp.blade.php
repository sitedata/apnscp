@component('partials.about', ['service' => 'Control panel'])
@endcomponent

<table class="table table-responsive table-striped">
	<tr>
		<th>
			{{ _("URL") }}
		</th>
		<td>
			{{ Auth_Redirect::getPreferredUri() }}
		</td>
	</tr>
	<tr>
		<th>
			{{ _("Username") }}
		</th>
		<td>
			{{ $auth->username() }}
		</td>
	</tr>
	<tr>
		<th>
			{{ _("Domain") }}
		</th>
		<td>
			{{ $auth->domain() }}
		</td>
	</tr>
	<tr>
		<th>
			{{ _("Password") }}
		</th>
		<td>
			<em>
				{{ _("Password used to access panel") }}
			</em>
		</td>
	</tr>
</table>

