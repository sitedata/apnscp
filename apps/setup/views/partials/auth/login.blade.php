<tr>
	<th>
		{{ _("Login") }}
	</th>
	<td>
		{{ $auth->username() }}{{ '@' . $auth->domain() }}
	</td>
</tr>
<tr>
	<th>
		{{ _("Alternative login") }}
	</th>
	<td>
		{{ $auth->username() }}#{{ $auth->domain() }}
	</td>
</tr>
<tr>
	<th>
		{{ _("Password") }}
	</th>
	<td>
		<em>
			{{ _("Password used to access panel") }}
		</em>
	</td>
</tr>