<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, November 2018
	 */

	namespace apps\ssl;

	class Certificate implements \ArrayAccess
	{
		// @var string
		protected $certificate;

		// @var resource
		protected $resource;

		// @var array
		protected $info;

		public function __construct(string $certificate)
		{
			$this->certificate = $certificate;
			$this->resource = openssl_x509_read($certificate);
			$this->info = openssl_x509_parse($certificate);
		}

		public function __toString()
		{
			return $this->certificate;
		}

		#[\ReturnTypeWillChange]
		public function offsetExists($offset)
		{
			return array_has($this->info, $offset);
		}

		#[\ReturnTypeWillChange]
		public function offsetGet($offset)
		{
			return array_get($this->info, $offset);
		}

		#[\ReturnTypeWillChange]
		public function offsetSet($offset, $value)
		{
			fatal("Cannot modify private key resource");
		}

		#[\ReturnTypeWillChange]
		public function offsetUnset($offset)
		{
			fatal("Cannot modify private key resource");
		}


	}


