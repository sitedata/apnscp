$('#configurationTarget').on('show.bs.collapse', function () {
	apnscp.call_app(null, 'handlePhpinfo', {'pool': $('#poolName').val(), 'params': []}, {useCustomHandlers:true}).done(function (e) {
		$('#configurationTarget').html($('<iframe />').attr({'srcdoc': e}));
	});
	return true;
}).on('hidden.bs.collapse', function () {
	$(this).empty();
})