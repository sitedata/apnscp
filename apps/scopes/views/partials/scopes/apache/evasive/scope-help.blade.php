<p class="help">
	{{ $scope->getHelp() }}. See <a href="https://docs.apiscp.com/admin/Evasive/#configuration">docs/Evasive.md</a>
	for detailed configuration.
</p>