<?php declare(strict_types=1);
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, April 2021
	 */

	namespace apps\nexus\models;

	use Template\Sortlet;

	class ResourceSort extends Sortlet
	{
		public function specs(): array
		{
			$params = [
				'domain'     => 'Domain',
			];

			if (CGROUP_SHOW_USAGE && \Preferences::get('nexus.show-resources')) {
				$params += [
					'cpu'     => 'CPU Usage',
					'disk-io' => 'Disk IO Usage',
					'memory'  => 'Memory',
					'procs'   => 'Processes'
				];
			}

			$params += ['storage' => 'Storage'];

			if (\Preferences::get('nexus.show-inodes')) {
				$params += [
					'inodes' => 'Inodes'
				];
			}



			$params += ['bandwidth' => 'Bandwidth'];

			return $params;
		}

		public function filterOrder(): string
		{
			return self::FILTER_ASC;
		}


	}

