<label class="mb-0 px-1">
	<span class="indicator @if (!$limit) text-gray-dark @elseif (($percentage = $used/$limit)*100 > $Page->getThreshold('danger')) text-danger @elseif ($percentage*100 > $Page->getThreshold('warning')) text-warning @else text-success @endif">
		<i class="fa fa-circle"></i>
	</span>
	{{ $title }}
</label>
@include('master::partials.shared.usage-fill')