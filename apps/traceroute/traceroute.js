var xhr;

function performTraceroute(ip) {
	apnscp.call_app(null, 'reserve_tee').done(function (ret) {
		var addr = $('#destination').val() || addr;
		var xhr = $.ajax_unbuffered({
			output: $('#output_status'),
			beforeSend: function () {
				$('#status').text("");
				return true;
			},
			onUpdate:
				function () {
					$('#output_status_container').get(0).scrollTop += 100000;
				},
			url: '/ajax?engine=cmd&fn=pman_run',
			file: ret,
			type: "POST",
			data:
				{
					args: ['traceroute %s', [addr], [], {'tee': ret}],
				}
		}).done(function () {
			xhr = null;
		}).fail(function () {
			$("#status").text("Another traceroute process is running. This must first finish.");
		});
	});

}

$(document).ready(
	function () {
		$('#traceroute_form').submit(function (e) {
			xhr && xhr.abort();
			$('#output_status').empty();
			performTraceroute($('#destination').val());
			return false;
		});
	}
);