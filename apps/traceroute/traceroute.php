<?php
	/**
	 * Copyright (C) Apis Networks, Inc - All Rights Reserved.
	 *
	 * Unauthorized copying of this file, via any medium, is
	 * strictly prohibited without consent. Any dissemination of
	 * material herein is prohibited.
	 *
	 * For licensing inquiries email <licensing@apisnetworks.com>
	 *
	 * Written by Matt Saladna <matt@apisnetworks.com>, May 2017
	 */

	namespace apps\traceroute;

	use Opcenter\Net\IpCommon;
	use Page_Container;

	class Page extends Page_Container
	{
		public function __construct()
		{
			parent::__construct();
			$this->init_js('ajax_unbuffered');
			$this->add_javascript('traceroute.js');
			$url = \HTML_Kit::new_page_url_params('/apps/traceroute/traceroute-ajax.php',
				array('fn' => 'perform_traceroute'));
			$this->add_javascript('var clientip = "' . \Auth::client_ip() . '";', 'internal');
		}

		public function on_postback($params)
		{
			if (isset($_POST['destination']) && !preg_match(\Regex::DOMAIN, $_POST['destination']) &&
				ip2long($_POST['destination']) < 0) {
				error("Invalid destination host/IP");
			}
		}

		public function reserve_tee()
		{
			return (new \apps\crontab\Page)->reserve_tee();
		}

		public function run($cmd, $tee)
		{
			return (new \apps\crontab\Page)->run($cmd, $tee);
		}
	}
