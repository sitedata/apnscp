$(document).ready(function () {
	$(".entry").highlight();
	$('#keys .entry .key-data div.comment').filter(':empty').hide();
	$('#keys .entry .ui-action-edit').click(function () {
		var dialog = null, $parent = $(this).parentsUntil('.entry').parent(),
			comment = $parent.find('.comment').text(),
			$html = $("<div id='comment-edit'/>"),
			indicator = $('<i></i>'),
			key = $.trim($parent.find('.key').text()),
			$commentedit = $('<input/>').attr(
				{
					name: 'comment',
					maxlength: 255,
					'class': 'form-control',
					value: $.trim(comment),
					type: 'text'
				}
			).bind("keydown", function (e) {
				if (e.keyCode == 13 /* enter */) {
					return $submit.click();
				}
			});
		$html.append($('<label class="mb-0 comment-new">New Comment</label>'), $commentedit);
		var $submit = $('<button />').attr(
			{
				'class': 'btn btn-primary mt-3',
				'id': "comment-save",
				'type': 'button'
			}
		).text("Save").bind('click', function () {
			var t = this, keyold, keytmp = key.toLowerCase();
			do {
				keyold = keytmp;
				keytmp = keyold.replace("-", "");
			} while (keyold != keytmp);

			var newcomment = dialog.find('[name=comment]').val();
			apnscp.cmd("auth_set_api_key_comment", [keytmp, newcomment], function (s) {
					var timeout = 4500;
					if (s.success) {
						$parent.find('.comment').fadeOut(function() {
							$(this).text(newcomment).fadeIn('fast');
						});
						timeout = 0;
					}

					setTimeout(function () {
						dialog.modal('hide');
					}, timeout);
				},
				{indicator: indicator}
			);
		}).append(indicator);
		$html.append($submit);
		dialog = apnscp.modal($html);

		dialog.modal('show');
		return false;
	});
	apnscp.hinted();
});

$(window).on('load', function () {
	var COPY_SUCCESS_MSG = 'copied!';
	$('[data-toggle="tooltip"]').tooltip();
	var clipboard = new Clipboard('.ui-action-copy');
	clipboard.on('success', function (e) {
		var originalTitle = $(e.trigger).attr('data-original-title');
		$(e.trigger).attr('data-original-title', COPY_SUCCESS_MSG).tooltip('show').on('hidden.bs.tooltip', function (e) {
			$(e.currentTarget).attr('data-original-title', originalTitle);
		});

		e.clearSelection();
	});
});