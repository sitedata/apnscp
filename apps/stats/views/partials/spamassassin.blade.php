<div class="card">
	<div class="card-header" role="tab" id="headingSpamfiltering">
		<h5 class="mb-0">
			<a class="btn-block" data-toggle="collapse" data-parent="#accordion" href="#spamfiltering"
			   aria-expanded="true" aria-controls="spamfiltering">
				Spam Filtering
			</a>
		</h5>
	</div>

	<div id="spamfiltering" class="collapse" role="tabpanel" aria-labelledby="headingSpamfiltering">

		<div class="card-block">
			<table width="100%">
				<tr>
					<td class="">
						<h4>Period</h4>
					</td>
				</tr>
				<tr>
					<td align="center">
						<?=$sastats['begin_date'] . ' - ' . $sastats['end_date']?>
					</td>
				</tr>
				<tr>
					<td class=""><h4>Overall Parsing Information</h4></td>
				</tr>
				<tr>
					<td>
						<code class="prg-output"><?=$sastats['reporting_information']?></code>
					</td>
				</tr>
				<tr>
					<td class=""><h4>Parsing per Hour</h4></td>
				</tr>
				<tr>
					<td>
						<code class="prg-output"><?=$sastats['stats_by_hour']?></code>
					</td>
				</tr>
				<tr>
					<td>
						<h4>Score Information</h4>
					</td>
				</tr>
				<tr>
					<td>
						<code class="prg-output"><?=$sastats['rule_information']?></code>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>